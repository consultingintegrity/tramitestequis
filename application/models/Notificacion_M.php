<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Notificacion_M extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
        //Codeigniter : Write Less Do More
    } //__construct

    public function insert($id_solicitud, $id_usuario, $contenido)
    {
        $data = array(
            'id_solicitud' => $id_solicitud,
            "id_usuario"   => $id_usuario,
            "contenido"    => $contenido,
        );
        $query = $this->db->insert('notificacion', $data);
        return $query;
    } //insert
    //funcion que trae los campos para llenar la pestaña notificacion  en base a el id de el ciudadano
    public function getNotificacion($id_ciudadano,$id_solicitud)
    {
        $this->db->select("n.contenido,n.estatus,t.nombre,t.logo,s.id,n.fecha")
            ->from("notificacion n")
            ->join("solicitud s", "s.id = n.id_solicitud")
            ->join("tramite t", "t.id = s.id_tramite")
            ->join("usuario u", "n.id_usuario = u.id")
            ->where("n.id_usuario", $id_ciudadano)
            ->where("n.id_solicitud", $id_solicitud)
            /*->where('s.estatus <> 1')
            ->where('s.estatus <> 0')*/
            ->order_by('n.id',"DESC");
        $query = $this->db->get();
        return $query->result();
    } //

    public function progresoSolictud($id_ciudadano)
    {
      $this->db->select("t.nombre, t.logo,
                        s.id, s.fecha_inicio,
                        (SELECT 100 / COUNT(tramite_fase.id_fase) * f.noFase FROM tramite_fase WHERE tramite_fase.id_tramite = t.id ) as progreso,
                        g.descripcion as giro, tc.nombreSubTramite as tipoConstruccion")
          ->from("solicitud s")
          ->join("tramite_fase tf","s.id_fase = tf.id_fase")
          ->join("tramite t", "s.id_tramite = t.id")
          ->join("fase f", "s.id_fase = f.id")
          ->join("solicitud_giro sg", "s.id = sg.id_solicitud","LEFT")
          ->join("giro g", "sg.id_giro = g.id","LEFT")
          ->join("solicitud_construccion sc", "s.id = sc.id_solicitud","LEFT")
          ->join("subTramite tc", "sc.id_tipoConstruccion = tc.id","LEFT")
          ->where("s.id_ciudadano", $id_ciudadano)
          ->where('s.estatus <> 1')
          ->where('s.estatus <> 0')
          ->order_by('s.id',"DESC");
      $query = $this->db->get();
      return $query->result();
    }//progresoSolictud

} //class
