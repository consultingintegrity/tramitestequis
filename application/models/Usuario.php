<?php
/**
 * @author Guadalupe Valerio
 * @version 1.0
 */
defined('BASEPATH') or exit('No direct script access allowed');

/**
 * En esta clase vamos a obtener inforación del
 */
class Usuario extends CI_Model {
	private $id;

	public function getUsuario($id){
		$this->db->select("p.primer_nombre,p.segundo_nombre,p.primer_apellido,p.segundo_apellido,p.genero,p.fecha_nacimiento,p.id_municipio,
							u.*,
							m.id_estado as id_estado, m.id as id_municipio,
							de.id as idDependencia, de.nombre as dependencia, de.descripcion as descripcionDependencia, di.id as id_direccion, di.descripcion as decripcionDireccion")
			->from("usuario u")
			->join("persona p","u.id_persona = p.id","left")
			->join("municipio m","p.id_municipio = m.id","left")
			->join("dependencia de","u.id_dependencia = de.id","left")
			->join("direccion di","de.id_direccion = di.id","left")
			->where("u.id",$id);
		$query = $this->db->get();
		return $query->row();
	}//getusuario


	public function getRol($id){
		$this->db->select("r.*")
			->from("rol r")
			->join("usuario_rol ur","ur.id_rol = r.id","left")
			->where("ur.id_usuario",$id);
		$query = $this->db->get();
		return $query->result();
	}//getRol

	public function exist($correo,$pwd){
		$this->db->select("id,correo_electronico,contrasenia")
			->from("usuario")
			->where("correo_electronico",$correo)
			->where("contrasenia LIKE BINARY",$pwd);
		$sql = $this->db->get();
		return $sql->row();
	}//exist

	public function ModImg($imagen,$id_usuario){

		 $data = array(
		"imagen"=>$imagen

		);


		 $this->db->where('id',$id_usuario);
		if ($this->db->update('usuario', $data)) {
		 	return true;
		 }//if update
		 else {
		 	return false;
		 }//else

	}
	public function registro($primer_nombre,$segundo_nombre,$primer_apellido,$segundo_apellido,$genero,$muni,$correo_electronico,$contrasenia,$id_rol,$telefono){
		$data = array(
		"primer_nombre"=>$primer_nombre,
		"segundo_nombre"=>$segundo_nombre,
		"primer_apellido"=>$primer_apellido,
		"segundo_apellido"=>$segundo_apellido,
		//"fecha_nacimiento"=>$fecha_nacimiento,
		"genero"=>$genero,
		"id_municipio"=>$muni
		//"calle"=>$calle
		//"colonia"=>$colonia,
		//"rfc"=>$rfc,
		//"razonso"=>$razonso,
		//"tipoPer"=>$tipoPer
		);

		if ($this->db->insert('persona', $data)) {
			$id_persona = $this->db->insert_id();
			$ban=true;
		}//if
		else {
			$ban=false;
		}//else

		$data = array(
		"correo_electronico"=>$correo_electronico,
		"contrasenia"=>$contrasenia,
		"id_persona"=>$id_persona,
		"telefono"=>$telefono,

		);	//data


		if ($this->db->insert('usuario', $data)) {
			$id_usuario = $this->db->insert_id();
			$ban2=true;
		}//if
		else {
		$ban2=false;
		}//else


		$data = array(
		"id_rol"=>$id_rol,
		"id_usuario"=>$id_usuario

		);	//data
		if ($this->db->insert('usuario_rol', $data)) {

			$ban3=true;
		}//if
		else {
			$ban3=false;
		}
		if ($ban&&$ban2&&$ban3) {
			return true;
		}//if
		else {
			return false;
		}//else


	}//registro
	public function getUsuarioId($correo_electronico)
	{
	$this->db->select("id")
			->from("usuario")

			->where("correo_electronico",$correo_electronico);
		$query = $this->db->get();
		return $query->row();
	}
	public function getStatus($id_usuario)
	{
	$this->db->select("status")
			->from("usuario")

			->where("id",$id_usuario);
		$query = $this->db->get();
		return $query->row();
	}
public function cambiarcontra($contrasenia,$id_usuario){




		 $data = array(
		"contrasenia"=>$contrasenia,

		);


		 $this->db->where('id', $id_usuario);
		if ($this->db->update('usuario', $data)) {
		 	return true;
		 }//if update
		 else {
		 	return false;
		 }//else
	}//modContra
	public function cambiartelefono($telefono,$id_usuario){




		 $data = array(
		"telefono"=>$telefono,

		);


		 $this->db->where('id', $id_usuario);
		if ($this->db->update('usuario', $data)) {
		 	return true;
		 }//if update
		 else {
		 	return false;
		 }//else
	}//modContra

public function modUsu($correo_electronico,$telefono,$id_usuario){




		 $data = array(
		"correo_electronico"=>$correo_electronico,
		"telefono"=>$telefono
		);


		 $this->db->where('id', $id_usuario);
		if ($this->db->update('usuario', $data)) {
		 	return true;
		 }//if update
		 else {
		 	return false;
		 }//else
	}//modUsu
public function modPer($primer_nombre,$segundo_nombre,$primer_apellido,$segundo_apellido,$fecha_nacimiento,$genero,$id_municipio,$id_persona){




		 $data = array(
		"primer_nombre"=>$primer_nombre,
		"segundo_nombre"=>$segundo_nombre,
		"primer_apellido"=>$primer_apellido,
		"segundo_apellido"=>$segundo_apellido,
		"fecha_nacimiento"=>$fecha_nacimiento,
		"genero"=>$genero,
		"id_municipio"=>$id_municipio
		);

		 $this->db->where('id', $id_persona);
		 if ($this->db->update('persona', $data)) {
		 	return true;
		 }//if update
		 else {
		 	return false;
		 }//update
	}//modPer

public function actualizaStatus($id_usuario){




		 $data = array(
		"status"=>1,

		);


		 $this->db->where('id', $id_usuario);
		if ($this->db->update('usuario', $data)) {
		 	return true;
		 }//if update
		 else {
		 	return false;
		 }//else
	}//actualizaStatus

	/**
	 * @return int
	 */
	public function getId()
	{
	    return $this->id;
	}//get

	/**
	 * @param int $id
	 */
	public function setId($id)
	{
	    $this->id = $id;
		return $this;
	}

}//class
/* End of file Usuario.php */
/* Location: ./application/models/Usuario.php */
