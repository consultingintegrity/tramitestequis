<?php
ini_set('memory_limit', '3024M'); // or you could use 1G
defined('BASEPATH') OR exit('No direct script access allowed');

class Predio extends CI_Model {

	public function __construct(){
    	parent::__construct();
    	//Codeigniter : Write Less Do More
 	}


	public function insertBatch($arr_data){
		$this->db->truncate('predio');
		return $this->db->insert_batch('predio', $arr_data);
	}//insertBatch

}//Model

/* End of file Predio.php */
/* Location: ./application/models/Predio.php */