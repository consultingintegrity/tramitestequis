<?php

defined('BASEPATH') or exit('No direct script access allowed');

/**
 *
 */
class Documentos extends CI_Model{


    //inserto documento de licencia
    public function insertaDocs($idSolicitud,$idTramite,$url,$folio){
        //consulto que documento es
        $this->db->select("id")
                ->from("documento")
                ->where("id_tramite", $idTramite);
        $query = $this->db->get();
        $id = $query->row();

        //verifico que no exista
        $this->db->select("*")
                ->from("documento_solicitud")
                ->where("id_documento", $id->id)
                ->where("id_solicitud", $idSolicitud);
        $query = $this->db->get();
        $result = $query->row();
        //si existe actualizo
        if(!empty($result)){

            $data = array(
                'id_documento' => $id->id,
                'id_solicitud' => $idSolicitud,
                'url' => $url,
                'folio' => $folio,

            );
            $query3 = $this->db->where("id_solicitud",$idSolicitud)->update('documento_solicitud', $data);

        }else{

            $data = array(
                'id_documento' => $id->id,
                'id_solicitud' => $idSolicitud,
                'url' => $url,
                'folio' => $folio,

            );
            $query3 = $this->db->insert('documento_solicitud', $data);
        }
    return true;

    }//.insertaDocs


    //registro el archivo de pago
    public function insertaPago($idSolicitud,$idTramite,$url,$fecha_vencimiento,$folio,$lineaCaptura){
            $this->db->select("*")
            ->from("pago")
            ->where("id_solicitud", $idSolicitud)
            ->where("id_tramite", $idTramite);
        $query = $this->db->get();
        $result = $query->row();
        //si existe actualizo
        if(!empty($result)){

            $data = array(
                'id_tramite' => $idTramite,
                'url' => $url,
                'fecha_vencimiento' => $fecha_vencimiento,
                'folio' => $folio,
                'linea_captura' => $lineaCaptura

            );
            $query2 = $this->db->where("id_solicitud",$idSolicitud)->update('pago', $data);
        }else{
        $data = array(
            'id_solicitud' => $idSolicitud,
            'id_tramite' => $idTramite,
            'url' => $url,
            'fecha_vencimiento' => $fecha_vencimiento,
            'folio' => $folio,
            'linea_captura' => $lineaCaptura
        );
            $query3 = $this->db->insert('pago', $data);
        }

    return true;

    }//.insertapago
     public function terminacion($idSolicitud){

            $this->db->select("*")
            ->from("solicitud_construccion")
            ->where("id_solicitud", $idSolicitud)
            ->where("id_tipoConstruccion", 15);
            $query = $this->db->get();
            $result = $query->result();
            return $result;
      
 
       


   

    }//.insertapago

}//.documentos
