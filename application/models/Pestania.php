<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pestania extends CI_Model {

	public function __construct(){
        parent::__construct();

    }

	public function tramite_fase_pestania($id_tramite,$noFase,$id_rol){
		//Obtenemos los tramite de las pestañas
		$this->db->select("tf.id_fase as faseTF,
						p.id as id_pestania, p.nombre, p.descripcion,
						f.id_rol as usuario_rol")
			->from("tramite_fase tf")
			->join("fase_pestania fp"," tf.id_fase = fp.id_fase","LEFT")
			->join("pestania p","fp.id_pestania = p.id","LEFT")
			->join("fase f","f.id = tf.id_fase ","LEFT")
			->where("tf.id_tramite",$id_tramite)
			->where("f.id",$noFase)
			->where("f.id_rol",$id_rol);
		$query = $this->db->get();
		$result = $query->result();
		return $result;
	}//getPestania


}//Class

/* End of file Pestania.php */
/* Location: ./application/models/Pestania.php */
