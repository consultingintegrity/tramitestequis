<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fase_M extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  public function getFase($no_fase,$id_tramite)
  {
    $this->db->select('f.id')
             ->from('fase f')
             ->join('tramite_fase tf','f.id = tf.id_fase','LEFT')
             ->where('tf.id_tramite',$id_tramite)
             ->where('f.noFase',$no_fase);
      $query = $this->db->get();
      return $query->row();
  }//getFase

  /**
   * Obtenemos el no de fase respecto a su id
   *return row
   */
   public function getNoFase($id_fase)
   {
     $this->db->select('noFase')
              ->from('fase')
              ->where('id',$id_fase);
      $query = $this->db->get();
      return $query->row();
   }//getNoFase

   /**
    * Obtenemos la información de una fase
    * return row
    */
  public function fase($id_fase)
  {
    //Obtenemos la información de la fase
    $query = $this->db->get_where('fase',['id'=>$id_fase]);
    return $query->row();
  }//fase



}//class
