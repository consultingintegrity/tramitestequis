<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Encuesta_model extends CI_Model
{
    public function __construct()
    {
    }

    public function insert_respuesta($data)
    {
       $insert= $this->db->insert('t_encuesta',$data);
        // $insert_id= $this->db->insert_id();
        return $insert;
    }
}