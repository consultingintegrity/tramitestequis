<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Predial_M extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
  }

  //consulto toda la informacion de la clave catastral
  public function ConsultaInfo($clave){
    $this->db->select("*")
             ->from("predio")
             ->where("ClaveCatastral",$clave);
    $sql = $this->db->get();
    return $sql->row();
  }
  public function getPredialSol(){
    $query = $this->db->query('SELECT *, 
    CASE 
      WHEN estatus_vigente = 0
      THEN "Vigente"
      ELSE "Vencido"
    END AS estatus_vigente, 
    CASE	
      WHEN estatus_pago = 0
      THEN "No Pagado"
      ELSE "Pagado"
    END AS estatus_pago
    FROM predioSol
    WHERE estatus_vigente = 0
    AND en_corteP = 0
    AND MONTH(fecha_emision) = MONTH(NOW())
    AND YEAR(fecha_emision) = YEAR(NOW())');

      return $query->result();
  }//getPredialSol
  
  //funcion que toma el total a pagar, para los sweetAlert en pago predial
  public function getMonto($id){
    $this->db->select("total")
             ->from("predioSol")
             ->where("id",$id);
    $sql = $this->db->get();
    return $sql->row();
  }//getMonto

  public function uptadeEstPago($id){
    $query = $this->db->query("UPDATE predioSol SET estatus_pago = 1, fecha = CURDATE() WHERE id = '$id'");
    return $query;
  }

  public function updateEstVigente(){
    $query = $this->db->query("UPDATE predioSol SET estatus_vigente = 1 WHERE fecha_vencimiento < CURDATE()");
    return $query;
  }

  public function insertCortePredial(){
    $query= $this->db->query("INSERT INTO cortepredial(id_prediosol, fecha_corte, monto_predial)
      SELECT predioSol.id, NOW(), predioSol.total
      FROM predioSol
      WHERE predioSol.estatus_pago = 1
      AND predioSol.en_corteP = 0
      AND predioSol.id=predioSol.id");
      
      $rowsAffected = $this->db->affected_rows() > 0;
      if ($rowsAffected > 0) return true;
      else return false;
    
  }//insertCortePredial

  public function generaCorteP(){//actualiza el estatus en la tabla tipo_pago en la columna corte
    $query = $this->db->query("UPDATE predioSol SET en_corteP = 1 WHERE estatus_pago = 1");
    return $query;
  }

  public function cortePredial(){
    $query = $this->db->query('SELECT cortepredial.id, predioSol.nombreContribuyente, predioSol.ClaveCatrastal, predioSol.lineaCaptura, 
    predioSol.folio, predioSol.total, predioSol.fecha
    FROM predioSol, cortepredial
    WHERE predioSol.id = cortepredial.id_prediosol
    AND cortepredial.fecha_corte=CURDATE()');

      return $query->result();
  }//cortePredial

  public function sumMontoTotalP(){
    $query = $this->db->query("SELECT SUM(monto_predial) as 'monto_predial' FROM cortepredial WHERE fecha_corte=CURDATE()");
      return $query->row('monto_predial');
  }//sumMontoTotalP

  public function saveDocCorte($id_corte, $id_cortePredial, $monto_total, $tipo_corte, $url){
    $data = array(
      "id_corte"=>$id_corte,
      "id_cortePredial"=>$id_cortePredial,
      "monto_total"=>$monto_total,
      "tipo_corte"=> $tipo_corte,
      "url"=>$url
    );
    $query = $this->db->insert('documento_corte', $data);
    return $query;
  }//saveDocCorte

  public function documentoGenerado(){
    $query = $this->db->query("UPDATE documento_corte SET documento_generado = 1");
  return $query;
  }

  public function insertaPredialSol($nomPersona,$cCatastral,$folio,$lCaptura,$total,$url,$fechaEmision,$fechaVencimiento){
    $estatusV=0;
    $estatusP=0;
    $data = array (
      "nombreContribuyente" => $nomPersona,
      "claveCatrastal" => $cCatastral,
      "FOLIO" => $folio,
      "lineaCaptura" => $lCaptura,
      "total" => $total,
      "url" => $url,
      "fecha_emision" => $fechaEmision,
      "fecha_vencimiento" => $fechaVencimiento,
      "estatus_vigente" => $estatusV,
      "estatus_pago" => $estatusP,
    ); //Campos de la tabla
    $query = $this->db->insert('predioSol', $data);
    $estatus =array ("estatus_vigente"=> 1);
    $query2 = $this->db->where("fecha_vencimiento < CURDATE()")->where("claveCatrastal",$cCatastral)->update('predioSol', $estatus);
     return $query;
  }//insertaPredialSol

  //consulto si existe registro vigente
  public function consultaExiste($clave){
    $this->db->select("url,fecha_vencimiento")
    ->from("predioSol")
    ->where("claveCatrastal",$clave)
    ->where("estatus_vigente ",0)
    ->where("fecha_vencimiento >= CURDATE()");
    $sql = $this->db->get();
    return $sql->row();
  }
  //consulto todo respecto a las ordenes de pago generadas
  public function consultaTodo(){
    $this->db->select("*")
    ->from("predioSol");
    $sql = $this->db->get();
    return $sql->result();
  }

  public function consultaActual($clave){
    $this->db->select("id,FOLIO")
        ->from("predioSol")
        ->where("claveCatrastal",$clave)
        ->order_by("id","desc")
        ->limit(1);
    $sql = $this->db->get();
    return $sql->row();
  }

  public function updateFolio($folio,$id){
    $folio =array ('lineaCaptura'=> $folio);
    $query2 = $this->db->where('id',$id)->update('predioSol', $folio);
    return $query2;
  }

  public function updatetrans($folio,$id){
    $folio =array ('FOLIO'=> $folio);
    $query2 = $this->db->where('id',$id)->update('predioSol', $folio);
    return $query2;
  }

  public function validTransaccion($tr){
    $this->db->select("lineaCaptura,id")
        ->from("predioSol")
        ->where("lineaCaptura",$tr);
        $sql = $this->db->get();
        return $sql->row();
  }

  public function updateDoc($id,$url){
    $url =array ('url'=> $url);
    $query2 = $this->db->where('id',$id)->update('predioSol', $url);
    return $query2;
  }

  public function doc($id){
    $this->db->select("url")
    ->from("predioSol")
    ->where("id",$id);
    $sql = $this->db->get();
    return $sql->row();
  }
}