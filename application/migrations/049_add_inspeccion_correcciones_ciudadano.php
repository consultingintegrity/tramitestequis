<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_inspeccion_correcciones_ciudadano extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
              'type'           => 'INT',
              'constraint'     => 11,
              'unsigned'       => true,
              'auto_increment' => true,
            ),
            'id_solicitud' => array(
              'type'       => 'INT',
              'constraint' => 11,
              'unsigned'   => true,
              'null'       => false,
            ),
  	        'id_ciudadano' => array(
              'type'       => 'INT',
              'constraint' => 11,
              'unsigned'   => true,
              'null'       => false,
            ),
            'id_funcionario'  => array(
              'type'       => 'INT',
              'constraint' => 11,
              'unsigned'   => true,
              'null'       => false,
            ),
            'observaciones'  => array(
              'type'       => 'TEXT',
              'constraint' => 500,
            ),
            'url' => array(
              'type' => 'CHAR',
              'constraint' => '180',
            ),
  	        'fecha' => array(
  	          'type' => 'TIMESTAMP',
  	          'null'=> FALSE,
  	        ),
        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('correcciones_ciudadano_inspeccion');
        		$this->db->query("ALTER TABLE `correcciones_ciudadano_inspeccion` CHANGE `fecha` `fecha` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `correcciones_ciudadano_inspeccion` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `correcciones_ciudadano_inspeccion` ADD FOREIGN KEY (`id_ciudadano`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `correcciones_ciudadano_inspeccion` ADD FOREIGN KEY (`id_funcionario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");

    } //up

    public function down()
    {
        $this->dbforge->drop_table("correcciones_ciudadano_inspeccion");
    } //down

} //class

/* End of file 033_add_pago.php */
/* Location: ./application/controllers/033_add_pago.php */
