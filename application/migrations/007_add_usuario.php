<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_usuario extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'correo_electronico' => array(
		                'type' => 'VARCHAR',
		                'unique' => TRUE,
		                'constraint' => '65',
		                'null'	=> FALSE,
		        ),
		        'contrasenia' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '130',
		                'null'	=> FALSE,
		        ),
		        'imagen' => array(
		                'type' => 'CHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'telefono' => array(
		                'type' => 'CHAR',
		                'constraint' => '18',
		                'null'	=> TRUE,
		        ),
		        'id_persona' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		        'id_dependencia' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'null'	=> TRUE,
		        ),
		        'status' => array(
		                'type' => 'TINYINT',
		                'constraint' => 2,
		                'unsigned' => TRUE,
		                'default' => 2,
		        ),
		        'extension' => array(
		                'type' => 'TINYINT',
		                'constraint' => 3,
		                'unsigned' => TRUE,

		        ),
		        'telefono_oficina' => array(
		                'type' => 'CHAR',
		                'constraint' => '18',
		                'null'	=> TRUE,
		        ),

		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('usuario');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `usuario` ADD FOREIGN KEY (`id_persona`) REFERENCES `persona`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//Agregamos la clave foranea de dependencia
		$this->db->query("ALTER TABLE `usuario` ADD FOREIGN KEY (`id_dependencia`) REFERENCES `dependencia`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		 //creamos un array con los datos del usuario
		$data_usuario = array(
			//admin
			array("correo_electronico"=>"admintx@tx.com.mx","contrasenia" => hash('sha512', "123"),"id_persona" => 1,"id_dependencia"=>1,"status" => 1),//1
			//DU
			array("correo_electronico"=>"desarrollourbano@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "ventanillaDU123"),"id_persona" => 2,"id_dependencia"=>2,"status" => 1),//2
			array("correo_electronico"=>"desarrollourbano1@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "encargadoNFDU123"),"id_persona" => 3,"id_dependencia"=>2,"status" => 1),//3
			array("correo_electronico"=>"arqebarajas@tequisquipan.gob.mx","contrasenia" => hash('sha512', "directorDU123"),"id_persona" => 4,"id_dependencia"=>2,"status" => 1),//4
			//este rol y correo sobra
			array("correo_electronico"=>"obraspublicas@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "secretarioDU123"),"id_persona" => 5,"id_dependencia"=>2,"status" => 1),//5
			array("correo_electronico"=>"desarrollourbano2@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "inspeccionDU123"),"id_persona" => 6,"id_dependencia"=>2,"status" => 1),//6
			array("correo_electronico"=>"desarrollourbano3@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "jefeInspeccionDU123"),"id_persona" => 7,"id_dependencia"=>2,"status" => 1),//7
			//este rol y correo sobra
			array("correo_electronico"=>"desarrollourbano4@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "secretariaDU123"),"id_persona" => 8,"id_dependencia"=>2,"status" => 1),//8
			array("correo_electronico"=>"desarrollourbano5@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "encargadoUSDU123"),"id_persona" => 9,"id_dependencia"=>2,"status" => 1),//9		
			array("correo_electronico"=>"desarrollourbano6@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "subDirector123"),"id_persona" =>10,"id_dependencia"=>2,"status" => 1),//10
			array("correo_electronico"=>"ventanilladeatencion@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "Cordinacion123"),"id_persona" =>11,"id_dependencia"=>2,"status" => 1),//11
			//aun no lo tengo
			array("correo_electronico"=>"opinionTecnicaDU@tx.com.mx","contrasenia" => hash('sha512', "opinionTecnicaDU123"),"id_persona" =>12,"id_dependencia"=>2,"status" => 1),//12
			//Usuarios para protección Civil
			array("correo_electronico"=>"proteccioncivil@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "ventanillaPC123"),"id_persona" =>13,"id_dependencia"=>3,"status" => 1),//13
			array("correo_electronico"=>"proteccioncivil1@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "inspeccionPC123"),"id_persona" =>14,"id_dependencia"=>3,"status" => 1),//14
			array("correo_electronico"=>"proteccioncivil2@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "opinionTecnicaPC123"),"id_persona" =>15,"id_dependencia"=>3,"status" => 1),//15
			//SG
			array("correo_electronico"=>"secgeneral@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "ventanillaSG123"),"id_persona" =>16,"id_dependencia"=>4,"status" => 1),//16
			array("correo_electronico"=>"secgeneral2@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "secretariaSG123"),"id_persona" =>18,"id_dependencia"=>4,"status" => 1),//18
			array("correo_electronico"=>"secretariaayuntamientotx@hotmail.com","contrasenia" => hash('sha512', "comisionSG123"),"id_persona" =>17,"id_dependencia"=>4,"status" => 1),//17
			array("correo_electronico"=>"secgeneral3@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "secretarioG123"),"id_persona" =>19,"id_dependencia"=>4,"status" => 1),//19
			//Usuarios  Licencias de Funcionamiento
			array("correo_electronico"=>"ingresos@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "cajaI123"),"id_persona" => 20,"id_dependencia"=>5,"status" => 1),//20
			array("correo_electronico"=>"ingresos1@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "ventanillaI123"),"id_persona" =>21,"id_dependencia"=>5,"status" => 1),//21
			array("correo_electronico"=>"carlosrenteria@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "tesoreroI123"),"id_persona" =>22,"id_dependencia"=>5,"status" => 1),//22
			array("correo_electronico"=>"presidente@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "presidenteMunicipalI123"),"id_persona" =>23,"id_dependencia"=>5,"status" => 1),//23
			array("correo_electronico"=>"opinionTecnicaFinanzas@tx.com.mx","contrasenia" => hash('sha512', "opinionTecnicaFinanzas123"),"id_persona" =>24,"id_dependencia"=>5,"status" => 1),//24
			array("correo_electronico"=>"secgeneral1@tequisquiapan.gob.mx","contrasenia" => hash('sha512', "inspeccionSecG123"),"id_persona" =>25,"id_dependencia"=>5,"status" => 1),//25
			//Usuario de Prueba
			array("correo_electronico"=>"ciudadano@tx.com.mx","contrasenia" => hash('sha512', "ciudadano123"),"id_persona" =>26,"id_dependencia"=>NULL,"status" => 1),//26

		);
		 //ingresamos el registro en la base de datos
		 $this->db->insert_batch("usuario", $data_usuario);
	}//up

	public function down() {
		$this->dbforge->drop_table("usuario");
	}//down

}//class

/* End of file 005_add_usuario.php */
/* Location: ./application/migrations/005_add_usuario.php */
