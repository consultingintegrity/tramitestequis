<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_requisitoSolicitud extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'url' => array(
	                'type' => 'CHAR',
	                'constraint' => '180',
	                "null" => FALSE,
	        ),
	        'estatus' => array(
	                'type' => 'TINYINT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_ciudadano' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_requisito' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);		
		//creamos la tabla
		$this->dbforge->create_table('requisito_solicitud');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `requisito_solicitud` ADD FOREIGN KEY (`id_ciudadano`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `requisito_solicitud` ADD FOREIGN KEY (`id_requisito`) REFERENCES `requisito`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `requisito_solicitud` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
	}//up

	public function down() {
		$this->dbforge->drop_table("requisito_solicitud");
	}//down

}//class

/* End of file 027_add_requisitoSolicitud.php */
/* Location: ./application/migrations/027_add_requisitoSolicitud.php */