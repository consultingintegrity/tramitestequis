<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_fase extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'noFase' => array(
	                'type' => 'TINYINT',
	                'constraint' => '30',
	                "null" => FALSE,
	        ),
	        'id_rol' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'NULL' => TRUE,
	        ),
	        'anterior' => array(
	                'type' => 'TINYINT',
	                'constraint' => '30',
	        ),
	        'siguiente' => array(
	                'type' => 'TINYINT',
	                'constraint' => '30',
	        ),
	        'id_documento' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                "null" => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('fase');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `fase` ADD FOREIGN KEY (`id_documento`) REFERENCES `documento`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `fase` ADD FOREIGN KEY (`id_rol`) REFERENCES `rol`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//Agregamos registros a la tabla
		$data_fase = array(
			//Fases para Alineamiento y Número Oficial
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>2,"id_documento"=>NULL),//1 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>1,"siguiente"=>3,"id_documento"=>NULL),//2 VENTANILLA
			array("noFase"=> 3, "id_rol"=>8,"anterior"=>2,"siguiente"=>4,"id_documento"=>NULL),//3 INSPECCION
			array("noFase"=> 4, "id_rol"=>9,"anterior"=>3,"siguiente"=>5,"id_documento"=>NULL),//4 JEFE DE INSPECCIÓN
			array("noFase"=> 5, "id_rol"=>5,"anterior"=>4,"siguiente"=>6,"id_documento"=>1),//5 ENCARGADO DE NUMEROS OFICIALES
			array("noFase"=> 6, "id_rol"=>23,"anterior"=>5,"siguiente"=>7,"id_documento"=>1),//6 SUBdIRECTOR
			array("noFase"=> 7, "id_rol"=>6,"anterior"=>6,"siguiente"=>8,"id_documento"=>1),//7 DIRECTOR
			array("noFase"=> 8, "id_rol"=>4,"anterior"=>7,"siguiente"=>9,"id_documento"=>1),//8 VENTANILLA
			array("noFase"=> 9, "id_rol"=>3,"anterior"=>8,"siguiente"=>10,"id_documento"=>1),//9 CAJA
			array("noFase"=> 10, "id_rol"=>4,"anterior"=>9,"siguiente"=>NULL,"id_documento"=>1),//10 VENTANILA
			//Fases para Disctamen Uso de Suelo
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>12,"id_documento"=>NULL),//11 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>11,"siguiente"=>13,"id_documento"=>NULL),//12 VENTANILLA
			array("noFase"=> 3, "id_rol"=>8,"anterior"=>12,"siguiente"=>14,"id_documento"=>NULL),//13 INSPECCION
			array("noFase"=> 4, "id_rol"=>9,"anterior"=>13,"siguiente"=>15,"id_documento"=>NULL),//14 JEFE DE INSPECCIÓN
			array("noFase"=> 5, "id_rol"=>11,"anterior"=>14,"siguiente"=>16,"id_documento"=>1),//15 ENCARGADO DE Dictamen uso de Suelo
			array("noFase"=> 6, "id_rol"=>6,"anterior"=>15,"siguiente"=>18,"id_documento"=>1),//16 SUBdIRECTOR
			array("noFase"=> 7, "id_rol"=>7,"anterior"=>16,"siguiente"=>18,"id_documento"=>1),//17 DIRECTOR
			array("noFase"=> 8, "id_rol"=>4,"anterior"=>16,"siguiente"=>19,"id_documento"=>1),//18 VENTANILLA
			array("noFase"=> 9, "id_rol"=>3,"anterior"=>18,"siguiente"=>20,"id_documento"=>1),//19 CAJA
			array("noFase"=> 10, "id_rol"=>4,"anterior"=>19,"siguiente"=>NULL,"id_documento"=>1),//20 VENTANILA
			//Fases para Protección Civil
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>22,"id_documento"=>NULL),//21 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>21,"siguiente"=>23,"id_documento"=>NULL),//22 VENTANILLA
			array("noFase"=> 3, "id_rol"=>8,"anterior"=>21,"siguiente"=>24,"id_documento"=>NULL),//23 INSPECTOR
			array("noFase"=> 4, "id_rol"=>4,"anterior"=>23,"siguiente"=>25,"id_documento"=>NULL),//24 VENTANILA
			array("noFase"=> 5, "id_rol"=>3,"anterior"=>24,"siguiente"=>26,"id_documento"=>NULL),//25 CAJA
			array("noFase"=> 6, "id_rol"=>4,"anterior"=>25,"siguiente"=>27,"id_documento"=>3),//26 VENTANILA
			array("noFase"=> 7, "id_rol"=>4,"anterior"=>26,"siguiente"=>0,"id_documento"=>3),//27 VENTANILA
			//Fases para licencia de funcionamiento
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>29,"id_documento"=>NULL),//28 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>28,"siguiente"=>30,"id_documento"=>NULL),//29 VENTANILLA
			array("noFase"=> 3, "id_rol"=>21,"anterior"=>29,"siguiente"=>31,"id_documento"=>NULL),//30 INSPECTOR
			array("noFase"=> 4, "id_rol"=>4,"anterior"=>30,"siguiente"=>32,"id_documento"=>4),//31 VENTANILA
			array("noFase"=> 5, "id_rol"=>3,"anterior"=>31,"siguiente"=>33,"id_documento"=>4),//32 CAJA
			array("noFase"=> 6, "id_rol"=>4,"anterior"=>32,"siguiente"=>34,"id_documento"=>4),//33 VENTANILA
			array("noFase"=> 7, "id_rol"=>12,"anterior"=>33,"siguiente"=>35,"id_documento"=>4),//34 TESORERO
			array("noFase"=> 8, "id_rol"=>13,"anterior"=>34,"siguiente"=>36,"id_documento"=>4),//35 PRESIDENTE
			array("noFase"=> 9, "id_rol"=>4,"anterior"=>35,"siguiente"=>0,"id_documento"=>4),//36 VENTANILLA
			//Fases para factibilidad de giro
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>38,"id_documento"=>NULL),//37 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>37,"siguiente"=>39,"id_documento"=>NULL),//38 VENTANILLA
			array("noFase"=> 3, "id_rol"=>8,"anterior"=>38,"siguiente"=>40,"id_documento"=>NULL),//39 INSPECCION
			array("noFase"=> 4, "id_rol"=>9,"anterior"=>39,"siguiente"=>41,"id_documento"=>NULL),//40 JEFE DE INSPECCION
			array("noFase"=> 5, "id_rol"=>4,"anterior"=>40,"siguiente"=>42,"id_documento"=>5),//41 VENTANILA
			array("noFase"=> 6, "id_rol"=>6,"anterior"=>41,"siguiente"=>43,"id_documento"=>5),//42 DIRECTOR DE DESARROLLO URBANO
			array("noFase"=> 7, "id_rol"=>4,"anterior"=>42,"siguiente"=>44,"id_documento"=>5),//43 VENTANILA
			array("noFase"=> 8, "id_rol"=>3,"anterior"=>43,"siguiente"=>45,"id_documento"=>1),//44 CAJA
			array("noFase"=> 9,"id_rol"=>4,"anterior"=>44,"siguiente"=>NULL,"id_documento"=>1),//45 VENTANILA
			//Fases para Licencia de Construcción
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>47,"id_documento"=>NULL),//46 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>46,"siguiente"=>48,"id_documento"=>NULL),//47 VENTANILLA
			array("noFase"=> 3, "id_rol"=>8,"anterior"=>47,"siguiente"=>49,"id_documento"=>NULL),//48 INSPECCION
			array("noFase"=> 4, "id_rol"=>9,"anterior"=>48,"siguiente"=>50,"id_documento"=>NULL),//49 JEFE DE INSPECCION
			array("noFase"=> 5, "id_rol"=>4,"anterior"=>49,"siguiente"=>51,"id_documento"=>NULL),//50 VENTANILLA
			array("noFase"=> 6, "id_rol"=>6,"anterior"=>50,"siguiente"=>52,"id_documento"=>6),//51 DIRECTOR DE DESARROLLO URBANO
			array("noFase"=> 7, "id_rol"=>4,"anterior"=>51,"siguiente"=>53,"id_documento"=>6),//52 VENTANILA
			array("noFase"=> 8, "id_rol"=>3,"anterior"=>52,"siguiente"=>54,"id_documento"=>6),//53 CAJA
			array("noFase"=> 9,"id_rol"=>4,"anterior"=>53,"siguiente"=>NULL,"id_documento"=>6),//54 VENTANILA

			//Fases para informe uso de suelo
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>56,"id_documento"=>NULL),//55 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>55,"siguiente"=>57,"id_documento"=>NULL),//56 VENTANILLA
			array("noFase"=> 3, "id_rol"=>8,"anterior"=>56,"siguiente"=>59,"id_documento"=>NULL),//57 INSPECTOR
			array("noFase"=> 4, "id_rol"=>14,"anterior"=>57,"siguiente"=>59,"id_documento"=>NULL),//58 AREA JURIDICA
			array("noFase"=> 5, "id_rol"=>24,"anterior"=>57,"siguiente"=>60,"id_documento"=>7),//59 Coordinacion VENTANILA
			array("noFase"=> 6, "id_rol"=>6,"anterior"=>59,"siguiente"=>61,"id_documento"=>7),//60 DIRECTOR DE DESARROLLO URBANO
			array("noFase"=> 7, "id_rol"=>4,"anterior"=>60,"siguiente"=>62,"id_documento"=>7),//61 VENTANILA
			array("noFase"=> 8, "id_rol"=>3,"anterior"=>61,"siguiente"=>63,"id_documento"=>7),//62 CAJA
			array("noFase"=> 9,"id_rol"=>4,"anterior"=>62,"siguiente"=>NULL,"id_documento"=>7),//63 VENTANILA

			//Trámite Licencia Venta de Alcoholes
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>65,"id_documento"=>NULL),//64 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>64,"siguiente"=>66,"id_documento"=>NULL),//65 VENTANILLA SECRETARIA GENERAL
			array("noFase"=> 3, "id_rol"=>15,"anterior"=>65,"siguiente"=>67,"id_documento"=>NULL),//66 COMISIÓN COMERCIO Y TURISMO
			array("noFase"=> 4, "id_rol"=>15,"anterior"=>66,"siguiente"=>68,"id_documento"=>8),//67 COMISIÓN COMERCIO Y TURISMO
			array("noFase"=> 5, "id_rol"=>19,"anterior"=>67,"siguiente"=>69,"id_documento"=>8),//68 SECRETARIA GENEAL AYUNTAMIENTO
			array("noFase"=> 6, "id_rol"=>4,"anterior"=>68,"siguiente"=>70,"id_documento"=>8),//69 VENTANILLA
			array("noFase"=> 7, "id_rol"=>3,"anterior"=>69,"siguiente"=>71,"id_documento"=>8),//70 CAJA
			array("noFase"=> 8, "id_rol"=>4,"anterior"=>70,"siguiente"=>NULL,"id_documento"=>8),//71 VENTANILLA
			array("noFase"=> 9, "id_rol"=>20,"anterior"=>66,"siguiente"=>67,"id_documento"=>8),//72 Opinión Técnica

			//Trámite Licencia Venta de Alcoholes(Provicional)
			array("noFase"=> 1, "id_rol"=>2,"anterior"=>0,"siguiente"=>74,"id_documento"=>NULL),//73 CIUDADANO
			array("noFase"=> 2, "id_rol"=>4,"anterior"=>73,"siguiente"=>75,"id_documento"=>NULL),//74 VENTANILLA SECRETARIA GENERAL
			array("noFase"=> 3, "id_rol"=>4,"anterior"=>74,"siguiente"=>76,"id_documento"=>NULL),//75 VENTANILLA SECRETARIA GENERAL
			array("noFase"=> 4, "id_rol"=>3,"anterior"=>75,"siguiente"=>77,"id_documento"=>9),//76 CAJA
			array("noFase"=> 5, "id_rol"=>4,"anterior"=>76,"siguiente"=>78,"id_documento"=>9),//77 VENTANILLA SECRETARIA GENERAL
			array("noFase"=> 6, "id_rol"=>22,"anterior"=>77,"siguiente"=>79,"id_documento"=>9),//78 SECRETARIO GENERAL
			array("noFase"=> 7, "id_rol"=>4,"anterior"=>78,"siguiente"=>NULL,"id_documento"=>9),//79 VENTANILLA SECRETARIA GENERAL


		);//data_fase
		//Insertamos los datos a la base de datos
		$this->db->insert_batch("fase",$data_fase);
	}//up

	public function down() {
		$this->dbforge->drop_table("fase");
	}//down

}//class

/* End of file 021_add_fase.php */
/* Location: ./application/migrations/021_add_fase.php */
