<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_funcionario_solicitud extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'estatus' => array(
		                'type' => 'INT',
		                'constraint' => '4',
		                'null'	=> FALSE,
                ),		        
                'fase' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'null'	=> FALSE,
                    'unsigned' => TRUE,
            ),		        
		        'id_solicitud' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
                ),
                'id_usuario' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
	    ),
	    'fecha' => array(
		'type' => 'TIMESTAMP',
		'constraint' => NULL,
		'unsigned' => FALSE,
	),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('funcionario_solicitud');
        $this->db->query("ALTER TABLE `funcionario_solicitud` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `funcionario_solicitud` ADD FOREIGN KEY (`id_usuario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `funcionario_solicitud` ADD FOREIGN KEY (`fase`) REFERENCES `fase`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");


		 //creamos un array con los datos de los municipios
		 //ingresamos el registro en la base de datos
	}//up

	public function down() {
		$this->dbforge->drop_table("funcionario_solicitud");
	}//down

}//class

/* End of file 031_add_funcionario_solicitud.php */
/* Location: ./application/migrations/031_add_funcionario_solicitud.php */