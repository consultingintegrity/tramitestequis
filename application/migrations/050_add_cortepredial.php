<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_cortepredial extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_prediosol' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
  	        'fecha_corte' => array(
  	                'type' => 'DATE',
  	                'null' => TRUE,
              ),
            'monto_predial'  => array(
                'type'     => 'DOUBLE',
                'unsigned' => true,
                'null'     => false,
            ),


        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('cortepredial');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `cortepredial` ADD FOREIGN KEY (`id_prediosol`) REFERENCES `prediosol`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
       	//	$this->db->query("ALTER TABLE `pago` CHANGE `fecha_vencimiento` `fecha_vencimiento` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("cortepredial");
    } //down

} //class

/* End of file 033_add_pago.php */
/* Location: ./application/controllers/033_add_pago.php */
