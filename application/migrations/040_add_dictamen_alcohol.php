<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_dictamen_alcohol extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }


    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_solicitud' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'id_funcionario' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
            ),
             'url'          => array(
                'type'       => 'VARCHAR',
                'constraint' => '80',
                'null'       => false,
            ),
        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('dictamen_alcohol');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `dictamen_alcohol` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `dictamen_alcohol` ADD FOREIGN KEY (`id_funcionario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("dictamen_alcohol");
    } //down

} //class

/* End of file 039_add_dictamen_alcohol.php */
/* Location: ./application/controllers/039_add_dictamen_alcohol.php */
