<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_solicitud extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
				$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'fecha_inicio' => array(
	                'type' => 'TIMESTAMP',
	                "null" => TRUE,
	        ),
	        'fecha_fin' => array(
	                'type' => 'TIMESTAMP',
	                'null' => TRUE,
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'estatus' => array(
	                'type' => 'TINYINT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_ciudadano' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_funcionario' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_fase' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'costo' => array(
	                'type' => 'DOUBLE',
	                "null" => true,

	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('solicitud');
		//Agregamos la clave foranea

		$this->db->query("ALTER TABLE `solicitud` ADD FOREIGN KEY (`id_ciudadano`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud` ADD FOREIGN KEY (`id_funcionario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud` ADD FOREIGN KEY (`id_fase`) REFERENCES `fase`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud` CHANGE `fecha_inicio` `fecha_inicio` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
		$this->db->query("ALTER TABLE `solicitud` CHANGE `fecha_fin` `fecha_fin` TIMESTAMP on update CURRENT_TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;");

	}

	public function down() {
		$this->dbforge->drop_table("solicitud");
	}

}

/* End of file 024_add_solicitud.php */
/* Location: ./application/migrations/024_add_solicitud.php */
