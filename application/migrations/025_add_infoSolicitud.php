<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_infoSolicitud extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'informacion' => array(
	                'type' => 'TEXT',
	                "null" => FALSE,
	        ),
	        'estatus' => array(
	                'type' => 'TINYINT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('infoSolicitud');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `infoSolicitud` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
	}//up

	public function down() {
		$this->dbforge->drop_table("infoSolicitud");
	}//down

}//class

/* End of file 025_add_infoSolicitud.php */
/* Location: ./application/migrations/025_add_infoSolicitud.php */
