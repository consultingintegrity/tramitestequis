<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Tramite_pago extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id' => array(
                'type'  => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true,
            ),
            'id_sol' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'null'       => false,
            ),
            'costo' => array(
                'type' => 'DOUBLE',
                'constraint' => '50,2',
                'null' => false,
            ),
            'fecha_ini'  => array(
                'type'     => 'DATE',
                'null'     => false,
            ),
            'folio' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => true
            ),
            'url_doc' => array(
                'type' => 'VARCHAR',
                'constraint' => '50',
                'null' => true
            ),

        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('tramite_pago');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `tramite_pago` ADD FOREIGN KEY (`id_sol`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        //	$this->db->query("ALTER TABLE `pago` CHANGE `fecha_vencimiento` `fecha_vencimiento` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("tramite_pago");
    } //down

} //class

/* End of file 037_tramite_pago.php */
