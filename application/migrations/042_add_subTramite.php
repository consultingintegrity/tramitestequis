<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_SubTramite extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'nombreSubTramite' => array(
	                'type' => 'CHAR',
	                'constraint' => 100,
                ),
                'subNumero'=> array(
                        'type' => 'VARCHAR',
                        'constraint' => 11,
                        'unsigned' => TRUE,
                ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('subTramite');
		//Agregamos la clave foranea
        $this->db->query("ALTER TABLE `subTramite` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        //Agrega registros
        $data_giro = array(

        //Alineamiento y Numero Oficial
        array("id_tramite"=> "1","nombreSubTramite"=>"Asignación de número Oficial","SubNumero"=>"1.1"),//1
        array("id_tramite"=> "1","nombreSubTramite"=>"Asignación de número Oficial en Casas","SubNumero"=>"1.2"),//2
        array("id_tramite"=> "1","nombreSubTramite"=>"Asignación de número Oficial en Fraccionamientos","SubNumero"=>"1.3"),//3
        array("id_tramite"=> "1","nombreSubTramite"=>"Asignación de número Oficial en Comercio/Empresas","SubNumero"=>"1.4"),//4
        //Dictamen de Uso de suelo id=2
        array("id_tramite"=> "2","nombreSubTramite"=>"Obra nueva","SubNumero"=>"2.1"),//5
        array("id_tramite"=> "2","nombreSubTramite"=>"Ratificación","SubNumero"=>"2.2"),//6
        array("id_tramite"=> "2","nombreSubTramite"=>"Regularización","SubNumero"=>"2.3"),//7
        array("id_tramite"=> "2","nombreSubTramite"=>"Ampliación","SubNumero"=>"2.4"),//8
        array("id_tramite"=> "2","nombreSubTramite"=>"Modificación","SubNumero"=>"2.5"),//9
        array("id_tramite"=> "2","nombreSubTramite"=>"Revalidación","SubNumero"=>"2.6"),//10
        array("id_tramite"=> "2","nombreSubTramite"=>"Reconsideración","SubNumero"=>"2.7"),//11
        //Factibilidad de Giro id=3
        array("id_tramite"=> "3","nombreSubTramite"=>"Apertura de Factibilidad de Giro","SubNumero"=>"3.1"),//12
        array("id_tramite"=> "3","nombreSubTramite"=>"Modificación de Factibilidad de Giro/Alcohol","SubNumero"=>"3.2"),//13
        //Licencia de Construccion id=7
        array("id_tramite"=> "7","nombreSubTramite"=>"Acabados","SubNumero"=>"7.1"),//14
        array("id_tramite"=> "7","nombreSubTramite"=>"Aviso de Terminación de Obra","SubNumero"=>"7.2"),//15
        array("id_tramite"=> "7","nombreSubTramite"=>"Bardeo","SubNumero"=>"7.3"),//16
        array("id_tramite"=> "7","nombreSubTramite"=>"Casa Habitación","SubNumero"=>"7.4"),//17
        array("id_tramite"=> "7","nombreSubTramite"=>"Modificación de Fachada","SubNumero"=>"7.5"),//18
        array("id_tramite"=> "7","nombreSubTramite"=>"Obra Menor","SubNumero"=>"7.6"),//19
        array("id_tramite"=> "7","nombreSubTramite"=>"Regularización de Obra","SubNumero"=>"7.7"),//20
        array("id_tramite"=> "7","nombreSubTramite"=>"Obra Nueva","SubNumero"=>"7.8"),//21
        array("id_tramite"=> "7","nombreSubTramite"=>"Ampliación, Modificación, Remodelación","SubNumero"=>"7.9"),//22
        array("id_tramite"=> "7","nombreSubTramite"=>"Revalidación","SubNumero"=>"7.10"),//23
        //Informe de Uso de Suelo id=8
        array("id_tramite"=> "8","nombreSubTramite"=>"Acabados","SubNumero"=>"8.1"),//24
        array("id_tramite"=> "8","nombreSubTramite"=>"Aviso de Terminación de Obra","SubNumero"=>"8.2"),//25
        array("id_tramite"=> "8","nombreSubTramite"=>"Bardeo","SubNumero"=>"8.3"),//26
        array("id_tramite"=> "8","nombreSubTramite"=>"Casa Habitación","SubNumero"=>"8.4"),//27
        array("id_tramite"=> "8","nombreSubTramite"=>"Modificación de Fachada","SubNumero"=>"8.5"),//26
        array("id_tramite"=> "8","nombreSubTramite"=>"Obra Menor","SubNumero"=>"8.6"),//29
        array("id_tramite"=> "8","nombreSubTramite"=>"Regularización de Obra","SubNumero"=>"8.7"),//30
        array("id_tramite"=> "8","nombreSubTramite"=>"Obra Nueva","SubNumero"=>"8.8"),//31
        array("id_tramite"=> "8","nombreSubTramite"=>"Ampliación, Modificación, Remodelación","SubNumero"=>"8.9"),//32
        array("id_tramite"=> "8","nombreSubTramite"=>"Revalidación","SubNumero"=>"8.10"),//33
        //Licencia de Funcionamiento id = 4
        array("id_tramite"=> "4","nombreSubTramite"=>"Apertura de Licencia de Funcionamiento","SubNumero"=>"4.1"),//34
        array("id_tramite"=> "4","nombreSubTramite"=>"Renovación de Licencia de Funcionamiento","SubNumero"=>"4.2"),//35
        array("id_tramite"=> "4","nombreSubTramite"=>"Modificación a la Licencia de Funcionamiento","SubNumero"=>"4.3"),//36
        //VOBO de Proteccion Civil id = 6
        array("id_tramite"=> "6","nombreSubTramite"=>"Vo.Bo. para la Licencia de Funcionamiento","SubNumero"=>"6.1"),//37
        array("id_tramite"=> "6","nombreSubTramite"=>"Vo.Bo. para Establecimientos","SubNumero"=>"6.2"),//38
        //Permiso para almacenaje, ventam, porteo y consumo de bebidas alcoholicas id = 5
        array("id_tramite"=> "5","nombreSubTramite"=>"Solicitud de permiso provisional para Almacenaje, Venta y Consumo de Bebidas Alcohólicas","SubNumero"=>"5.1"),//39
        array("id_tramite"=> "5","nombreSubTramite"=>"Permiso para Almacenaje, Venta, Porteo y Consumo de Bebidas Alcohólicas","SubNumero"=>"5.2"),//40
          
        //complemento para factibilidad de giro y proteccion Civil
        array("id_tramite"=> "6","nombreSubTramite"=>"Vo.Bo. de Protección Civil","SubNumero"=>"6.0"),//41
        array("id_tramite"=> "3","nombreSubTramite"=>"Factibilidad de Giro","SubNumero"=>"3.0"),//42
        );//data_giro
		//Insertamos los datos a la base de datos
		$this->db->insert_batch("subTramite",$data_giro);
	}//up

	public function down() {
		$this->dbforge->drop_table("subTramite");
	}//down

}//class