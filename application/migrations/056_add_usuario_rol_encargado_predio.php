<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_usuario_rol_encargado_predio extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {

		$record =  array(
			array("id_rol"=>25,"id_usuario" =>27)
		);//dataRol
		$this->db->insert_batch("usuario_rol", $record);
	}

	public function down() {
		//se elimina el registro
		$this->db->where('id', 27);
   		$this->db->delete('usuario_rol');
	}//down
}//class

/* End of file 053_add_usuario_rol_encargado_predio.php */
/* Location: ./application/migrations/053_add_usuario_rol_encargado_predio.php */
