<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Solicitud_Correccion extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	    'id' => array(
	      'type' => 'INT',
	      'constraint' => 11,
	      'unsigned' => TRUE,
	      'auto_increment' => TRUE,
	    ),
	    'id_solicitud' => array(
	      'type' => 'INT',
	      'constraint' => 11,
	      'unsigned' => TRUE,
	    ),
	    'id_funcionario' => array(
	      'type' => 'INT',
	      'constraint' => 11,
	      'unsigned' => TRUE,
	    ),
	    'observaciones' => array(
	      'type' => 'VARCHAR',
		    'constraint' => '255',
	      'NULL' => FALSE,
	    ),
	    'fecha' => array(
	      'type' => 'TIMESTAMP',
				'NULL' => FALSE
	    ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('solicitud_correccion');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `solicitud_correccion` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud_correccion` ADD FOREIGN KEY (`id_funcionario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
	}//up

	public function down() {
		$this->dbforge->drop_table("solicitud_correccion");
	}//down

}//class

/* End of file 038_add_solicitud_correccion.php */
/* Location: ./application/migrations/038_add_solicitud_correccion.php */
