<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_tipo_pago extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }

    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_solicitud' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => true,
            ),

            'folio'   => array(
                'type'       => 'VARCHAR',
                'constraint' => 28,
                'unsigned'   => true,
                'null'       => false,
            ),

            'corte'          => array(
                'type'       => 'INT',
                'null'       => false,
            ),

  	        'fecha' => array(
  	                'type' => 'DATE',
  	                "null" => TRUE,
            ),

            'tipo_pago'  =>array(
                    'type' => 'CHAR',
                    'constraint' => 11,
                    'null' => false,
            ),
            

        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('tipo_pago');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `tipo_pago` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
       } //up

    public function down()
    {
        $this->dbforge->drop_table("tipo_pago");
    } //down

} //class

/* End of file 033_add_pago.php */
/* Location: ./application/controllers/033_add_pago.php */
