<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_usuariosopinion extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }


    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_solicitud' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'id_funcionario' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
            ),
            'estatus' => array(
                    'type' => 'tinyint',
                    'constraint' => 1,
                    'unsigned' => true,
		                'default' => 1,
            ),'fecha_dictamen' => array(
                'type' => 'date',
                'unsigned' => true,
        ),
        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('usuariosopinion');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `usuariosopinion` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE CASCADE ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `usuariosopinion` ADD FOREIGN KEY (`id_funcionario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("usuariosopinion");
    } //down

} //class

/* End of file 041_add_usuariosopinion.php */
/* Location: ./application/controllers/041_add_usuariosopinion.php */
