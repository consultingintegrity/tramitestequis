<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_persona extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'primer_nombre' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> FALSE,
		        ),
		        'segundo_nombre' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> TRUE,
		        ),
		        'primer_apellido' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> FALSE,
		        ),
		        'segundo_apellido' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '45',
		                'null'	=> TRUE,
		        ),
		        'fecha_nacimiento' => array(
		                'type' => 'DATE',
		                'null'	=> TRUE,
		        ),
		        'genero' => array(
		                'type' => 'CHAR',
		                'constraint' => '1',
		                'null'	=> TRUE,
		        ),
		        'id_municipio' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		        ),
		        'calle' => array(
		                'type' => 'CHAR',
		                'constraint' => '50',
		                'null'	=> TRUE,
		        ),
		        'colonia' => array(
		                'type' => 'CHAR',
		                'constraint' => '50',
		                'null'	=> TRUE,
		        ),
		        'numero_interior' => array(
		                'type' => 'CHAR',
		                'constraint' => '15',
		                'null'	=> TRUE,
		        ),
		        'numero_exterior' => array(
		                'type' => 'CHAR',
		                'constraint' => '15',
		                'null'	=> TRUE,
		        ),
		        'rfc' => array(
		                'type' => 'CHAR',
		                'constraint' => '13',
		                'null'	=> TRUE,
		        ),
		        'razon_social' => array(
		                'type' => 'CHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'tipo_persona' => array(
		                'type' => 'TINYINT',
		                'constraint' => '1',
		                'null'	=> TRUE,
		        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('persona');
		//Agregamos la clavce foranea
		$this->db->query("ALTER TABLE `persona` ADD FOREIGN KEY (`id_municipio`) REFERENCES `municipio`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		 //creamos un array con los datos de los paìses
		 $data_persona = array(
			//admin
			array("primer_nombre"=>'Administrador',"segundo_nombre"=>"","primer_apellido" =>'Administrador',"segundo_apellido" =>"ESTRELLA","id_municipio" => 17),//1//Administrador
			//DU
			array("primer_nombre"=>"MARIELL","segundo_nombre"=>"","primer_apellido" =>"MORAN","segundo_apellido" =>"ESTRELLA","id_municipio" => 17),//2//ventanillaDU
			array("primer_nombre"=>"MIGUEL","segundo_nombre"=>"","primer_apellido" =>"TREJO","segundo_apellido" =>"GARCÍA","id_municipio" => 17),//3//EncargadoNFDU
			array("primer_nombre"=>"LUIS","segundo_nombre"=>"EDUARDO","primer_apellido" =>"BARAJAS","segundo_apellido" =>"ORTIZ","id_municipio" => 17),//4//DirectorDU
			array("primer_nombre"=>"MIGUEL","segundo_nombre"=>"","primer_apellido" =>"CABRERA","segundo_apellido" =>"LOPEZ","id_municipio" => 17),//5//SecretarioDU
			array("primer_nombre"=>"ARMANDO","segundo_nombre"=>"","primer_apellido" =>"CEBALLOS","segundo_apellido" =>"ARRIAGA","id_municipio" => 17),//6//InspeccionDU
			array("primer_nombre"=>"GUSTAVO","segundo_nombre"=>"","primer_apellido" =>"CRUZ","segundo_apellido" =>"CABALLERO","id_municipio" => 17),//7//JefeInspeccionDU
			array("primer_nombre"=>"MARIO","segundo_nombre"=>"DE LA LUZ","primer_apellido" =>"HERNÁNDEZ","segundo_apellido" =>"REYES","id_municipio" => 17),//8//secretariaDU
			array("primer_nombre"=>"MIGUEL","segundo_nombre"=>"","primer_apellido" =>"TREJO","segundo_apellido" =>"GARCÍA","id_municipio" => 17),//9 //encargadoDictamen
			array("primer_nombre"=>"MARCO","segundo_nombre"=>"POLO","primer_apellido" =>"RAMIREZ","segundo_apellido" =>"VALENCIA","id_municipio" => 17),//10//SubDirectorDu
			array("primer_nombre"=>"Ventanilla","segundo_nombre"=>"","primer_apellido" =>"de Atención","segundo_apellido" =>"","id_municipio" => 17),//11//coordinacionVentanillaDU
			array("primer_nombre"=>"Opinión Técnica","segundo_nombre"=>"","primer_apellido" =>"Desarrollo Urbano","segundo_apellido" =>"","id_municipio" => 17),//12//OpinionTecnicaDU
			//usuarios para Protección Civil
			array("primer_nombre"=>"MARÍA","segundo_nombre"=>"GUADALUPE","primer_apellido" =>"HARNÁNDEZ","segundo_apellido" =>"PEREA","id_municipio" => 17),//13//VentanillaPC
			array("primer_nombre"=>"FERMÍN","segundo_nombre"=>"","primer_apellido" =>"GONZÁLEZ","segundo_apellido" =>"CANTERA","id_municipio" => 17),//14//InspeccionPC
			array("primer_nombre"=>"JORGE","segundo_nombre"=>"EDUARDO","primer_apellido" =>"ARCE","segundo_apellido" =>"SALINAS","id_municipio" => 17),//15//OpinionTecnicaPC
			//secretaria General
			array("primer_nombre"=>"BLANCA","segundo_nombre"=>"","primer_apellido" =>"LEDESMA","segundo_apellido" =>"GARCÍA","id_municipio" => 17),//16//VentanillaSG
			array("primer_nombre"=>"BLANCA","segundo_nombre"=>"ESTELA","primer_apellido" =>"MANCERA","segundo_apellido" =>"GUTIERREZ","id_municipio" => 17),//17//ComisionSG
			array("primer_nombre"=>"MARÍA","segundo_nombre"=>"GUADALUPE","primer_apellido" =>"HERNÁNDEZ","segundo_apellido" =>"GONZÁLEZ","id_municipio" => 17),//18//SecretariaSG
			array("primer_nombre"=>"MARIO","segundo_nombre"=>"","primer_apellido" =>"DORANTES","segundo_apellido" =>"NIETO","id_municipio" => 17),//19//SecretarioGeneralSG
			//finanzas
			array("primer_nombre"=>"GLORIA","segundo_nombre"=>"","primer_apellido" =>"VEGA","segundo_apellido" =>"VALENCIA","id_municipio" => 17),//20//CajaIn
			array("primer_nombre"=>"CAREN","segundo_nombre"=>"","primer_apellido" =>"CALIXTO","segundo_apellido" =>"ROMERO","id_municipio" => 17),//21 VentanillaIN
			array("primer_nombre"=>"CARLOS","segundo_nombre"=>"ALBERTO","primer_apellido" =>"RENTERIA","segundo_apellido" =>"RIVERA","id_municipio" => 17),//22//TesoreroIN
			array("primer_nombre"=>"Presidente Municipal","segundo_nombre"=>"","primer_apellido" =>"Ayuntamiento","segundo_apellido" =>"","id_municipio" => 17),//23//presidenteIN
			array("primer_nombre"=>"Opinión Técnica","segundo_nombre"=>"","primer_apellido" =>"Finanzas","segundo_apellido" =>"","id_municipio" => 17),//24//OpinonTecnicaIN
			array("primer_nombre"=>"VICTOR","segundo_nombre"=>"HUGO","primer_apellido" =>"MARTÍNEZ","segundo_apellido" =>"PACHECO","id_municipio" => 17),//25//InspeccionSecGob		
			//Ciudadano de prueba
			array("primer_nombre"=>"Usuario","segundo_nombre"=>"","primer_apellido" =>"Testing","segundo_apellido" =>"Testing","id_municipio" => 17),//26//ciudadano
		);
		 //ingresamos el registro en la base de datos
		 $this->db->insert_batch('persona', $data_persona);
	}//up

	public function down() {
		$this->dbforge->drop_table("persona");
	}//down

}//class

/* End of file 004_add_persona.php */
/* Location: ./application/migrations/004_add_persona.php */
