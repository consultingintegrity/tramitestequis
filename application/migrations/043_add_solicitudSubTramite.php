<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_SolicitudSubTramite extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_subTramite' => array(
	                'type' => 'INT',
			'constraint' => 11,
			'unsigned' => TRUE,
            ),
            
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('solicitudSubTramite');
		//Agregamos la clave foranea
        $this->db->query("ALTER TABLE `solicitudSubTramite` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `solicitudSubTramite` ADD FOREIGN KEY (`id_subTramite`) REFERENCES `subTramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    }//up    

	public function down() {
		$this->dbforge->drop_table("solicitudSubTramite");
	}//down

}//class