<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_atributoFormulario extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'id_formulario' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_atributo' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		    'value' => array(
	            'type' => 'CHAR',
	            'constraint' => 50,
	            'null' => TRUE
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);		
		//creamos la tabla
		$this->dbforge->create_table('atributo_formulario');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `atributo_formulario` ADD FOREIGN KEY (`id_formulario`) REFERENCES `formulario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `atributo_formulario` ADD FOREIGN KEY (`id_atributo`) REFERENCES `atributo`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//creamos regitros
		$data_regisros = array(
			array("id_formulario" => 1,"id_atributo"=>1, "value" => "UTF-8"),
			array("id_formulario" => 1,"id_atributo"=>3, "value" => "on"),
			array("id_formulario" => 2,"id_atributo"=>1, "value" => "UTF-8"),
			array("id_formulario" => 2,"id_atributo"=>3, "value" => "on"),
			array("id_formulario" => 7,"id_atributo"=>4, "value" => "multipart/form-data"),
			array("id_formulario" => 11,"id_atributo"=>4, "value" => "multipart/form-data")


		);
		//insertamos ls registro
		$this->db->insert_batch("atributo_formulario",$data_regisros);
		
	}//u

	public function down() {
		$this->dbforge->drop_table("atributo_formulario");
	}//down

}//class

/* End of file 012_add_atributoFormulario.php */
/* Location: ./application/migrations/012_add_atributoFormulario.php */