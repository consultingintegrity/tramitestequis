<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_Solicitud_Giro extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'impacto' => array(
	                'type' => 'VARCHAR',
	                'constraint' => 25,
	                'NULL' => TRUE,
	        ),
	        'id_solicitud' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
	        'id_giro' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('solicitud_giro');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `solicitud_giro` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `solicitud_giro` ADD FOREIGN KEY (`id_giro`) REFERENCES `giro`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
	}//up

	public function down() {
		$this->dbforge->drop_table("solicitud_giro");
	}//down

}//class

/* End of file 017_add_solicitud_giro.php */
/* Location: ./application/migrations/017_add_concepto.php */
