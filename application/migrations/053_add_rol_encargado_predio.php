<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_rol_encargado_predio extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		//Insertamos registro en la tabla ROL
		$record =  array(
			array("nombre"=>"Predio Base de Datos","descripcion"=>"Cargar predio de Excel en base de datos del sistema")//25
		);//dataRol
		$this->db->insert_batch("rol", $record);
	}//up

	public function down() {
		//se elimina el registro
		$this->db->where('id', 25);
   		$this->db->delete('rol');  
	}//down

}//class

/* End of file 050_add_rol_encargado_predio.php */
/* Location: ./application/migrations/050_add_rol_encargado_predio.php */