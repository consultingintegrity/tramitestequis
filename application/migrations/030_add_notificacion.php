<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_notificacion extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'estatus' => array(
		                'type' => 'INT',
		                'constraint' => '4',
		                'null'	=> FALSE,
		        ),
		        'id_solicitud' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
                ),
                'id_usuario' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
            	),
						'contenido' => array(
						'type' => 'VARCHAR',
						'constraint' => 250
				),

				'fecha' => array(
            				'type' => 'TIMESTAMP',
                			"null" => FALSE,
							),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('notificacion');
        $this->db->query("ALTER TABLE `notificacion` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `notificacion` ADD FOREIGN KEY (`id_usuario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `notificacion` CHANGE `fecha` `fecha` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP;");
         //creamos un array con los datos de los municipios
		 //ingresamos el registro en la base de datos

	}//up

	public function down() {
		$this->dbforge->drop_table("notificacion");
	}//down

}//class

/* End of file 030_add_notificacion.php */
/* Location: ./application/migrations/030_add_notificacion.php */
