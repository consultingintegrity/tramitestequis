<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_formulario extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
		        'id' => array(
		                'type' => 'INT',
		                'constraint' => 11,
		                'unsigned' => TRUE,
		                'auto_increment' => TRUE,
		        ),
		        'name' => array(
		                'type' => 'VARCHAR',
		                'constraint' => '100',
		                'unique' => TRUE,
		                'null'	=> FALSE,
		        ),
		        'method' => array(
		                'type' => 'CHAR',
		                'constraint' => '5',
		                'null'	=> FALSE,
		        ),
		        'action' => array(
		                'type' => 'CHAR',
		                'constraint' => '100',
		                'null'	=> TRUE,
		        ),
		        'title' => array(
		                'type' => 'CHAR',
		                'constraint' => '100',
		                'null'	=> TRUE
		        )
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('formulario');
		//creamos registros para la tabla
		$data_form = array(
			array("name"=>"frm_login","method"=>"POST","action"=>"Auth/login","title"=>"Login"),//1
			array("name"=>"frm_ins_cdo","method"=>"POST","action"=>"Ciudadano/registro","title"=>"Registro"),//2
			array("name"=>"frm_cambiar_pwd","method"=>"POST","action"=>"Usuarios/cambiarContrasenia","title"=>"Modificar contraseña"),//3
			array("name"=>"frm_recupera_pwd","method"=>"POST","action"=>"Ciudadano/resuperaContrasenia","title"=>"Recuperar contraseña"),//4
			array("name"=>"frm_datos_persona","method"=>"POST","action"=>"Usuarios/cambiarDatosPersona","title"=>"Modificar datos"),//5
			array("name"=>"frm_datos_usuario","method"=>"POST","action"=>"Usuarios/cambiarDatosUsuario","title"=>"Modificar datos de contacto"),//6
			array("name"=>"frm_img_perfil","method"=>"POST","action"=>"Usuarios/cambiarImagenPerfil","title"=>"Modificar imagen"),//7
			array("name"=>"frm_ins_func","method"=>"POST","action"=>"Usuarios/registro","title"=>"Registrar"),//8
			array("name"=>"frm_datos_solicitante","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos del Solicitante"),//9
			array("name"=>"frm_no_oficial","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos del predio"),//10
			array("name"=>"frm_requisitos_no_oficial","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Requisitos de la Solicitud"),//11
			array("name"=>"frm_persona_confianza","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos de la Persona de Confianza"),//12
			array("name"=>"frm_inspeccion_subir","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Archivo de Inspección"),//13
			array("name"=>"frm_revision_inspeccion","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Documento de Inspección"),//14
			array("name"=>"frm_asignacion_numero_oficial","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Asignación de Número Oficial"),//15
			array("name"=>"frm_firma_direccionDU","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Firma Director"),//16
			array("name"=>"frm_firma_secretario","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Firma del Secretario"),//17
			array("name"=>"frm_documento_firmado","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Recepción de documento firmado"),//18
			array("name"=>"frm_generar_orden","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Orden de pago"),//19
			array("name"=>"frm_ubicacion_predio","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Ubicación del predio"),//20
			array("name"=>"frm_uso_de_suelo","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Dictamen de uso de suelo"),//21
			array("name"=>"frm_uso_habitacional","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Uso Habitacional"),//22
			array("name"=>"frm_asignacion_dictamen_uso_suelo","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Dictamen Uso de Suelo"),//23
			array("name"=>"frm_vobo_proteccion_civil","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Visto bueno protección civil"),//24
			array("name"=>"frm_recivo_pago","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Recibo de pago"),//25
			array("name"=>"frm_dts_contribuyente","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos del contribuyente"),//26
			array("name"=>"frm_dts_negocio","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos del negocio"),//27
			array("name"=>"frm_secretario_finanzas_publicas_municipales","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Firma Secretario de Finanzas Públicas Municipales"),//28
			array("name"=>"frm_presidente","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Firma presidente"),//29
			array("name"=>"frm_licencia_funcionamiento","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Licencia de funcionamiento"),//30
			array("name"=>"frm_factibilidad_giro","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Factibilidad de giro"),//31
			//formularios para licencias de construcción
			array("name"=>"frm_antecedentes","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Antecedentes"),//32
			array("name"=>"frm_datos_obra","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos de la Obra"),//33
			array("name"=>"frm_propietario","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Propietario"),//34
			array("name"=>"frm_perito_responsable","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Perito Responsable"),//35
			array("name"=>"frm_informacion_adicional_estadistica","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Información Adicional Estadística"),//36
			array("name"=>"frm_datos_perito","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos del Perito"),//37
			array("name"=>"frm_nota_importante","method"=>"POST","action"=>"Solicitud/inserta","title"=>"NOTA IMPORTANTE"),//38
			//Formularios para terminación de obra
			array("name"=>"frm_terminacion_obra","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos de la solicitud"),//39
			array("name"=>"frm_supervision_obra","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Revisión y Autorización de Obra"),//40
			array("name"=>"frm_licencia_construccion","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Licencia de Construcción"),//41
			//Formulario Uso Comercial
			array("name"=>"frm_uso_comercial","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Uso Comercial"),//42
			//Formulario Uso Servicios
			array("name"=>"frm_uso_servicios","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Uso Servicios"),//43
			//Formulario informe uso de suelo
			array("name"=>"frm_informe_uso_suelo","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Informe de uso de suelo"),//44
			array("name"=>"frm_opinion_tecnica","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Opinión técnica"),//45
			array("name"=>"frm_revision_opinion","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Revisión de opinión técnica"),//46
			array("name"=>"frm_sube_dictamen","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Dictamen"),//47
			array("name"=>"frm_revision_dictamen","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Dictamen"),//48
			array("name"=>"frm_sube_informe","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Informe Cabildo"),//49
			array("name"=>"frm_revision_informe","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Informe cabildo"),//50
			array("name"=>"frm_solicita_opinion","method"=>"POST","action"=>"Solicitud/inserta","title"=>"solicita opinión técnica"),//51
			//Formulario para subir opinión técnica
			array("name"=>"frm_doc_opinion_tecnica","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Subir archivo de opinión técnica"),//52
			//para subir provicional de alcohol
			array("name"=>"frm_carga_permiso","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Permiso Provicional (Alcohol)"),//53
			array("name"=>"frm_muestra_permiso","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Permiso Provicional (Alcohol)"),//54
			array("name"=>"frm_firma_subdirector","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Firma de Sub Director"),//55
			array("name"=>"frm_datos_negocio","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Nombre del Negocio"),//56
			array("name"=>"frm_datos_solicitudNOOficial","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos del Solicitante"),//57
			array("name"=>"frm_datos_evento","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos de ubicación del evento"),//58
			//formulario de razon social
			array("name"=>"frm_razon_social","method"=>"POST","action"=>"Solicitud/inserta","title"=>"Datos de Razón Social"),//59
		);//data_form
		//Insertamos los datos a la tabla
		$this->db->insert_batch("formulario",$data_form);
	}//up

	public function down() {
		$this->dbforge->drop_table("formulario");
	}//down

}//class

/* End of file 010_add_formulario.php */
/* Location: ./application/migrations/010_add_formulario.php */
