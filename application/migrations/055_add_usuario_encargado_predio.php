<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_usuario_encargado_predio extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {

		$record =  array(
			array("correo_electronico"=>"predioDB@tx.com.mx","contrasenia" => hash('sha512', "predioDB123"),"id_persona" => 27,"id_dependencia"=>5,"status" => 1),//27
		);//dataRol
		$this->db->insert_batch("usuario", $record);		
		
	}//up

	public function down() {
		//se elimina el registro
		$this->db->where('id', 27);
   		$this->db->delete('usuario'); 
	}//down

}//class

/* End of file 052_add_usuario_encargado_predio.php */
/* Location: ./application/migrations/052_add_usuario_encargado_predio.php */