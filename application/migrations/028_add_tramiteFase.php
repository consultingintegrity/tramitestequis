<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_tramiteFase extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'genera_documento' => array(
	                'type' => 'TINYINT',
	                'constraint' => 1,
	                'unsigned' => TRUE,
	        ),
	        'pide_documento' => array(
	                'type' => 'TINYINT',
	                'constraint' => 1,
	                'unsigned' => TRUE,
	        ),
	        'id_fase' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'unsigned' => TRUE,
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('tramite_fase');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `tramite_fase` ADD FOREIGN KEY (`id_fase`) REFERENCES `fase`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `tramite_fase` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//creamos los registros para la tabla
		$data_tramite_fase = array(
			//No Oficial
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>1,"id_tramite"=>1),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>2,"id_tramite"=>1),//fase ventanilla valida info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>3,"id_tramite"=>1),//fase inspección realiza y sube inspeccion
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>4,"id_tramite"=>1),//fase jefe de inspeccion valida inspeccion
			array("genera_documento"=>1,"pide_documento"=>1,"id_fase"=>5,"id_tramite"=>1),//fase encargado numeros oficiales genera NO. oficial
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>6,"id_tramite"=>1),//fase sub Director firma No. oficial
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>7,"id_tramite"=>1),//fase Director firma No. oficial
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>8,"id_tramite"=>1),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>9,"id_tramite"=>1),//fase caja sella pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>10,"id_tramite"=>1),//fase ventanilla entrega No.oficial
			//Dictamen usao de Suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>11,"id_tramite"=>2),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>12,"id_tramite"=>2),//fase ventanilla valida info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>13,"id_tramite"=>2),//fase inspección realiza y sube inspeccion
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>14,"id_tramite"=>2),//fase jefe de inspeccion valida inspeccion
			array("genera_documento"=>1,"pide_documento"=>1,"id_fase"=>15,"id_tramite"=>2),//fase encargado Dctamen uso de suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>16,"id_tramite"=>2),//fase sub Director firma  Ditamen uso e suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>17,"id_tramite"=>2),//fase Director firma  Ditamen uso e suelo
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>18,"id_tramite"=>2),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>19,"id_tramite"=>2),//fase caja sella pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>20,"id_tramite"=>2),//fase ventanilla entrega No.oficial
			//Protección Civil
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>21,"id_tramite"=>6),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>22,"id_tramite"=>6),//fase ventanilla valida info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>23,"id_tramite"=>6),//fase inspección realiza y sube inspeccion
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>24,"id_tramite"=>6),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>25,"id_tramite"=>6),//fase caja sella pago
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>26,"id_tramite"=>6),//fase ventanilla
			//licencia de funcionamiento
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>28,"id_tramite"=>4),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>29,"id_tramite"=>4),//fase ventanilla valida info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>30,"id_tramite"=>4),//fase inspección realiza y sube inspeccion
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>31,"id_tramite"=>4),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>32,"id_tramite"=>4),//fase caja sella pago
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>33,"id_tramite"=>4),//fase ventanilla imprime licencia
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>34,"id_tramite"=>4),//fase tesorero firma
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>35,"id_tramite"=>4),//fase presidente firma
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>36,"id_tramite"=>4),//fase ventanilla entrega licencia
			//Factibilidad de giro
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>37,"id_tramite"=>3),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>38,"id_tramite"=>3),//fase ventanilla valida
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>39,"id_tramite"=>3),//info fase inspección sube archivod e inspección
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>40,"id_tramite"=>3),//fase jefe de inspeccion valida documentacion
			array("genera_documento"=>1,"pide_documento"=>1,"id_fase"=>41,"id_tramite"=>3),//fase ventanilla genera factibilidad de giro
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>42,"id_tramite"=>3),//fase Director firma factibilidad de giro
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>43,"id_tramite"=>3),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>44,"id_tramite"=>3),//fase caja sella pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>45,"id_tramite"=>3),//fase ventanilla entrega factibilidad de giro
			//Licencia de Construcción
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>46,"id_tramite"=>7),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>47,"id_tramite"=>7),//fase ventanilla valida
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>48,"id_tramite"=>7),//info fase inspección sube archivo de inspección
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>49,"id_tramite"=>7),//fase jefe de inspeccion valida documentacion
			array("genera_documento"=>1,"pide_documento"=>1,"id_fase"=>50,"id_tramite"=>7),//fase ventanilla genera Licencia de Construcción
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>51,"id_tramite"=>7),//fase Director firma fLicencia de Construcción
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>52,"id_tramite"=>7),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>53,"id_tramite"=>7),//fase caja sella pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>54,"id_tramite"=>7),//fase ventanilla entrega licencia de construcción
			//informe uso de suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>55,"id_tramite"=>8),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>56,"id_tramite"=>8),//fase ventanilla valida
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>57,"id_tramite"=>8),//info fase inspección sube archivo de inspección
			//array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>58,"id_tramite"=>8),//fase área jurídica valida documentacion
			array("genera_documento"=>1,"pide_documento"=>1,"id_fase"=>59,"id_tramite"=>8),//fase ventanilla genera informe uso de suelo
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>60,"id_tramite"=>8),//fase Director firma informe uso de suelo
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>61,"id_tramite"=>8),//fase ventanilla genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>62,"id_tramite"=>8),//fase caja sella pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>63,"id_tramite"=>8),//fase ventanilla entrega informe uso de suelo

			//licencia de Alcoholes
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>64,"id_tramite"=>5),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>65,"id_tramite"=>5),//fase ventanilla valida info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>66,"id_tramite"=>5),//fase comision recibe requisitos
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>67,"id_tramite"=>5),//fase Comisión elabora dictamen
			array("genera_documento"=>0,"pide_documento"=>1,"id_fase"=>68,"id_tramite"=>5),//fase secretaria sube documento
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>69,"id_tramite"=>5),//fase VENTANILLA genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>70,"id_tramite"=>5),//fase caja aprueba soliciud
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>71,"id_tramite"=>5),//fase ventanilla entrega permiso
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>72,"id_tramite"=>5),//fase opinión técnica sube documento

			//licencia de Alcoholes Provicional
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>73,"id_tramite"=>9),//fase ciudadano sube info
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>74,"id_tramite"=>9),//fase ventanilla valida info
			array("genera_documento"=>1,"pide_documento"=>0,"id_fase"=>75,"id_tramite"=>9),//fase VENTANILLA genera orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>76,"id_tramite"=>9),//fase caja aprueba soliciud
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>77,"id_tramite"=>9),//fase ventanilla envia a secretario
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>78,"id_tramite"=>9),//fase Secretario carga el permiso Provicional
			array("genera_documento"=>0,"pide_documento"=>0,"id_fase"=>79,"id_tramite"=>9),//fase ventanilla entrega a ciudadano
		);
		//agregamos los registros a la base de datos
		$this->db->insert_batch("tramite_fase",$data_tramite_fase);
	}//up

	public function down() {
		$this->dbforge->drop_table("tramite_fase");
	}//down

}//class

/* End of file 028_add_tramiteFase.php */
/* Location: ./application/migrations/028_add_tramiteFase.php */
