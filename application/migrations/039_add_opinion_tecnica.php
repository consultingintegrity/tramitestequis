<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Migration_Add_opinion_tecnica extends CI_Migration
{

    public function __construct()
    {
        $this->load->dbforge();
        $this->load->database();
    }


    public function up()
    {
        $campos = array(
            'id'           => array(
                'type'           => 'INT',
                'constraint'     => 11,
                'unsigned'       => true,
                'auto_increment' => true,
            ),

            'id_solicitud' => array(
                'type'       => 'INT',
                'constraint' => 11,
                'unsigned'   => true,
                'null'       => false,
            ),
            'url' => array(
                    'type' => 'CHAR',
                    'constraint' => '180',
            ),
            'id_funcionario' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => TRUE,
            ),
        ); //campos
        //Agregamos los campos para crear la tabla
        $this->dbforge->add_field($campos);
        // agregamos PK `id` (`id`)
        $this->dbforge->add_key('id', true);
        //creamos la tabla
        $this->dbforge->create_table('opinion_tecnica');
        //se agregan las claves foraneas a la tabla
        $this->db->query("ALTER TABLE `opinion_tecnica` ADD FOREIGN KEY (`id_solicitud`) REFERENCES `solicitud`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
        $this->db->query("ALTER TABLE `opinion_tecnica` ADD FOREIGN KEY (`id_funcionario`) REFERENCES `usuario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
    } //up

    public function down()
    {
        $this->dbforge->drop_table("opinion_tecnica");
    } //down

} //class

/* End of file 039_add_opinion_tecnica.php */
/* Location: ./application/controllers/039_add_opinion_tecnica.php */
