<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_persona_encargado_predio extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$record =  array(
			array(
				"primer_nombre"=>"Predio",
				"primer_apellido"=>"Base de datos",
				"Genero"=>"M",
				"id_municipio"=>17)//27
		);//dataRol
		$this->db->insert_batch("persona", $record);
		
	}//up

	public function down() {		
		//se elimina el registro
		$this->db->where('id', 27);
   		$this->db->delete('persona'); 
	}//down

}//class

/* End of file 051_add_persona_encargado_predio.php */
/* Location: ./application/migrations/051_add_persona_encargado_predio.php */