<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_documento extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		$campos =  array(
	        'id' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	                'auto_increment' => TRUE,
	        ),
	        'nombre' => array(
	                'type' => 'CHAR',
	                'constraint' => '100',
	                'unique' => TRUE,
	                "null" => FALSE,
	        ),
	        'url' => array(
	                'type' => 'CHAR',
	                'constraint' => '180',
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => 11,
	                'unsigned' => TRUE,
	        ),
		);//campos
		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('documento');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `documento` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");

		//creamos registros para la tabla
		$data_document = array(
			array("nombre"=>"Alineamiento y Número Oficial","url"=>"Documento/genera","id_tramite"=>1),//1
				array("nombre"=>"Dictamen Uso de Suelo","url"=>"Documento/genera","id_tramite"=>2),//2
				array("nombre"=>"Visto Bueno de Protección Civil","url"=>"Documento/genera","id_tramite"=>6),//3
				array("nombre"=>"Licencia de funcionamiento","url"=>"Documento/genera","id_tramite"=>4),//4
				array("nombre"=>"Factibilidad de giro","url"=>"Documento/genera","id_tramite"=>3),//5
				array("nombre"=>"Licencia de Construcción","url"=>"Documento/genera","id_tramite"=>7),//6
				array("nombre"=>"Informe uso de suelo","url"=>"Documento/genera","id_tramite"=>8),//7
				array("nombre"=>"Permiso de bebidas alcoholicas","url"=>"Documento/genera","id_tramite"=>5),//8
				array("nombre"=>"Permiso Provicional de bebidas alcoholicas","url"=>"Documento/genera","id_tramite"=>9),//9

		);//data_document
		//Insertamos los datos a la tabla
		$this->db->insert_batch("documento",$data_document);
	}//up

	public function down() {
		$this->dbforge->drop_table("documento");
	}//down

}//class

/* End of file 020_add_documento.php */
/* Location: ./application/migrations/020_add_documento.php */
