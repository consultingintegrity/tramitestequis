<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Add_pestaniaForm extends CI_Migration {

	public function __construct()
	{
		$this->load->dbforge();
		$this->load->database();
	}

	public function up() {
		//creamos la estructura de la tabla
		$campos = array(
			"id"=>array(
				'type' => 'INT',
	            'constraint' => 11,
	            'unsigned' => TRUE,
	            'auto_increment' => TRUE
			),
			"genera_documento"=>array(
				'type' => 'TINYINT',
	            'constraint' => 1,
	            'unsigned' => TRUE
			),
			"pide_documento"=>array(
				'type' => 'TINYINT',
	            'constraint' => 1,
	            'unsigned' => TRUE
			),
	        'id_pestania' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'unsigned' => TRUE,
	        ),
	        'id_formulario' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'unsigned' => TRUE,
	        ),
	        'id_tramite' => array(
	                'type' => 'INT',
	                'constraint' => '11',
	                'unsigned' => TRUE,
	        )
		);//campos

		//Agregamos los campos para crear la tabla
		$this->dbforge->add_field($campos);
		// agregamos PK `id` (`id`)
		$this->dbforge->add_key('id', TRUE);
		//creamos la tabla
		$this->dbforge->create_table('pestania_form');
		//Agregamos la clave foranea
		$this->db->query("ALTER TABLE `pestania_form` ADD FOREIGN KEY (`id_formulario`) REFERENCES `formulario`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `pestania_form` ADD FOREIGN KEY (`id_pestania`) REFERENCES `pestania`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		$this->db->query("ALTER TABLE `pestania_form` ADD FOREIGN KEY (`id_tramite`) REFERENCES `tramite`(`id`) ON DELETE RESTRICT ON UPDATE RESTRICT;");
		//creamos los registros para la tabla
		$data_tramite_fase = array(
			//Alineamiento y Número Oficial
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>57,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>10,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>5,"id_formulario"=>15,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>6,"id_formulario"=>16,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>29,"id_formulario"=>55,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>7,"id_formulario"=>18,"id_tramite"=>1),
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>1),
			//Dictamen uso de Suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>9,"id_tramite"=>2),//Datos del solicitante
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>20,"id_tramite"=>2),//Ubicación del predio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>21,"id_tramite"=>2),//Dictamen uso de suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>22,"id_tramite"=>2),//Uso Habitacional
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>42,"id_tramite"=>2),//Uso Comercial
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>43,"id_tramite"=>2),//Uso Servicios
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>2),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>2),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>2),//requisitos de la solicitud
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>2),//INSPECCIÓN
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>2),//Revisión de Inspección
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>10,"id_formulario"=>23,"id_tramite"=>2),//Asignación Dictmen Uso de Suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>6,"id_formulario"=>16,"id_tramite"=>2),//Firma Sub Director
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>9,"id_formulario"=>17,"id_tramite"=>2),//Recepción documento Firmado
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>7,"id_formulario"=>18,"id_tramite"=>2),//Generación Orden de Pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>2),//Firma de Director
			//proteccion civil
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>9,"id_tramite"=>6),//Datos del solicitante
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>20,"id_tramite"=>6),//Ubicación del predio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>6),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>6),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>6),//requistos tramite
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>6),//inspecion
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>6),//Generación ordende pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>6),//Documento de inspección
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>12,"id_formulario"=>25,"id_tramite"=>6),//Generación orden de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>11,"id_formulario"=>24,"id_tramite"=>6),//Generación VoBo Protección Civil
			//Licencia de funcionamiento
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>26,"id_tramite"=>4),//Datos del contibuyente
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>27,"id_tramite"=>4),//Datos del negocio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>20,"id_tramite"=>4),//Ubicación del predio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>4),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>4),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>4),//requistos tramite
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>4),//reporte inspecion
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>4),//visualiza inspeccion
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>4),//Generación ordende pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>12,"id_formulario"=>25,"id_tramite"=>4),//Visualizción recibo de pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>13,"id_formulario"=>30,"id_tramite"=>4),//Generación licencia
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>14,"id_formulario"=>28,"id_tramite"=>4),//firma tesorero
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>15,"id_formulario"=>29,"id_tramite"=>4),//firma presidente
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>11,"id_formulario"=>24,"id_tramite"=>4),//Generación VoBo Protección Civil
			//Factibilidad de giro
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>9,"id_tramite"=>3),//Datos del solicitante
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>20,"id_tramite"=>3),//Ubicación del predio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>3),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>3),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>56,"id_tramite"=>3),//nombre del negocio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>3),//requisitos de la solicitud
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>16,"id_formulario"=>31,"id_tramite"=>3),//factibilidad de giro
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>6,"id_formulario"=>16,"id_tramite"=>3),//firma director
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>7,"id_formulario"=>18,"id_tramite"=>3),//Generación Orden de Pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>3),//pase a caja
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>3),//INSPECCIÓN
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>3),//visualiza inspeccion
			//Licencia de construcción
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>9,"id_tramite"=>7),//Datos del solicitante
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>20,"id_tramite"=>7),//Ubicación del predio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>32,"id_tramite"=>7),//ANTECEDENTES
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>33,"id_tramite"=>7),//DATOS DE LA OBRA
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>34,"id_tramite"=>7),//PROPIETARIO
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>35,"id_tramite"=>7),//PERITO RESPONSABLE
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>36,"id_tramite"=>7),//INFORMACION ADICIONAL Y ESTADISTICA
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>37,"id_tramite"=>7),//DATOS DEL PERITO
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>39,"id_tramite"=>7),//DATOS DE SOLICITUD TERMINAIÓN DE OBRA
//			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>40,"id_tramite"=>7),//REVISIÓN Y SUPERVISIÓN
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>7),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>7),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>7),//requisitos de la solicitud
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>17,"id_formulario"=>41,"id_tramite"=>7),//licencia de construcción
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>6,"id_formulario"=>16,"id_tramite"=>7),//firma director
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>7,"id_formulario"=>18,"id_tramite"=>7),//Generación Orden de Pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>7),//pase a caja
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>7),//INSPECCIÓN
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>7),//visualiza inspeccion
			//informe uso de SUELO
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>9,"id_tramite"=>8),//Datos del solicitante
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>20,"id_tramite"=>8),//Ubicación del predio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>8),//Requisitos de la solicitud
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>8),//confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>8),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>3,"id_formulario"=>13,"id_tramite"=>8),//Inspección
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>4,"id_formulario"=>14,"id_tramite"=>8),//visualiza inspeccion
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>18,"id_formulario"=>44,"id_tramite"=>8),//Informe uso de suelo
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>6,"id_formulario"=>16,"id_tramite"=>8),//Firma director
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>7,"id_formulario"=>18,"id_tramite"=>8),//Generación Orden de Pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>8),//Pase a caja

			//Licencia de alcoholes
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>26,"id_tramite"=>5),//Datos del contibuyente
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>27,"id_tramite"=>5),//Datos del negocio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>5),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>5),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>5),//requistos tramite
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>19,"id_formulario"=>45,"id_tramite"=>5),//opinion tecnica
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>20,"id_formulario"=>46,"id_tramite"=>5),//Revision de opinion tecnica
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>21,"id_formulario"=>47,"id_tramite"=>5),//Sube dictamen
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>22,"id_formulario"=>48,"id_tramite"=>5),//Revision de dictamen
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>23,"id_formulario"=>49,"id_tramite"=>5),//Sube informe cabildo
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>24,"id_formulario"=>50,"id_tramite"=>5),//Revision informe cabildo
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>25,"id_formulario"=>51,"id_tramite"=>5),//solicita opinion tecnica
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>7,"id_formulario"=>18,"id_tramite"=>5),//Generación Orden de Pago
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>5),//Pase a caja
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>26,"id_formulario"=>52,"id_tramite"=>5),//Opinión Técnica

			//Licencia de alcoholes (Provicional)
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>26,"id_tramite"=>9),//Datos del contibuyente
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>58,"id_tramite"=>9),//Datos del negocio
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>12,"id_tramite"=>9),//persona de confianza
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>1,"id_formulario"=>59,"id_tramite"=>9),//razon social
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>2,"id_formulario"=>11,"id_tramite"=>9),//requistos tramite
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>8,"id_formulario"=>19,"id_tramite"=>9),//Pase a caja
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>12,"id_formulario"=>25,"id_tramite"=>9),//visualizo pase a caja
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>27,"id_formulario"=>53,"id_tramite"=>9),//permiso provicionla
			array("genera_documento"=>0,"pide_documento"=>0,"id_pestania"=>28,"id_formulario"=>54,"id_tramite"=>9),//Visualizo permiso provicionla


		);
		//agregamos los registros a la base de datos
		$this->db->insert_batch("pestania_form",$data_tramite_fase);

	}

	public function down() {
		$this->dbforge->drop_table("pestania_form");
	}//down

}//class

/* End of file 029_add_pestaniaForm.php */
/* Location: ./application/migrations/029_add_pestaniaForm.php */
