<?php
defined('BASEPATH') or exit('No direct script access allowed');
require_once APPPATH . 'third_party/BBVA/Bbva.php';
class Paybanco extends CI_Controller
{
  private $urlRedirect;
  private $urlRedirect2;
  private $moneda;
  private $afiliacion;
  private $id;
  private $key;

  public function __construct()
  {
    parent::__construct();
    //Codeigniter : Write Less Do More
    $this->load->model('Predial_M');
    //Bbva::setProductionMode(true);
    Bbva::setSandboxMode(true);
    $this->urlRedirect = base_url('Paybanco/respuesta');
    $this->urlRedirect2 = base_url('Paybanco/respuesta_tramite');
    $this->moneda = 'MXN';
    $this->afiliacion = '288420';
    $this->id = 'mw4aagmdysl4q2i4h6iv';
    $this->key = 'sk_aa6b1e56e8e545c4978f7c65ed078bec';
  }

  public function index()
  {
    //obtengo los datos de la clave para proceder a hacer el pago
    $claveCatastral = $_POST['clavemodal'];
    $claveCatastral = str_replace(' ', '', $claveCatastral);
    $dataPredial = $this->Predial_M->ConsultaInfo($claveCatastral);
    //validamos que exista info, sino refrescamos
    if ($dataPredial != null) {
      $data["valid"] = 0;
      $data["title"] = "Pago del Predial";
      $data["clave"] = $dataPredial->ClaveCatastral;
      $this->load->view('head', $data);
      $this->load->view('header');
      $this->load->view('predial/formPago', $data);
      $this->load->view('footer');
      $this->load->view('scripts/js');
      $this->load->view('scripts/utils');
      $this->load->view('scripts/predio');
    } else {
      redirect(base_url() . "Predial", 'refresh');
    }
  }

  //aqui comienza la transacción al banco
  public function generarPago()
  {
    //obtengo los datos
    $jsonResponse['estatus'] = 'fail'; 
    $name  = (!empty($this->input->post("nombre"))) ? addslashes($this->input->post("nombre")) : "";
    $MSI  = (!empty($this->input->post("msi"))) ? addslashes($this->input->post("msi")) : 0;
    $lastname = (!empty($this->input->post("apellido"))) ? addslashes($this->input->post("apellido")) : "";
    $number = (!empty($this->input->post("numero"))) ? addslashes($this->input->post("numero")) : "";
    $mail = (!empty($this->input->post("correo"))) ? addslashes($this->input->post("correo")) : "";
    $claveCatastral = (!empty($this->input->post("claveCatastral"))) ? addslashes($this->input->post("claveCatastral")) : "";
    $dataPredial = $this->Predial_M->ConsultaInfo($claveCatastral);
    //datos requeridos para la transacción
    
    $fechaEmision = date('Y-m-d');
    $dia = date('d', strtotime("+1 days"));
    $fecha_vencimientoTabla = date("Y-m-") . $dia;

    $nomPersona = $dataPredial->nombreContribuyente;
    $monto = $dataPredial->total;
    $respuesta = $this->Predial_M->insertaPredialSol($nomPersona, $claveCatastral, "", "", $monto, "", $fechaEmision, $fecha_vencimientoTabla);
    
    if ($respuesta == true) {
      $idpredioSol = $this->Predial_M->consultaActual($claveCatastral);
      $fechaFol = date("dmy", strtotime($fechaEmision));
      $folio = 'OP_' . $idpredioSol->id . "-" . $fechaFol;
      $tr = $this->Predial_M->updatetrans($folio, $idpredioSol->id);
      //$afiliacion = '416276';//Debe contener el número de afiliación.
      //$monto = $dataPredial->$total;//Cantidad del cargo. Debe ser una cantidad mayor a cero, con hasta dos dígitos decimales
      $descripcion = 'Pago de Predial (TequisDigital)'; //Una descripción asociada al cargo.
      //$moneda = 'MXN';//Tipo de moneda del cargo Por el momento solo se soportan 2 tipos de monedas: Pesos Mexicanos(MXN) y Dólares Americanos(USD).
      //$idpredioSol="";//Identificador único del cargo. Debe ser único entre todas las transacciones
      //$name="";//Información del cliente al que se le realiza el cargo
      //$lastname="";//apellidos
      //$urlRedirect = base_url().'Paybanco/respuesta';//Usado para cargos de tipo redirect. Indica la url a la que redireccionar despues de una transaccion exitosa en el fomulario de pago de BBVA
      if ($MSI == 'true') {
        $chargeRequest = array(
          'affiliation_bbva' => $this->afiliacion, //req
          'amount' => $monto, //req
          'description' => $descripcion, //req
          'currency' => $this->moneda, //Opt
          'order_id' => $folio, //req
          'redirect_url' => $this->urlRedirect, //req
          'customer' => array( //req
            'name' => $name,
            'last_name' => $lastname,
            'email' => $mail,
            'phone_number' => $number
          ),
          'payment_plan' => array( //req
            'payments' => 6,
            'payments_type' => "WITHOUT_INTEREST"
          )
        );
      } else if ($MSI == 'false') {
        $chargeRequest = array(
          'affiliation_bbva' => $this->afiliacion, //req
          'amount' => $monto, //req
          'description' => $descripcion, //req
          'currency' => $this->moneda, //Opt
          'order_id' => $folio, //req
          'redirect_url' => $this->urlRedirect, //req
          'customer' => array( //req
            'name' => $name,
            'last_name' => $lastname,
            'email' => $mail,
            'phone_number' => $number
          )
        );
      }
      //iniciamos intancia
      $bbva = Bbva::getInstance($this->id, $this->key);
      try {
        
        //creamos la solicitud con la info del tarjetahabiente 
        $charge = $bbva->charges->create($chargeRequest);
        $charge = $charge != NULL ? $charge : null;
        
        if ($charge != null) {
          //validamos que sea una respuesta correcta o de error
          $val = (isset($charge->error_code)) ? true : false;
          if ($val == false) {
            //si es correcta guardamos el indentificador de la transacción
            $foliobanco = $charge->id;
            $resp = $this->Predial_M->updateFolio($foliobanco, $idpredioSol->id);
            
            //si todo es correcto procedemos a redirigir al VPOS
            if ($resp == true) {
              $url = $charge->payment_method;
              $jsonResponse["estatus"] = 200;
              $jsonResponse["msg"] = "Se procedera a pagar";
              $jsonResponse["url"] = $url->url;
            } else {
              $jsonResponse["estatus"] = 500;
              $jsonResponse["msg"] = "No se guardo la operación";
            }
          } else if ($val == true) {
            //si es una respuesta de error identificamos cual
            switch ($charge->error_code) {
              case 1000:
                $msg = "Ocurrió un error interno en el servidor";
                break;
              case 1001:
                $msg = "El formato de la petición no es JSON, los campos no tienen el formato correcto, o la petición no tiene campos que son requeridos.";
                break;
              case 1002:
                $msg = "La llamada no esta autenticada o la autenticación es incorrecta.";
                break;
              case 1003:
                $msg = "La operación no se pudo completar por que el valor de uno o más de los parametros no es correcto.";
                break;
              case 1004:
                $msg = "Un servicio necesario para el procesamiento de la transacción no se encuentra disponible.";
                break;
              case 1005:
                $msg = "Uno de los recursos requeridos no existe";
                break;
              case 1006:
                $msg = "Ya existe una transacción con el mismo ID de orden.";
                break;
              case 1007:
                $msg = "La transferencia de fondos entre una cuenta de banco o tarjeta y la cuenta no fue aceptada.";
                break;
              case 1008:
                $msg = "Una de las cuentas requeridas en la petición se encuentra desactivada";
                break;
              case 1009:
                $msg = "El cuerpo de la petición es demasiado grande.";
                break;

              default:
                $msg = "Algo salio terriblemente mal";
                break;
            }
            $jsonResponse["estatus"] = 500;
            $jsonResponse["msg"] = $msg;
          }
        } //si existe el charger
        else {
          $jsonResponse["estatus"] = 500;
          $jsonResponse["msg"] = "No se proceso tu solicitud";
        }
      } catch (\Throwable $th) {
        //throw $th;
        $jsonResponse["estatus"] = 500;
        $jsonResponse["msg"] = "No se proceso tu solicitud";
      }
    } else {
      //si el registro en solpredio falla
      $jsonResponse["estatus"] = 500;
      $jsonResponse["msg"] = "No se proceso tu solicitud";
    }
    echo json_encode($jsonResponse);
  }
  //respuesta del api bancomer
  public function respuesta()
  {
    $frm = 0;
    //valor enviado por el banco
    $transaccion = addslashes($_GET["id"]); //Identificador del cargo a consultar.
    //validamos que el id de transaccion sea igual al que se guardo o que exista
    $tr = $this->Predial_M->validTransaccion($transaccion);
    if (isset($tr)) {

      //iniciamos intancia
      $bbva = Bbva::getInstance($this->id, $this->key);
      //obtenemos toda la información de la transacción por medio del API
      $charge = $bbva->charges->get($tr->lineaCaptura);
      $status = $charge->status == "completed" ? 1 : null;
      if ($status != null) {
        //validamos si es una respuesta de error o correcta
        $val = ($charge->error_message == null and $charge->error_message == '') ? 1 : 0;

        if ($val == 1) {
          if ($pay = $this->Predial_M->uptadeEstPago($tr->id)) {
            $resp = "Pago Exitoso";
            $frm = 1;
            $doc = $this->generarRecibo($charge);
            $this->Predial_M->updateDoc($tr->id, $doc);
          } else {
            $resp = "No se actualizo el pago";
            $frm = 0;
          }
        } else if ($val == 0) {
          switch ($charge->error_code) {
            case 3001:
              $resp = "La tarjeta fue declinada.";
              break;
            case 3002:
              $resp = "La tarjeta ha expirado.";
              break;
            case 3003:
              $resp = "La tarjeta no tiene fondos suficientes.";
              break;
            case 3004:
              $resp = "La tarjeta ha sido identificada como una tarjeta robada.";
              break;
            case 3005:
              $resp = "La tarjeta ha sido identificada como una tarjeta fraudulenta.";
              break;
            case 3006:
              $resp = "La operación no esta permitida para este cliente o esta transacción.";
              break;
            case 3008:
              $resp = "La tarjeta no es soportada en transacciones en linea";
              break;
            case 3009:
              $resp = "La tarjeta fue reportada como perdida.";
              break;
            case 3010:
              $resp = "El banco ha restringido la tarjeta.";
              break;
            case 3011:
              $resp = "El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.";
              break;
            case 3012:
              $resp = "Se requiere solicitar al banco autorización para realizar este pago.";
              break;

            default:
              $resp = "Algo salio Terriblemente mal.";
              break;
          }
        } //val==false
      } //validamos que el charger existas
      else {
        $resp = "Ha ocurrido un problema";
      }
    } else //validamos que la transaccion exista
    {
      $resp = "La Transacción no existe.Vuelve a intentarlo";
    }
    $data['id'] = $tr->id;
    $data["status"] = $resp;
    $data["valid"] = 1;
    $data["frm"] = $frm;
    $data["title"] = "Pago del Predial";
    $this->load->view('head', $data);
    //$this->load->view('header');
    $this->load->view('predial/formPago', $data);
    $this->load->view('encuesta');
    $this->load->view('scripts/js');
    $this->load->view('scripts/utils');
    $this->load->view('scripts/predio');
    $this->load->view('scripts/encuesta');
    $this->load->view('footer');
  } //RESPUESTA

  public function descargar()
  {
    $id  = (!empty($this->input->post("id"))) ? addslashes($this->input->post("id")) : "";
    $url = $this->Predial_M->doc($id);
    if ($url) {
      //$url=$charge->payment_method;        
      $jsonResponse["estatus"] = 200;
      $jsonResponse["msg"] = "Se Descargara tu Recibo";
      $jsonResponse["url"] = $url->url;
    } else {
      $jsonResponse["estatus"] = 500;
      $jsonResponse["msg"] = "No se guardo la operación";
    }

    echo json_encode($jsonResponse);
  } //descargar doc

  public function generarRecibo($data)
  {
    if ($data != null) {
      $this->load->library('Pdf');

      $pdf = new Pdf();
      $pdf->AddPage();
      $pdf->SetMargins(15, 25, 15, 20);
      $pdf->AliasNbPages();

      $pdf->SetTitle(utf8_decode("Recibo de pago"));
      $pdf->SetFont('Times', 'B', 13);
      $pdf->Cell(0, 25, utf8_decode('MUNICIPIO DE TEQUISQUIAPAN'), 0, 0, 'C');
      $pdf->Ln(40);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('CONCEPTO:'), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode($data->description), 0, 0, 'L');
      $pdf->Ln(8);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('MONTO: '), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode('$' . $data->amount), 0, 0, 'L');
      $pdf->Ln(8);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('N° TARJETA: '), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode($data->card->card_number), 0, 0, 'L');
      $pdf->Ln(8);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('TARJETA: '), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode($data->card->bank_name), 0, 0, 'L');
      $pdf->Ln(8);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('CLIENTE: '), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode($data->card->holder_name), 0, 0, 'L');
      $pdf->Ln(8);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('N° OPERACIÓN: '), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode($data->id), 0, 0, 'L');
      $pdf->Ln(8);
      $pdf->SetFont('Arial', 'B', 12);
      $pdf->Cell(50, 0, utf8_decode('FECHA OPERACIÓN: '), 0, 0, 'L');
      $pdf->SetFont('Arial', '', 12);
      $pdf->Cell(50, 0, utf8_decode($data->operation_date), 0, 0, 'L');
      $pdf->Ln(2);
      $pdf->SetFont('Times', 'B', 6);
      $pdf->SetY(-30);
      $pdf->Cell(0, 5, utf8_decode('RESGUARDE ESTE DOCUMENTO PARA FUTURAS ACLARACIÓNES'), 0, 0, 'C');
      $pdf->Ln(3);
      $pdf->Cell(0, 5, utf8_decode('ESTE DOCUMENTO NO ES UN COMPROBANTE FISCAL'), 0, 0, 'C');
      /*
      * Se manda el pdf al navegador
      *
      * $this->pdf->Output(nombredelarchivo, destino);
      *
      * I = Muestra el pdf en el navegador
      * D = Envia el pdf para descarga
      * F = guarda el archivo dn una ruta
      */
      $name = "predialDocs/{$data->order_id}_{$data->id}.pdf";
      $pdf->close();
      $pdf->OutPut('' . $name, 'F');
    } else {
      $name = 0;
    }
    return $name;
  } //generar recibo

  public function pagoTramite()
  {
    $msg['msg'] = 'fail';
    $idSol = $this->input->post('idSol');
    $nombre = $this->input->post('desc');
    $this->load->model('TramitesConsulta');
    $tramite = $this->TramitesConsulta->TramitePago($idSol);

    $costo = $tramite->costo;
    $fecha = date('ymd', strtotime($tramite->fecha_inicio));
    $descripcion = $nombre;
    $respuesta = $this->TramitesConsulta->tramiteSol($idSol, $costo, $tramite->fecha_inicio);
    $folio = $idSol . $tramite->id_tramite . $fecha . $respuesta;

    $chargeRequest = array(
      'affiliation_bbva' => $this->afiliacion, //req
      'amount' => $costo, //req
      'description' => $descripcion, //req
      'currency' => $this->moneda, //Opt
      'order_id' => $folio, //req
      'redirect_url' => $this->urlRedirect2, //req
      'customer' => array( //req
        'name' => $this->session->userdata('primer_nombre'),
        'last_name' => $this->session->userdata('primer_apellido'),
        'email' => $this->session->userdata('correo_electronico'),
        'phone_number' => $this->session->userdata('telefono')
      ),
      // 'payment_plan' => array( //req
      //   'payments' => 6,
      //   'payments_type' => "WITHOUT_INTEREST"
      // )
    );
    $bbva = Bbva::getInstance($this->id, $this->key);
    //respuesta de bancomer
    try {
      $charge = $bbva->charges->create($chargeRequest);
      $url = $charge->payment_method;
      $url->url;
      if ($url->url) {
        $this->TramitesConsulta->update_tramite_sol($respuesta, $charge->id);
        $msg['msg'] = 'success';
        $msg['url'] = $url->url;
      }
    } catch (\Throwable $th) {
      //throw $th;
    }

    echo json_encode($msg);
  }
  public function respuesta_tramite()
  {
    $id_transaccion = $this->input->get('id');
    $this->load->model('TramitesConsulta');
    $id_sol = intval($this->TramitesConsulta->valid_id_transaccion($id_transaccion));
    $upd_doc = false;
    $resp = '';
    if ($id_sol > 0) {
      //iniciamos intancia
      $bbva = Bbva::getInstance($this->id, $this->key);
      //obtenemos toda la información de la transacción por medio del API
      $charge = $bbva->charges->get($id_transaccion);
      $resp = $charge->status = "completed" ? true : false;
      // if($resp){
      //   redirect(base_url().'misTramites');
      // }
      $val = ($charge->error_message == null and $charge->error_message == '') ? 1 : 0;
      if ($val == 1) {
        //Se actualiza la url del documento en la tabla del documento de la solicitud tramite pago.
        $doc = $this->generarRecibo($charge);
        if ($doc) {
          $upd_doc = $this->TramitesConsulta->update_doc_sol($id_transaccion, $doc);
          if ($upd_doc) {

            // redirect(base_url($doc));
          }
        }
        //Actualizar estatus de tramite a pagado.

      } else if ($val == 0) {
        switch ($charge->error_code) {
          case 3001:
            $resp = "La tarjeta fue declinada.";
            break;
          case 3002:
            $resp = "La tarjeta ha expirado.";
            break;
          case 3003:
            $resp = "La tarjeta no tiene fondos suficientes.";
            break;
          case 3004:
            $resp = "La tarjeta ha sido identificada como una tarjeta robada.";
            break;
          case 3005:
            $resp = "La tarjeta ha sido identificada como una tarjeta fraudulenta.";
            break;
          case 3006:
            $resp = "La operación no esta permitida para este cliente o esta transacción.";
            break;
          case 3008:
            $resp = "La tarjeta no es soportada en transacciones en linea";
            break;
          case 3009:
            $resp = "La tarjeta fue reportada como perdida.";
            break;
          case 3010:
            $resp = "El banco ha restringido la tarjeta.";
            break;
          case 3011:
            $resp = "El banco ha solicitado que la tarjeta sea retenida. Contacte al banco.";
            break;
          case 3012:
            $resp = "Se requiere solicitar al banco autorización para realizar este pago.";
            break;

          default:
            $resp = "Algo salio Terriblemente mal.";
            break;
        }
      } //val==false

    }
    if ($upd_doc) {
      redirect(base_url('Ciudadano/misTramites?sol=' . $id_sol . '&msg=' . $resp));
    } else {
      redirect(base_url('Ciudadano/misTramites?msg=' . $resp));
    }
  }
}//CLASS
