<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class DesgloceTramite extends CI_Controller {

	public function index($id_tramite = "")
	{
		//validamos que exista una sessión abierta y se consulte un tramite
		if ($this->session->userdata('logged_in') && $id_tramite != "") {
			//validamos que la sesión sea de un Ciudadano
			if ($this->session->userdata('rol')[0]->nombre == "Ciudadano") {
				//Cargamos el modelo de tramite
				$perfilado_form = $this->session->flashdata('formularios');
				$perfilado_requisitos = $this->session->flashdata('requisitos');
				$perfilado_subTramite = $this->session->flashdata('subTramiteId');
				$perfilado_tipoSolicitud = ($this->session->flashdata('tipoSolicitud') > 0) ? $this->session->flashdata('tipoSolicitud') : 0;
				$id_giro = ($this->session->flashdata('giro') > 0) ? $this->session->flashdata('giro') : 0;//tipo de giro de establecimiento
				$id_tipo_licencia_construccion = ($this->session->flashdata('tipo_licencia_construccion') > 0) ? $this->session->flashdata('tipo_licencia_construccion') : 0;//tipo de licencia de construcción
				$perfilado_provicional = ($this->session->flashdata('alcoholProvicional') > 0) ? $this->session->flashdata('alcoholProvicional') : 0;
				$personaFisica = $this->session->flashdata('personaFisica');
				$razonSocial = $this->session->flashdata('razonSocial');
				//validamos el perfilado sino que redireccione a tramites
				if($perfilado_form != null || $perfilado_requisitos != null){
					//Cargamos el modelo para consultar el trámite
					$this->load->model('TramitesConsulta');
					$this->load->model('M_Catalogo');
					//Obtenemos información del trámite
					$id_tramite = (int)$id_tramite;
					$tramite = $this->TramitesConsulta->descTramite($id_tramite);
					//validamos si el tramite existe
					if ($tramite != null) {
						//Guardamos el titulo del tramite para mostrarlo en la vista
						$data= array("title"=> $tramite->nombre,"id_tramite"=>$tramite->id,"id_dependencia"=>$tramite->id_dependencia);
						//Cargamos el modelo de las pestañas para obtener las relaciondas con la primer fase del tramite
						$this->load->model('Pestania');
						$id_usuario = $this->session->userdata('rol')[0]->id;
						//cargamos el modelo de fases
						$this->load->model('Fase_M');
						$fase =  $this->Fase_M->getFase(1,$id_tramite)->id;//siempre la fase sera la  NO° 1 la inicial del trámite
						//Obtenemos las pestañas que tiene la fase inicial del trámite
						$pestanias["pestanias"] = $this->Pestania->tramite_fase_pestania($id_tramite,$fase,$id_usuario);
						//obtenemos el id de las pestañas
						$id_pestanias = $this->obtenerPestania($pestanias["pestanias"]);

						//Cargamos el modelo para obtener los formularios
						$this->load->model('Formulario');
						//obtenemos los formularios de cada pestaña
						$form_pestanias["form_pestanias"] = $this->Formulario->getFormPestania($id_pestanias,$perfilado_form,$id_tramite);
						//Obtenemos el id de los formularios de las pestañas
						$id_form = $this->obtenerForm($form_pestanias["form_pestanias"]);
						//obtenemos los formularios de las pestañas
						$this->Formulario->setIdFormulario($id_form);
						$formularios["formularios"] = $this->Formulario->getForm();
						$atributos_formularios = $this->Formulario->getAributoFormulario();
						$campos_formularios = $this->Formulario->getCamposFormulario();
						//Obtenemos los requisitos del trámite
						$requisitos["requisitos"] = $this->TramitesConsulta->requisitosTramite($id_tramite,$perfilado_requisitos);
						$giro = $this->M_Catalogo->giro($id_giro);
						//obtenemos el tipo de licencia de consrucción si es que es el trámite para licencia de construcción
						$tipo_licencia_construcción = $this->M_Catalogo->tipo_construccion($id_tipo_licencia_construccion);
						//cargaos las notificaciones
						$this->load->model('Notificacion_M');
            $data["notificaciones"] = $this->Notificacion_M->progresoSolictud($this->session->userdata('id_usuario'));
						
						//Creamos array asociativo para mandar los datos a laa vista
						$data["pestanias"] = $pestanias;
						$data["form_pestanias"] = $form_pestanias;
						$data["formularios"] = $formularios;
						$data["atributos_formularios"] = $atributos_formularios;
						$data["campos_formularios"] = $campos_formularios;
						$data["requisitos"] = $requisitos;
						$data["giro"] = $giro;//giro del rámite, i es que lo tiene
						$data["tipo_construccion"] = $tipo_licencia_construcción;//tipo de licencia de construcción
						$data["subTramiteId"] = $perfilado_subTramite;//sub Tramites
						$data["tipoSolicitud"] = $perfilado_tipoSolicitud; //tipo solicitud
						$data["provicional"] = $perfilado_provicional; //tipo provicional
						$data["personaFisica"] = $personaFisica; //tipo persona
						$data["razonSocial"] = $razonSocial; //tipo persona
						//Cargamos las vistas correspondientes para mostrar el trámite
						$this->load->view('head',$data);
						$this->load->view('ciudadano/header',$data);
						$this->load->view('pestanias/pestanias',$data);
						$this->load->view('footer');
						$this->load->view('scripts/js');
						$this->load->view('scripts/utils');
						$this->load->view('scripts/tramite');
						//scripts Web Sockets
			      $this->load->view('scripts/ws-funcionario');
			      $this->load->view('scripts/dashboard');
				}//IF
				else{
					//EL tramite no existe en la DB
					$data["title"] = "Tramite no soportado";
					$data["pestanias"] = array();
					$this->load->view('head',$data);
					$this->load->view('ciudadano/header',$data);
					$this->load->view('pestanias/pestanias',$data);
					$this->load->view('footer');
					$this->load->view('scripts/js');
				}//else
			}//.if perfilado
			else{
				//el perfilado viene vacio
				redirect(base_url().'Ciudadano');
			}//else
			}//if
			else{
				//El usuario no es un cudadano
				redirect(base_url(),'refresh');
			}//else
		}//if
		else{
			//No existe usuario logueado
			redirect(base_url().'Ciudadano');
		}//else

	}//index

	//consulto los datos para llenar la vista de tramites en linea
	//obtengo la informacion de un tramite en especifico
	public function filtroTramite($id)
	{
		//cargo el modelo para obtener la información
		$this->load->model('tramitesConsulta');

		//guardo la informacion consultada en arreglos
		$dataTramites = $this->tramitesConsulta->descTramite($id);
		$dataRequisitos = $this->tramitesConsulta->requisitosTramite($id,"");

		//creo un array para almacenar los dos arreglos anteriores y poder enviar la informacion a la vista
		$data = [
			"tramite" => $dataTramites,
			"requisitos" => $dataRequisitos,
			"title" => "Descripción de Trámite",
		];

		//cargo las vistas necesarias y envio la informacion mediante un arreglo data
    	$this->load->view('head',$data);
		$this->load->view('descTramites/head',$data);
		$this->load->view('descTramite',$data);
		$this->load->view('footer');
		$this->load->view('scripts/js');
	}//filtro tramite

	/**
	 * @param array $pestanias_form
	 */
	public function obtenerPestania($arr_pestania)
	{
		$pestanias = array();
		foreach ($arr_pestania as $pestania) {
			array_push($pestanias, $pestania->id_pestania);
		}//foreach
	    return $pestanias;
	}//obtener_pestania
	/**
	 * @param array $arr_pestania
	 */
	public function obtenerForm($arr_form)
	{
		$id_form = array();
		foreach ($arr_form as $form) {
			array_push($id_form, $form->id_formulario);
		}//foreach
	    return $id_form;
	}//obtenerForm

	}//class
