<?php
defined('BASEPATH') or exit('No direct script access allowed');

class ModificarFuncionario extends CI_Controller
{

    public function index()
    {
        if (!empty($this->session->userdata()) && $this->session->userdata('logged_in') /*&& $this->session->userdata('rol')[0]->nombre == "Ciudadano"*/) {

            $data["title"] = "Modificación de funcionario";
            //Carga de las vistas  principales
            //$this->load->view('scripts/js');
            //$this->load->view('scripts/catalogo',$data);
            $this->load->view('head', $data);
            $this->load->view('funcionario/header', $data);
            $this->load->view('funcionario/contentModificar', $data);
            $this->load->view('footer');
            $this->load->view('scripts/js');
        } //if
        else {
            $this->session->sess_destroy();
            redirect(base_url() . "Auth", 'refresh');
        } //else
    } //index


//funion que modifica imagen y telefono
    public function cambiarDatosFuncionario()
    {

        $txtImagenPerfil = ($this->input->post("txtImagenPerfil") != "") ? addslashes($this->input->post("txtImagenPerfil")) : "";
        $txtTelefono = ($this->input->post("txtTelefono") != "") ? addslashes($this->input->post("txtTelefono")) : "";
        //se crea array con los campos y sus respectivas validaciones
        $arr_validations = [
                array(
                    "field" => "txtTelefono",
                    "label" => "Teléfono",
                    "rules" => "max_length[13]|is_natural|trim",
                ),
            ];
            //se crea array con mensajes de las validaciones
            $arr_msg = [
                array(
                    "max_length[13]" => "El campo %s debe tener menos de 13 caracteres",
                    "is_natural"     => "El campo %s solo puede contener numeros",
                ),
            ];
            //se carga la libreria validation
            $this->load->library('validation');
            //se gurada en una variable el resultado de dicha validacion (true,false)
            $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
            //se mete en un if el resultado de la validacion
            if ($form_validation) {


        $this->load->library('SubirImagen');
        //Url para guardar la ruta de los archivos

        $urlImgPerfil = "plantilla/images/user-perfil/";
        $this->subirimagen->dir_exist($urlImgPerfil);
        //Verificamos que los directorios donde se guardaran las imagenes existan, en caso contrario se crean antes de subir los archivos

        $this->subirimagen->dir_exist($urlImgPerfil);

        $id_user_ses = $this->session->userdata('id_usuario');
     
        $nom1_ses1    = $this->session->userdata('primer_nombre');
        $nom1_ses    =    str_replace(' ','_',$nom1_ses1);
        $txtNomIma   = $id_user_ses . "img" . $nom1_ses;

        $ruta1 = $urlImgPerfil . $txtNomIma . ".jpg";
        $ruta2 = $urlImgPerfil . $txtNomIma . ".png";
        //comprobamos si el archvo existe en casi de que exista se elimina
        if (file_exists($ruta1)) {
            unlink($ruta1);
        }
        //se comprueba que el archivo exista en caso de que exita se elimnia
        if (file_exists($ruta2)) {
              unlink($ruta2);
        }


        if (!empty($_FILES["txtImagenPerfil"]["name"])) {
            $config["upload_path"]   = $urlImgPerfil;
            $config['allowed_types'] = 'jpg|png';
            $config['max_size']      = 525542880;
            $config['file_name']     = $txtNomIma;
            //Subiendo imagen al server
            $respondImg = $this->subirimagen->upload_files($config, "txtImagenPerfil");
            $nameImg    = $respondImg["uploadSuccess"]["file_name"];

        }

        $extImg = $this->upload->data('file_ext');
        $ruta   = $urlImgPerfil . $txtNomIma . $extImg;
        if ($respondImg["valid"]) {
            //Cargamos el modelo para insertar los archivos
            $this->load->model('Usuario');

            if ($this->Usuario->ModImg($ruta, $id_user_ses)) {

                $this->session->set_userdata('img', base_url() . $ruta);


            }
            if ($this->Usuario->cambiartelefono($txtTelefono, $id_user_ses)) {
                $this->session->set_userdata('telefono',$txtTelefono);
                $this->session->set_flashdata('good', " ");
                redirect(base_url() . "ModificarFuncionario", '');
            }
        } else {
            $this->session->set_flashdata('error', "hubo un error al actualizar tus datos");
            redirect(base_url() . "ModificarFuncionario", '');
        } //else

    }//if validacion formvalidations
    else {
                    //se envian los errores  del form validation atravez del flashdata
                    $this->session->set_flashdata('errorr', validation_errors());
                    //redireccionas a la vista correspondiente
                    redirect(base_url() . "ModificarFuncionario", '');
    }
    } //modIma

} //class
