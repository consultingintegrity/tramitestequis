<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Notificacion extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('rol')[0]->nombre != "Ciudadano") {
      redirect(base_url()."Auth","");
    }
  }

  function index($id_solicitud = 0)
  {
    $idUsuario = $this->session->userdata('id_usuario');
    //cargo modelo de Notificacion
    $this->load->model('Notificacion_M');
    $historialNotificaciones = $this->Notificacion_M->getNotificacion($idUsuario,$id_solicitud);

    $title = 'Notificaciones';
    $desc = 'Seguimiento de la solicitud #' . $id_solicitud;
    $data = [
        "historialNotificaciones" => $historialNotificaciones,
        'title' => $title,
        'descripcion' => $desc,
    ];

    $data["notificaciones"] = $this->Notificacion_M->progresoSolictud($this->session->userdata('id_usuario'));

    $this->load->view('head', $data);
    $this->load->view('ciudadano/header');
    $this->load->view('ciudadano/notificaciones',$data);
    $this->load->view('footer');
    $this->load->view('scripts/js');
  }//index

}//Class
