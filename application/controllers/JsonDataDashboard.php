<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class JsonDataDashboard extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_Catalogo',"Solicitud_M"));
  }//construct

  function dashboard()
  {
    $solicitud_revision = $this->Solicitud_M->getAllSolicitud(4,date("m"));//obenemos las solicitudes en revision
    $solicitud_terminada = $this->Solicitud_M->getAllSolicitud(1,date("m"));//obenemos las solicitudes en terminadas
    $solicitud_cancelada = $this->Solicitud_M->getAllSolicitud(0,date("m"));//obenemos las solicitudes canceladas
    $dinero_generado = $this->Solicitud_M->getDineroGenerado(date("m"));//obenemos el dinero generado
    $solicitudes = $this->Solicitud_M->solicitudes(date("m"));//obenemos las solicitudes
    $solicitudes_progreso = $this->Solicitud_M->progresoSolictud(date("m"));//obenemos el progreo de las  solicitudes
    $documentos_generados = $this->Solicitud_M->getCountdocumentosGenerados(date("m"));//obenemos los documentos generados de las solicitudes

    $data = array(
      'solicitudes_proceso' => count($solicitud_revision), 'solicitudes_terminadas' => count($solicitud_terminada), 'solicitudes_canceladas' => count($solicitud_cancelada), 'dinero_generado' => $dinero_generado,
    'solicitudes'=> $solicitudes, "progreso_solicitudes" => $solicitudes_progreso, "documentos_generados" => $documentos_generados);

    echo json_encode($data);

  }

}
