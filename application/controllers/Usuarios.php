<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Usuarios extends CI_Controller
{

    public function index($control = "")
    {
        if ($this->session->userdata('logged_in') && $this->session->userdata('rol')[0]->nombre == "Ciudadano") {
            $this->load->model('Usuario');
            $this->load->model('Formulario');

            $this->Formulario->setIdFormulario(5);

            if ($control == "contacto") {
                $this->Formulario->setIdFormulario(6);

            } else if ($control == "imagen") {
                $this->Formulario->setIdFormulario(7);
            } elseif ($control == "contra") {
                $this->Formulario->setIdFormulario(3);
            }

            $formulario = $this->Formulario->getForm(); //obtenemos los datos del formulario
            //Obtenemos los aributos del formulario
            $atributos_formulario = $this->Formulario->getAributoFormulario();
            //obtenemos los campos del formulrio
            $campos_formulario = $this->Formulario->getCamposFormulario();
            //mandamos los datos a la vista

            $data = [
                "formulario"           => $formulario,
                "atributos_formulario" => $atributos_formulario,
                "campos_formulario"    => $campos_formulario,

            ];

            $data["title"] = "Modificción de usuario";
            //Carga de las vistas  principales
            //$this->load->view('scripts/js');
            //$this->load->view('scripts/catalogo',$data);
            $this->load->view('head', $data);
            $this->load->view('navegacion/header', $data);
            $this->load->view('navegacion/content', $data);
            $this->load->view('footer');
            $this->load->view('scripts/js');

            //Cargamos el modelo de Usuario para mostrarlos en la vista
            //Obtenemos los atos del formulario de login
            /*
        //Cargamos las vistas correspondientes a este controlador
        $data["title"]="Modificar Datos";
        $this->load->view('head',$data);
        $this->load->view('navegacion/header');
        $this->load->view('navegacion/content',$data);
        $this->load->view('footer');    */
        } //if
        else {
            $this->session->sess_destroy();
            redirect(base_url() . "Auth", 'refresh');
        } //else
    } //index
    //funcion que modifica datos personales
    public function cambiarDatosPersona()
    {

        $txtPrimerNom  = (!empty($this->input->post("txtPrimerNom"))) ? addslashes($this->input->post("txtPrimerNom")) : "";
        $txtSegundoNom = (!empty($this->input->post("txtSegundoNom"))) ? addslashes($this->input->post("txtSegundoNom")) : "";
        $txtPrimerAp   = (!empty($this->input->post("txtPrimerAp"))) ? addslashes($this->input->post("txtPrimerAp")) : "";
        $txtSegundoAp  = (!empty($this->input->post("txtSegundoAp"))) ? addslashes($this->input->post("txtSegundoAp")) : "";
        $txtfecha      = (!empty($this->input->post("txtFecha"))) ? addslashes($this->input->post("txtFecha")) : "";
        $optGenero     = (!empty($this->input->post("optGenero"))) ? addslashes($this->input->post("optGenero")) : "";
        $optEstado     = (!empty($this->input->post("optEstado"))) ? addslashes($this->input->post("optEstado")) : "";
        $optMunicipio  = (!empty($this->input->post("optMunicipio"))) ? addslashes($this->input->post("optMunicipio")) : "";

        if ($this->input->post("btnAceptar")) {
            $arr_validations = [

                array(
                    "field" => "txtPrimerNom",
                    "label" => "Primer Nombre",
                    "rules" => "required|max_length[65]|trim",
                ),
                array(
                    "field" => "txtSegundoNom",
                    "label" => "Segundo Nombre",
                    "rules" => "max_length[65]|trim",
                ),
                array(
                    "field" => "txtPrimerAp",
                    "label" => "Primer Apellido",
                    "rules" => "required|max_length[65]|trim",
                ),
                array(
                    "field" => "txtSegundoAp",
                    "label" => "Segundo Apellido",
                    "rules" => "max_length[10]|trim",
                ),
                array(
                    "field" => "optGenero",
                    "label" => "Genero",
                    "rules" => "required|trim|in_list[m,M,f,F]",
                ),
                array(
                    "field" => "optMunicipio",
                    "label" => "Municipio",
                    "rules" => "required|trim|is_natural",
                ),
                array(

                    "field" => "txtFecha",
                    "label" => "Fecha de nacimiento",
                    "rules" => "required|max_length[65]|trim|min_length[6]",
                ),
            ];
            $arr_msg = [
                array(
                    "required"      => "EL campo %s es requerido para continuar",
                    "valid_email"   => "EL campo %s debe ser un correo electrónco valido",
                    "min_length[2]" => "El campo %s debe tener mas de 2 caracteres",
                    "min_length[6]" => "El campo %s debe tener mas de 6 caracteres", "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                    "alpha"         => "El campo %s solo debe contener letras",
                    "is_unique"     => "Error ese %s ya se encuentra registrado",
                    "valid_date"    => "El campo %s debe estar en formato correcto",

                ),
            ];

            //cargamos form_validation
            $this->load->library('validation');
            $form_validation = $this->validation->validForm($arr_validations, $arr_msg);

            if ($this->validation->acento($txtPrimerNom)) {
                $this->session->set_flashdata('error', "El campo primer nombre solo debe contener letras");
                redirect(base_url() . "Usuarios", 'refresh');
            } //if validacion de nombre1
            elseif ($this->validation->acento($txtSegundoNom) && $txtSegundoNom != null) {
                $this->session->set_flashdata('error', "El campo segundo nombre solo debe contener letras");
                redirect(base_url() . "Usuarios", 'refresh');
            } //elseif validacion de nombre 2

            elseif ($this->validation->acento($txtPrimerAp)) {
                $this->session->set_flashdata('error', "El campo primer apellido solo debe contener letras");
                redirect(base_url() . "Usuarios", 'refresh');
            } //elseif validacion ape1
            elseif ($this->validation->acento($txtSegundoAp) && $txtSegundoAp != null) {
                $this->session->set_flashdata('error', "El campo segundo apellido solo debe contener letras");
                redirect(base_url() . "Usuarios", 'refresh');
            } //elseif validacion ape2
            elseif ($this->validation->fecha($txtfecha) == false) {
                $this->session->set_flashdata('error', "El campo fecha debe estar en formato correcto");
                redirect(base_url() . "Usuarios", 'refresh');
            } //elseif validacion fecha
            else {
              if ($form_validation) {
                $id_per_ses = $this->session->userdata('id_persona');
                $this->load->model('Usuario');
                //Llamamos la funcion de modPer
                if ($this->Usuario->modPer($txtPrimerNom, $txtSegundoNom, $txtPrimerAp, $txtSegundoAp, $txtfecha, $optGenero, $optMunicipio, $id_per_ses)) {
                  //Modificamos los datos de la sessión que se modificarón en el perfil del usuario
                  $this->session->set_userdata('primer_nombre',$txtPrimerNom);
                  $this->session->set_userdata('segundo_nombre',$txtSegundoNom);
                  $this->session->set_userdata('primer_apellido',$txtPrimerAp);
                  $this->session->set_userdata('segundo_apellido',$txtSegundoAp);
                  $this->session->set_userdata('genero',$optGenero);
                  $this->session->set_userdata('fecha_nacimiento',$txtfecha);
                  $this->session->set_userdata('id_municipio',$optMunicipio);
                  $this->session->set_flashdata('good', " ");
                  redirect(base_url() . "Usuarios", 'refresh');
                } //if
              } //if
              else {
                $this->session->set_flashdata('error', validation_errors());
                redirect(base_url() . "Usuarios", 'refresh');
              } //else validaciones de form_validation
            } //else resto del codigo validaciones nombre etc

            //--------------------------------------------------------------------

        } //if boton aceptar

    } //fin de la funcion

//modificar datos de contacto
    public function cambiarDatosUsuario()
    {

        $this->load->library('email');

        $txtCorreo     = (!empty($this->input->post("txtEmail"))) ? addslashes($this->input->post("txtEmail")) : "";
        $txtTelefono   = (!empty($this->input->post("txtTelefono"))) ? addslashes($this->input->post("txtTelefono")) : "";
        $correo_sesion = $this->session->userdata('correo_electronico');
        $id_usu_ses    = $this->session->userdata('id_usuario');
        if ($this->input->post("btnAceptar")) {

            if ($txtCorreo != $correo_sesion) {
                $this->form_validation->set_rules('txtEmail', 'correo electrónico', 'required|valid_email|max_length[65]|trim|is_unique[usuario.correo_electronico]',
                    array(
                        "required"       => "EL campo %s es requerido para continuar",
                        "valid_email"    => "EL campo %s debe ser un correo electrónco valido",
                        "min_length[7]"  => "El campo %s debe tener mas de 7 caracteres",
                        "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                        "is_natural"     => "El campo %s solo debe tener numeros",
                        "is_unique"      => "Error ese %s ya se encuentra registrado",

                    )
                ); //set rules

            } //if  validacion es el mismo correo

            $this->form_validation->set_rules('txtTelefono', 'telefono', 'min_length[7]|is_natural|max_length[13]',
                array(
                    "required"       => "EL campo %s es requerido para continuar",
                    "valid_email"    => "EL campo %s debe ser un correo electrónco valido",
                    "min_length[7]"  => "El campo %s debe tener mas de 7 caracteres",
                    "max_length[65]" => "El campo %s debe tener menos de 65 caracteres",
                    "is_natural"     => "El campo %s solo debe tener numeros",
                    "is_unique"      => "Error ese %s ya se encuentra registrado",

                )
            );
            $this->load->library('form_validation');

            if ($this->form_validation->run()) {

                $this->load->model('Usuario');
                //Llamamos la funcion de modificar usuario
                if ($this->Usuario->modUsu($txtCorreo, $txtTelefono, $id_usu_ses)) {
                    $this->session->set_userdata('correo_electronico', $txtCorreo);
                    $this->session->set_userdata('telefono', $txtTelefono);
                    $this->session->set_flashdata('good', " ");
                    redirect(base_url() . "Usuarios/index/contacto", '');

                } //if

            } //if

            else {
                $this->session->set_flashdata('error', validation_errors());
                redirect(base_url() . "Usuarios/index/contacto", '');
            } //else validaciones de form_validation

        } //if boton aceptar
    } //mod_contact

//funion que modifica contraseña
    public function cambiarContrasenia()
    {
        //redirect(base_url()."Usuarios/index/contra",'');

        $txtPwd         = (!empty($this->input->post("txtPwd"))) ? addslashes($this->input->post("txtPwd")) : "";
        $txtConfirmPass = (!empty($this->input->post("txtConfirmPass"))) ? addslashes($this->input->post("txtConfirmPass")) : "";

        if ($this->input->post("btnCambiarPwd")) {
            //validamos campos del formulario
            $arr_validations = [

                array(
                    "field" => "txtPwd",
                    "label" => "Contraseña",
                    "rules" => "required|min_length[6]|matches[txtConfirmPass]",
                ),
                array(
                    "field" => "txtConfirmPass",
                    "label" => "Confirmar Contraseña",
                    "rules" => "required|min_length[6]",
                ),

            ];
            $arr_msg = [
                array(
                    "required"      => "EL campo %s es requerido para continuar",
                    "min_length[6]" => "El campo %s debe tener mas de 6 caracteres",

                ),
            ];

            //cargamos form_validation
            $this->load->library('validation');
            $form_validation = $this->validation->validForm($arr_validations, $arr_msg);
            if ($form_validation) {
                $id_usu_ses = $this->session->userdata('id_usuario');
                $this->load->model('Usuario');
                //Llamamos la funcion de registro
                if ($this->Usuario->cambiarcontra(hash('sha512', $txtPwd), $id_usu_ses)) {

                    $this->session->set_flashdata('good', " ");
                    redirect(base_url() . "usuarios/index/contra", '');

                } //if metodo cambiar

            } //if    form validacion

            else {
                $this->session->set_flashdata('error', validation_errors());
                redirect(base_url() . "usuarios/index/contra", 'refresh');
            } //else validaciones de form_validation

        } //if del boton aceptar
    } //modContra

    public function actualizaStatus($id_usuario)
    {
        $this->load->model('Usuario');
        $status1 = $this->Usuario->getStatus($id_usuario);
        $status  = $status1->status;
        if ($status == 2) {
            //Llamamos la funcion de modificar usuario
            if ($this->Usuario->actualizaStatus($id_usuario)) {

                $this->session->set_flashdata('validado', " ");
                redirect(base_url() . "Auth", '');

            } //if

        } //if boton aceptar
        elseif ($status == 1) {
            $this->session->set_flashdata('validado', " ");
            redirect(base_url() . "Auth", '');

        } elseif ($status == 0) {
            $this->session->set_flashdata('validadoE', " ");
            redirect(base_url() . "Auth", '');
        }

    } //actualizar status

//funion que modifica imagen
    public function cambiarImagenPerfil()
    {
        $txtImagenPerfil = ($this->input->post("txtImagenPerfil") != "") ? addslashes($this->input->post("txtImagenPerfil")) : "";

        $this->load->library('SubirImagen');
        //Url para guardar la ruta de los archivos

        $urlImgPerfil = "plantilla/images/user-perfil/";
        $this->subirimagen->dir_exist($urlImgPerfil);
        //Verificamos que los directorios donde se guardaran las imagenes existan, en caso contrario se crean antes de subir los archivos

        $this->subirimagen->dir_exist($urlImgPerfil);

        $id_user_ses = $this->session->userdata('id_usuario');
        $nom1_ses1    = $this->session->userdata('primer_nombre');
        $nom1_ses    =    str_replace(' ','_',$nom1_ses1);
        $txtNomIma   = $id_user_ses . "img" . $nom1_ses;

        $ruta1 = $urlImgPerfil . $txtNomIma . ".jpg";
        $ruta2 = $urlImgPerfil . $txtNomIma . ".png";
        //comprobamos si el archvo existe en casi de que exista se elimina
        if (file_exists($ruta1)) {
            unlink($ruta1);
        }
        //se comprueba que el archivo exista en caso de que exita se elimnia
        if (file_exists($ruta2)) {
              unlink($ruta2);
        }


        if (!empty($_FILES["txtImagenPerfil"]["name"])) {
            $config["upload_path"]   = $urlImgPerfil;
            $config['allowed_types'] = 'jpg|png';
            $config['max_size']      = 5242880;
            $config['file_name']     = $txtNomIma;
            //Subiendo imagen al server
            $respondImg = $this->subirimagen->upload_files($config, "txtImagenPerfil");
            $nameImg    = $respondImg["uploadSuccess"]["file_name"];
        }

        $extImg = $this->upload->data('file_ext');
        $ruta   = $urlImgPerfil . $txtNomIma . $extImg;

        if ($respondImg["valid"]) {
            //Cargamos el modelo para insertar los archivos
            $this->load->model('Usuario');

            if ($this->Usuario->ModImg($ruta, $id_user_ses)) {

                $this->session->set_userdata('img', base_url() . $ruta);

                $this->session->set_flashdata('good', " ");
                redirect(base_url() . "usuarios/index/imagen", '');
            }

        } else {
            $this->session->set_flashdata('error', "hubo un error al actualizar tu imagen de perfil");
            redirect(base_url() . "usuarios/index/imagen", '');
        } //else*/
    } //modIma

} //class
