<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Encuesta extends CI_Controller {
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Encuesta_model','modelo_encuesta');

    }
    public function index()
    {
        
        $this->load->view("encuesta");
        $this->data['js']= ['encuesta'];
    }
    public function encuesta()
    {
       $msg['msg']='fail';
    //    $id_respuesta=$this->input->post('encuestaid');
       $categoria=$this->input->POST();
    //    $fecha_encuesta = date('Y-m-d H:i:s');
       if (!empty ($categoria)) {
        //    $categoria= array_merge($categoria,)
        $datos_encuesta= array(
            'fecha_registro' => date('Y-m-d H:i:s'),
            'respuesta' =>json_encode($categoria)
        );
        // print_r($datos_encuesta) ;
        // die;
        $datos_encuesta = $this->modelo_encuesta->insert_respuesta($datos_encuesta);
        if ($datos_encuesta) {
            $msg['msg']='success';
        }
       }
       echo json_encode($msg);
    }
}