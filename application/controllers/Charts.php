<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * @author Pepe-Droid
 * Mandamos traer los datos necesarios para llenar las gráficas en formato Json
 */
class Charts extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model(array('M_Catalogo',"Solicitud_M"));
    $this->load->model("Funcionario");
    $this->load->library('Json');//Obtener información de json_solicitud
    //librerias
    $this->load->library('Validation');
    if (!$this->session->userdata('logged_in') && !$this->session->userdata('rol')[0]->nombre == "Administrador"  ) {
      $this->session->sess_destroy();
      redirect(base_url()."Auth","refresh");
    }//if
  }//construct

  function tramitesSolicitados(){
    $char_bar["data"] = $this->Solicitud_M->tramitesSolicitados();//gráfica de barras
    echo json_encode($char_bar);
  }//charBar

  function ingresos(){
    $char["data"] = $this->Solicitud_M->dineroPorTramites();//gráfica de radar
    echo json_encode($char);
  }//charBar

  public function informacionFormularioSolicitud(){
    $id_tramite = $this->input->post("id_tramite");
    $id_form =  $this->input->post("id_form");
    $solicitud_construccion = $this->Solicitud_M->infoSolicitudByTramite($id_tramite);//obenemos las solicitudes
    echo json_encode($this->json->json_decode_info_form($solicitud_construccion,$id_form));
  }//estadisticaAdicionalLicenciaConstruccion

}//Class
