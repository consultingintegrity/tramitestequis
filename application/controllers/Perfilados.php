<?php
/**
 * @author esau Krack
 * @version 1.0
 */
defined('BASEPATH') OR exit('No direct script access allowed');
class Perfilados extends CI_Controller {
    
    //recibo la informacion del perfilado y la evaluo
    public function index($tramite){
        //cargamos modelo de Solicitud_M
        $this->load->model('Solicitud_M');

        //evaluo el perfilado mediante el tramite
        switch ($tramite) {
            // Tramite = Lineamiento y Número Oficial
            case '1':
                $subTramite = "";
                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $tipoPersona = $this->input->post("options");
                $subTramite = $this->input->post("tipo_SubTramite1");
                $tipoSolicitud = $this->input->post("tipoSolicitud");
                $formularios = array();
                $requisitos = array();
                $personaFisica =($tipoPersona == "fisica")? 1 : 0; 
                //formularios
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
                $confianza = ($personaConfianza == "si") ? 0 : 12;

                //requisitos
                $poderNotarial = ($tipoPersona == "moral") ? 0 : 5;
                $actaMoral = ($tipoPersona == "moral") ? 0 : 7;
                $cartaNotariada = ($personaConfianza == "si") ? 0 : 12;

                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);
                array_push($requisitos,$actaMoral);
                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$cartaNotariada);

                //envio la informacion al siguiente controlador
                 $this->session->set_flashdata('requisitos',$requisitos);
                 $this->session->set_flashdata('formularios',$formularios);
                 $this->session->set_flashdata('subTramiteId',$subTramite);
                 $this->session->set_flashdata('tipoSolicitud',$tipoSolicitud);
                 $this->session->set_flashdata('personaFisica',$personaFisica);
                 $this->session->set_flashdata('razonSocial',$razonSocial);
                 

                redirect(base_url()."DesgloceTramite/index/1","");
                break;
            case '2':
                //tramite 2
                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $usoSolicitado = $this->input->post("optUsoEstablecimiento");
                $tipoPersona = $this->input->post("options");
                $subTramite = $this->input->post("tipo_SubTramite2");
                $acta_constitutiva = ($tipoPersona == "moral") ? 0 : 7;
                $personaFisica =($tipoPersona == "fisica")? 1 : 0;
                $formularios = array();
                $requisitos = array();
                //excluimos los formularios y/o requisitos
                $confianza = ($personaConfianza != 'si') ? 12 : 0;
                $poderNotarial = ($tipoPersona == "moral") ? 0 : 5;
                $cartaNotariada = ($personaConfianza == "si") ? 0 : 12;
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
                //Recorremos el uso Solicitado
                $habitacional = 22;
                $comercial = 42;
                $servicios =  43;
                foreach ($usoSolicitado as $tipoUso) {
                  switch ($tipoUso) {
                    case 'Habitacional':
                      $habitacional = 0;
                    break;
                    case 'Servicios':
                      $servicios = 0;
                    break;
                    case 'Comercial':
                      $comercial = 0;
                    break;
                    default:
                      // code...
                      break;
                  }//switch
                }//foreach
                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);
                array_push($formularios,$habitacional);
                array_push($formularios,$servicios);
                array_push($formularios,$comercial);
                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$acta_constitutiva);
                array_push($requisitos,$cartaNotariada);
                //envio la informacion al siguiente controlador
                $this->session->set_flashdata('formularios',$formularios);
                $this->session->set_flashdata('requisitos',$requisitos);
                $this->session->set_flashdata('subTramiteId',$subTramite);
                $this->session->set_flashdata('personaFisica',$personaFisica);
                $this->session->set_flashdata('razonSocial',$razonSocial);
                redirect(base_url()."DesgloceTramite/index/2","");
                break;//dictamen uso de suelo
            case '3':
                //tramite Factibilidad de giro
                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $tipoGiro = $this->input->post("tipoGiro");
                $tipoPersona = $this->input->post("options");
                $subTramite = $this->input->post("tipo_SubTramite3");
                $contrato_arrendamiento = ($this->input->post("propietario") != "si") ? 0  : 18;
                $poderNotarial = ($tipoPersona == "moral") ? 0 : 7;
                $acta_constitutiva = ($tipoPersona == "moral") ? 0 : 5;
                $cartaNotariada = ($personaConfianza == "si") ? 0 : 12;
                $dictamen = ($tipoSubTramite = $this->input->post("tipo_SubTramite3") == 13)? 17: 77;
                $personaFisica =($tipoPersona == "fisica")? 1 : 0;

                $nombreNegocio = ($tipoPersona == "fisica") ? 0 : 56;
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
                $formularios = array();
                $requisitos = array();

                if($personaConfianza != 'si'){

                    $confianza = 12;
                }else{

                    $confianza = "0";
                }

                array_push($formularios,$nombreNegocio);
                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);
                array_push($requisitos,$contrato_arrendamiento);
                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$acta_constitutiva);
                array_push($requisitos,$cartaNotariada);
                array_push($requisitos,$dictamen);
                //envio la informacion al siguiente controlador
                $this->session->set_flashdata('formularios',$formularios);
                $this->session->set_flashdata('requisitos',$requisitos);
                $this->session->set_flashdata('giro',$tipoGiro);
                $this->session->set_flashdata('subTramiteId',$subTramite);
                $this->session->set_flashdata('personaFisica',$personaFisica);
                $this->session->set_flashdata('razonSocial',$razonSocial);
                redirect(base_url()."DesgloceTramite/index/3","");
                break;//factibilidad
            case '4':
                //tramite licencia de funcionamiento
                                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $tipoPersona = $this->input->post("options");
                $tipoGiro = $this->input->post("tipoGiroLicFuncionamiento");
                $vta_Alcohol = $this->input->post("tipoGiroLicFuncionamiento");
                $subTramite = $this->input->post("tipo_SubTramite4");
                $licenciaF = $this->input->post("licFuncionamiento");
                $poderNotarial = ($tipoPersona == "moral") ? 0 : 5;
                $acta_constitutiva = ($tipoPersona == "moral") ? 0 : 28;
                $cartaNotariada = ($personaConfianza == "si") ? 0 : 12;
                $personaFisica =($tipoPersona == "fisica")? 1 : 0;
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
                //controlar leyenda de doc
                $provicional = 0;
                $documentoCrediticio=0;
                $constanciaTurismo = ($this->input->post("turismo") == "si") ? 0 : 72;
                $constanciaAmbiental = ($this->input->post("ambiental") == "si") ? 0 : 73;
                $licenciaFuncionamiento =($licenciaF == "no")? 42 :0 ;          
                //mande el id obtenido del perfilado para obtener el requisito de Cabildo
                //Se toma el ID si es venta de alcohol y se compara directamente con el modelo, si es 1 lo muestra si es 0 no
                $vta = $this->Solicitud_M->VentaDeAlcohol($vta_Alcohol);
                $vtaAlimentos = $this->Solicitud_M->VentaDeComida($vta_Alcohol);
                $Alimentos = (!empty($vtaAlimentos)) ? 0 : 26;
                $vistoBuenoCabildo = (!empty($vta)) ? 0 : 71;
    
                //controlar requisitos
                if($subTramite == 34){
                    if($vistoBuenoCabildo == 0){
                        $constanciaAlcoholesEstatal = ($this->input->post("alcoholesEstatal") == "si") ? 0 : 75;
                        $documentoCrediticio = ($constanciaAlcoholesEstatal == 75) ? 76 : 76;
                        $vistoBuenoCabildo = ($this->input->post("alcoholesEstatal") == "si")?71:0;
                        $provicional = ($this->input->post("alcoholesEstatal") == "si")?0:1;
                    }elseif($vistoBuenoCabildo == 71){
                        $constanciaAlcoholesEstatal=75;
                        $documentoCrediticio=76;    
                    }
                }else if($subTramite == 35 || $subTramite == 36){
                    if($vistoBuenoCabildo == 0){
                        $constanciaAlcoholesEstatal = ($this->input->post("alcoholesEstatal") == "si") ? 0 : 75;
                        $documentoCrediticio = ($constanciaAlcoholesEstatal == 75) ? 0 : 76;
                        $vistoBuenoCabildo = ($this->input->post("alcoholesEstatal") == "si")?71:0;
                        $provicional = ($this->input->post("alcoholesEstatal") == "si")?0:1;
                    }elseif($vistoBuenoCabildo == 71){
                        $constanciaAlcoholesEstatal=75;
                        $documentoCrediticio=76;    
                    }
                }

                if(!empty($tipoGiro)){
                $formularios = array(20);
                $requisitos = array();

                if($personaConfianza != 'si'){

                    $confianza = 12;
                }else{

                    $confianza = "0";
                }

                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);
                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$acta_constitutiva);
                array_push($requisitos,$cartaNotariada);
                array_push($requisitos,$constanciaTurismo);
                array_push($requisitos,$constanciaAmbiental);
                array_push($requisitos,$constanciaAlcoholesEstatal);
                array_push($requisitos,$documentoCrediticio);
                array_push($requisitos,$licenciaFuncionamiento);
                array_push($requisitos,$vistoBuenoCabildo);
                array_push($requisitos,$Alimentos);
               
                //envio la informacion al siguiente controlador
                 $this->session->set_flashdata('requisitos',$requisitos);
                 $this->session->set_flashdata('formularios',$formularios);
                 $this->session->set_flashdata('giro',$tipoGiro);
                 $this->session->set_flashdata('subTramiteId',$subTramite);
                 $this->session->set_flashdata('alcoholProvicional',$provicional);
                 $this->session->set_flashdata('personaFisica',$personaFisica);
                 $this->session->set_flashdata('razonSocial',$razonSocial);

                redirect(base_url()."DesgloceTramite/index/4","");
            }else{
                redirect(base_url()."ciudadano");            
            }
                break;
            case '5':
                //tramite licencia de alcohol
                                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $tipoPersona = $this->input->post("options");
                $licencia = $this->input->post("licenciaF");
                $subTramite = $this->input->post("tipo_SubTramite5");
                $contrato_arrendamiento = ($this->input->post("propietario") != "si") ? 0  : 18;
                $DocPropiedad = ($contrato_arrendamiento == 0) ? 33 : 0;
                $formularios = array();
                $requisitos = array();
                $cartaNotariada = ($personaConfianza == "si") ? 0 : 12;
                $reqLicencia = ($licencia == "si") ? 0 : 42;
                $personaFisica =($tipoPersona == "fisica")? 1 : 0;
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
            //controlamos si es permiso para venta o provicional
            if($subTramite == 40){

                if($personaConfianza != 'si'){

                    $confianza = 12;
                }else{

                    $confianza = "0";
                }

                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);

                 if($tipoPersona != 'moral'){

                    $actaMoral = 7;
                    $poderNotarial = 5;
                }else{

                    $actaMoral = "0";
                    $poderNotarial = "0";
                }

                array_push($requisitos,$reqLicencia);
                array_push($requisitos,$actaMoral);
                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$cartaNotariada);
                array_push($requisitos,$contrato_arrendamiento);
                array_push($requisitos,$DocPropiedad);
                //envio la informacion al siguiente controlador
                 $this->session->set_flashdata('requisitos',$requisitos);
                 $this->session->set_flashdata('formularios',$formularios);
                 $this->session->set_flashdata('requisitos',$requisitos);
                 $this->session->set_flashdata('subTramiteId',$subTramite);
                 $this->session->set_flashdata('personaFisica',$personaFisica);
                 $this->session->set_flashdata('razonSocial',$razonSocial);

                     redirect(base_url()."DesgloceTramite/index/5","");
                 }elseif($subTramite == 39){
                    if($personaConfianza != 'si'){

                        $confianza = 12;
                    }else{
    
                        $confianza = "0";
                    }
    
                    array_push($formularios,$confianza);
                    array_push($formularios,$razonSocial);
    
                     if($tipoPersona != 'moral'){
    
                        $actaMoral = 7;
                        $poderNotarial = 5;
                    }else{
    
                        $actaMoral = "0";
                        $poderNotarial = "0";
                    }
    
                    array_push($requisitos,$reqLicencia);
                    array_push($requisitos,$actaMoral);
                    array_push($requisitos,$poderNotarial);
                    array_push($requisitos,$cartaNotariada);
                    array_push($requisitos,$contrato_arrendamiento);
                    array_push($requisitos,$DocPropiedad);
                    //envio la informacion al siguiente controlador
                     $this->session->set_flashdata('requisitos',$requisitos);
                     $this->session->set_flashdata('formularios',$formularios);
                     $this->session->set_flashdata('requisitos',$requisitos);
                     $this->session->set_flashdata('subTramiteId',$subTramite);
                     $this->session->set_flashdata('personaFisica',$personaFisica);
                     $this->session->set_flashdata('razonSocial',$razonSocial);
                     
                    redirect(base_url()."DesgloceTramite/index/9","");
                 }

                break;
            case '6':
                //tramite Visto BUeno de Proteccion civil
                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $tipoPersona = $this->input->post("options");
                $subTramite = $this->input->post("tipo_SubTramite6");
                $formularios = array();
                $requisitos = array();
                $cartaNotariada = ($personaConfianza == "si") ? 0 : 12;
                $contrato_arrendamiento = ($this->input->post("propietario") != "si") ? 0  : 18;
                $licencia_anterior = ($this->input->post("licenciaAnterior") == "si") ? 0  : 42;
                $personaFisica =($tipoPersona == "fisica")? 1 : 0;
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
                if($personaConfianza != 'si'){

                    $confianza = 12;
                }else{

                    $confianza = "0";
                }

                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);

                 if($tipoPersona != 'moral'){

                    $actaMoral = 28;
                    $poderNotarial = 8;
                    $carpetaPrograma = 46;
                }else{

                    $actaMoral = "0";
                    $poderNotarial = "0";
                    $carpetaPrograma = "0";
                }

                array_push($requisitos,$contrato_arrendamiento);
                array_push($requisitos,$licencia_anterior);
                array_push($requisitos,$actaMoral);
                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$carpetaPrograma);
                array_push($requisitos,$cartaNotariada);

                //envio la informacion al siguiente controlador
                 $this->session->set_flashdata('requisitos',$requisitos);
                 $this->session->set_flashdata('formularios',$formularios);
                 $this->session->set_flashdata('subTramiteId',$subTramite);
                 $this->session->set_flashdata('personaFisica',$personaFisica);
                 $this->session->set_flashdata('razonSocial',$razonSocial);
                redirect(base_url()."DesgloceTramite/index/6","");
                break;
            case '7':
              //tramite Licencia de licencia de construcción
              $personaConfianza = $this->input->post("tipoGestor");
              $tipoPersona = $this->input->post("options");
              $planosAnt = ($this->input->post('planos') == 'no')? 48 : 0;//cuando tienes planos anteriores
              $monumentos = $this->input->post("monumentos");
              $subTramite = $this->input->post("tipo_SubTramite7");
              $personaFisica =($tipoPersona == "fisica")? 1 : 0;
              //Guardamos el tipo de licencia de construcción
              $tipoLicenciaConstruccion = $this->input->post("tipo_SubTramite7");
              $this->session->set_flashdata('tipoLicenciaConstruccion',$tipoLicenciaConstruccion);
              $formularios = array();
              $requisitos = array();
              $cartaNotariada = ($personaConfianza == "si") ? 0 : 12; 
              $Monumentos = ($monumentos != "si")? 65 : 0;//si esta en monumentos
              $razonSocial =($tipoPersona == "moral")? 0 : 59;
              //Validamos el formulario de confianza
              $confianza = ($personaConfianza != 'si') ? 12 :0;
              if ($tipoLicenciaConstruccion == 19 || $tipoLicenciaConstruccion == 16 || $tipoLicenciaConstruccion == 14) {//Licencia de Construcción de Obra Menor, Licencia de Bardeo, Licencia de Acabados
                //Formularios
                $dictamenAnterior=($tipoLicenciaConstruccion == 14)?32:0;
                $croquis=($tipoLicenciaConstruccion == 14)?1:0;
                $formularios = array(39,40,$confianza,$dictamenAnterior,$razonSocial);
                //requisitos
                $requisitos_no_mostrar =  array(2,15,32,48,49,50,51,54,55,56,57,58,59,60,61,62,63,64,65,66,67,68,$confianza,$croquis,85,83,84,86,87,88,89,90,91,92,93,94);
              }//if
              else if ($tipoLicenciaConstruccion == 18) {//Licencia dde MOdificación de Fachada
                //Formularios
                $formularios = array(39,40,$confianza,$razonSocial);
                //requisitos
                $requisitos_no_mostrar =  array(2,15,32,50,51,54,56,57,58,59,60,61,62,63,64,65,67,68,78,$confianza,$planosAnt,85,83,84,86,87,88,89,90,91,92,93,94);
              }//if
              else if ($tipoLicenciaConstruccion == 17 || $tipoLicenciaConstruccion == 21) {//Licencia de Casa Habitación / Obra nueva
                //Formularios
                $formularios = array(39,40,$confianza,32,$razonSocial);
                //requisitos
                    $requisitos_no_mostrar =  array(2,15,32,47,48,49,54,55,57,58,60,61,62,63,64,66,68,78,$confianza,$Monumentos);
            }//if
              else if ($tipoLicenciaConstruccion == 22 || $tipoLicenciaConstruccion == 23) {//Licencia para Ampliación,  modificación, remodelación y revalidación
                $croquis=($tipoLicenciaConstruccion == 23)?1:0;
                //Formularios
                $formularios = array(39,40,$confianza,$razonSocial);
                //si es revalidacion
                if($tipoLicenciaConstruccion == 23){
                    $rValidacion =  0;//en revalidacion
                    $rValidacion1 = 0;//en revalidacion
                    $rValidacion2 = 0;//en revalidacion
                }else{
                    $rValidacion =  0;//no revalidacion
                    $rValidacion1 = 67;//no revalidacion
                    $rValidacion2 = 0;//no revalidacion
                }               
                //requisitos
                if($tipoLicenciaConstruccion == 22){
                $requisitos_no_mostrar =  array(2,15,32,47,49,56,48,58,59,60,61,62,63,64,65,78,$confianza,$rValidacion,$rValidacion1,$rValidacion2,$croquis,83,84,86,87,88,89,90,91,92,93,94);
                }else if($tipoLicenciaConstruccion == 23){
                    $requisitos_no_mostrar =  array(2,15,32,47,49,56,57,48,58,59,60,61,62,63,64,65,78,$confianza,$rValidacion,$rValidacion1,$rValidacion2,$croquis,83,84,86,87,88,89,90);
                }
              }//if
              else if ($tipoLicenciaConstruccion == 15) {//Terminación de obra
                //Formularios
                $formularios = array(32,33,34,36,37,$confianza,$razonSocial);
                //requisitos
                $requisitos_no_mostrar =  array(2,15,20,32,47,48,49,50,51,54,55,56,62,63,64,65,66,67,68,78,$confianza,85,83,84,86,87,88,89,90,91,92,93,94);
              }//if
              else if ($tipoLicenciaConstruccion == 20) {//REGULARIZCIÓN DE OBRA
                $croquis=($tipoLicenciaConstruccion == 20)?1:0;
                //Formularios
                $formularios = array(39,40,$confianza,$razonSocial);
                //requisitos
                $requisitos_no_mostrar =  array(15,32,47,48,49,50,51,54,55,56,57,58,59,60,61,63,65,66,67,68,78,$confianza,$croquis,83,84,86,87,88,89,90,91,92,93,94);
              }//if

              $poderNotarial = ($tipoPersona == "moral") ? 0 : 5;
              $acta_constitutiva = ($tipoPersona == "moral") ? 0 : 7;

              array_push($requisitos_no_mostrar,$poderNotarial);
              array_push($requisitos_no_mostrar,$acta_constitutiva);
              //envio la informacion al siguiente controlador
              $this->session->set_flashdata('requisitos',$requisitos_no_mostrar);
              $this->session->set_flashdata('formularios',$formularios);
              $this->session->set_flashdata('tipo_licencia_construccion',$tipoLicenciaConstruccion);
              $this->session->set_flashdata('subTramiteId',$subTramite);
              $this->session->set_flashdata('personaFisica',$personaFisica);
              $this->session->set_flashdata('razonSocial',$razonSocial);
              redirect(base_url()."DesgloceTramite/index/7","");
                break;
                case '8':
                //tramite informe uso de suelo
                //obtengo los datos de las perguntas del perfilado
                //y los relaciono dependiento de las respuestas
                $personaConfianza = $this->input->post("tipoGestor");
                $tipoPersona = $this->input->post("options");
                $subTramite = $this->input->post("tipo_SubTramite8");
                $personaFisica =($tipoPersona == "fisica")? 1 : 0;
                $razonSocial =($tipoPersona == "moral")? 0 : 59;
              
                $formularios = array();
                $requisitos = array();

                if($personaConfianza != 'si'){

                    $confianza = 12;
                }else{

                    $confianza = "0";
                }

                array_push($formularios,$confianza);
                array_push($formularios,$razonSocial);

                 if($tipoPersona != 'moral'){

                    $poderNotarial = 5;
                    $actaConstitutiva = 7;

                }else{

                    $poderNotarial = "0";
                    $actaConstitutiva = "0";
                }

                array_push($requisitos,$poderNotarial);
                array_push($requisitos,$actaConstitutiva);
                array_push($requisitos,$confianza);

                //envio la informacion al siguiente controlador
                 $this->session->set_flashdata('requisitos',$requisitos);
                 $this->session->set_flashdata('formularios',$formularios);
                 $this->session->set_flashdata('subTramiteId',$subTramite);
                 $this->session->set_flashdata('personaFisica',$personaFisica);
                 $this->session->set_flashdata('razonSocial',$razonSocial);
                redirect(base_url()."DesgloceTramite/index/8","");
  
                break;

            default:
                # code...
                break;
        }
    }
}
