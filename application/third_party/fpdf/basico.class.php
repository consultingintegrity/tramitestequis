<?php
class PDF extends MYPDF
{
	protected $_server_link = NULL;
	
  function setServerLink($server_link)
	{
		$this->_server_link = $server_link;
  }

  function getServerLink(){
        return $this->_server_link;
  }
	function Header()
	{
			// Logo
      $this->Image(''.$this->getServerLink().'elmarques/control/images/logo_gobmunicipal.jpg',20,null,40,16);
      $this->Image(''.$this->getServerLink().'elmarques/control/images/logo_progreso.jpg',160,2,40,30);	
			$this->anio=date('Y');
			$this->SetFillColor(205,21,29);
			$this->SetTextColor(0);

	}

	// Pie de página
	function Footer()
	{
		$this->SetY(-40);
		$this->SetX(null);
		//$this->Image(''.$this->getServerLink().'/control/images/centenario.jpg',82,null,50,18);
		$this->SetFillColor(255,179,179);
		$this->SetY(-22);
		$this->SetFont('Times','',6);
		//$this->Cell(null,5,utf8_decode('"'.$this->anio.', AÑO DEL CENTENARIO DE LA PROMULGACIÓN DE LA CONSTITUCIÓN POLÍTICA DE LOS ESTADOS UNIDOS MEXICANOS"'),0,1,'C');
		$this->Rect(15,262,185,0,'F');
		$this->Rect(15,263,185,2,'F');
		$this->SetY(-15);
		$this->SetFont('Times','',8);
		$this->Cell(null,5,utf8_decode('Venustiano Carranza No. 2, La Cañada, El Marqués, Querétaro'),0,1,'C');
		$this->SetY(-12);
		$this->Cell(null,5,'CP 76240, TEL. (442) 238 84 00',0,1,'C');
		$this->SetY(-9);
		$this->Cell(null,5,'www.elmarques.gob.mx',0,1,'C');
	}

}
?>