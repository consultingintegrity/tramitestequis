<?php
  use Chat\Chat;

  use Ratchet\Server\IoServer;
  use Ratchet\Http\HttpServer;
  use Ratchet\WebSocket\WsServer;
  require APPPATH . '/third_party/Realtime/vendor/autoload.php';


  /**
   *
   */
  class Server
  {

    function __construct()
    {

      $server = IoServer::factory(
          new HttpServer(
              new WsServer(
                  new Chat()
              )
          ),
          2000
      );
      $server->run();
    }//construct
  }//Class


?>
