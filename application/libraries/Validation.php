<?php
/**
 * @author Guadalupe-Valerio
 * @version 1.0
 */
class Validation
{
    /**
     * summary
     */
    public function __construct()
    {
    	//Obtenemos la instancia ci para cceder a las librerias de codeigniter
        $this->ci =& get_instance();
    }//construct

    public function validForm($arr_validations,$arr_msg){
        $this->ci->form_validation->set_rules($arr_validations);
        $this->ci->form_validation->set_message($arr_msg);
        return $this->ci->form_validation->run();
    }//validForm

    /**
     * Esta función nos devuelve un true o false si los datos son igules
     */
    public function existe($email,$email_db,$pwd,$pwd_db){
        $valid = false;
        if (strnatcasecmp ($email,$email_db) === 0 && $pwd === $pwd_db) {
            $valid = true;
        }//if
        return $valid;
    }//validForm

    /**
     * esta función redireccionar al usuario si esta logueado
     */
    public function redirect(){
        //validamos que exista una session
        if ($this->ci->session->userdata('logged_in')) {
            if($this->ci->session->userdata('rol')[0]->nombre == "Administrador") {
                redirect(base_url().'Administrador');
            }//if
            else if ($this->ci->session->userdata('rol')[0]->nombre == "Ciudadano"){
                redirect(base_url()."Ciudadano");
            }//if
            else {
                redirect(base_url().'Funcionario');
            }//else
        }//if
        else{
            redirect(base_url(),"refresh");
        }//else
    }//redirect


    public function confirmarpass($password,$confirpassword) {
        $van=($password===$confirpassword) ? true : false;
        return $van;
        }//confirmarpass

        public function existemail($correo,$correo_bd) {
        $van=($correo===$correo_bd) ? true : false;
        return $van;
        }//existemail

         public function acento($str) {

        return (preg_match("/^(?:[\s']*[a-zA-Z\pL][\s']*)+$/u", $str)) ? FALSE : TRUE;

        }//acento

    function fecha($fecha)
    {
      // match the format of the date
      if (preg_match ("/^([0-9]{4})-([0-9]{2})-([0-9]{2})$/", $fecha, $parte))
      {
        // check whether the date is valid or not
        if (checkdate($parte[2],$parte[3],$parte[1])) {
          return true;
        } else {
          return false;
        }//else
      } else {
        return false;
      }
    }//funcion fecha

    public function formatoFechaDia($fecha){
      $str_fecha = ""; //Variable que almacenará la cadena de la fecha
      switch (date("D",strtotime($fecha))) {
        case 'Mon':
          $str_fecha = "Lunes" ;
        break;
        case 'Tue':
          $str_fecha = "Martes";
        break;
        case 'Wed':
          $str_fecha = "Miércoles";
        break;
        case 'Thu':
          $str_fecha = "Jueves";
        break;
        case 'Fri':
          $str_fecha = "Viernes";
        break;
        case 'Sun':
          $str_fecha = "Sábado";
        break;
        case 'Sat':
          $str_fecha = "Domingo";
        break;
        default:
          $str_fecha = date("D",strtotime($fecha));
        break;
      }//switch Día
      //Añadimos el día
      $str_fecha .= " " . date("d",strtotime($fecha));
      return $str_fecha;

    }//formatofecha

    public function formatoFechaMes($fecha){
      $str_fecha = ""; //Variable que almacenará la cadena de la fecha
      switch (date("M",strtotime($fecha))) {
        case 'Jan':
          $str_fecha = "Enero" ;
        break;
        case 'Feb':
          $str_fecha = "Febrero";
        break;
        case 'Mar':
          $str_fecha = "Marzo";
        break;
        case 'Apr':
          $str_fecha = "Abril";
        break;
        case 'May':
          $str_fecha = "Mayo";
        break;
        case 'Jun':
          $str_fecha = "Junio";
        break;
        case 'Jul':
          $str_fecha = "Julio";
          break;
        case 'Aug':
          $str_fecha = "Agosto";
          break;
        case 'Sep':
          $str_fecha = "Septiembre";
          break;
        case 'Oct':
          $str_fecha = "Octubre";
          break;
        case 'Nov':
          $str_fecha = "Noviembre";
          break;
        case 'Dec':
          $str_fecha = "Diciembre";
        break;
        default:
          $str_fecha = date("D",strtotime($fecha));
        break;
      }//switch Día
      //Añadimos el día
      $str_fecha = "" . $str_fecha;
      return $str_fecha;
    }//formatofechaMes

}//class

 ?>
