<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 * Libraries License
 *
 * This Libraries for ...
 * 
 * @package		CodeIgniter
 * @category	Libraries
 * @author    Pepe Droid
 * @param     ...
 * @return    ...
 *
 */

class License
{
  private $key_license = "";

  public function __construct()
  {
    $this->key_license =  file_get_contents(base_url()."license.json");//Arcivo de licencia para podder hacer uso del sitema
  }//__construct

  public function authenticate(){
    //validamos que se haya ingresado la contraseña correctamente
    $json_license = json_decode($this->key_license);
    if ($json_license->data[0]->values[0]->validate == "FALSE") {
      // mandar a página para solicitar contraseña
      redirect(base_url()."Licencia");
    }//if
  }//function

  public function register(){

    //sobreesribimo el archvo de licencia
    $file = "./license.json";
   //fopen($file,"w+");
    //fwrite($file, $this->key_license);
    //fclose($file);
    //$json_string = json_encode($this->key_license);
    //var_dump($this->key_license);
    file_put_contents($file, json_encode($this->key_license));

  }
  public function setKeyLicense($key_license){
    $this->key_license = $key_license;
  }
  public function getKeyLicense(){
    return $this->key_license;
  }
}//class

/* End of file License.php */
/* Location: ./application/libraries/License.php */