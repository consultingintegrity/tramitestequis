<?php
/**
 * @author Pepe-Droid
 */
class Json
{
  private $form_array;
  private $field_value;

  public function __construct()
  {
    //Obtenemos la instancia ci para cceder a las librerias de codeigniter
    $this->ci =& get_instance();
  }//construct

  /**
   * @param Array $json_array
   * @return Array $form_array
   * Obtenemos la posicion de un formulario
   */
  public function formData($json_array,$form){
    //Recorremos el json hasta obtener los elementos del formulario desdeado desde la otra clase
    for ($i=0; $i < count($json_array); $i++) {
      if ($json_array[$i] != null) {
        //buscamos el nombre del formulario que deseamos obtener o el id
        if ($form == $json_array[$i]["id_form"] || $form == $json_array[$i]["name_form"]) {
          $this->form_array = $json_array[$i]["content"];
          break;
        }//if
      }//if
    }//for
  }//formData

  /**
   * Obtenemos el valor deseado del json_array
   *  @param string $input_value
   * @return string $field_value
   */
  public function valueFieldForm($input_value){
    for ($i=0; $i < count($this->form_array) ; $i++) {
      if ($this->form_array[$i]["field"] === $input_value) {
        $this->field_value = $this->form_array[$i]["value"];
      }//if
    }//for
  }//valurFieldForm

  /**
   * Obtenemos todo el contenido de un formuario
   */
  public function valueForm(){
    for ($i=0; $i < count($this->form_array) ; $i++) {
      $this->field_value .= $this->form_array[$i]["value"] . " ";
    }//for
  }//valueForm

  /**
     * @return json array
     * BUSCAMOS LA INFORMACIÓN POR ID DE FOTMULARIO Y DEVOLVEMOS LOS DATOS EN UN ARRAY
     */
  public function json_decode_info_form($json_array_data,$id_form){
      $info_solicitud = $json_array_data;
      $form_solicitud = array();//Fuardamos la información del formulario
      //Recorremos el array para hasta encontrar la posiciópn del id del foprmulario que queremos obtener
      for ($i=0; $i < count($info_solicitud); $i++) {
        $data_json = json_decode($info_solicitud[$i]->informacion,1);//Decodificamos el String a formato JSON
        for ($x=0; $x < COUNT($data_json) ; $x++) {
          if (!empty($data_json[$x]["id_form"])) {//Buscamos la posicion id_form
            if ($data_json[$x]["id_form"] == $id_form) {
              for ($j=0; $j < count($data_json[$x]["content"]); $j++) {
                $array_aux = [
                              "field"=>$data_json[$x]["content"][$j]["field"],
                              "value" => $data_json[$x]["content"][$j]["value"]
                            ];
                //$form_solicitud[$aux_poscicion_array] = $data_json[$x]["content"][$j]["field"];//Nombre del campo
                //$form_solicitud[$aux_poscicion_array] = $data_json[$x]["content"][$j]["value"];//valor del campo
                array_push($form_solicitud, $array_aux);
              }//for
            }//if
          }//if
        }//for
      }//for
      return $form_solicitud;
    }//json_decode_info_form


  /**
   * Get the value of Form Array
   *
   * @return mixed
   */
  public function getFormArray()
  {
      return $this->form_array;
  }

  /**
   * Set the value of Form Array
   *
   * @param mixed form_array
   *
   * @return self
   */
  public function setFormArray($form_array)
  {
      $this->form_array = $form_array;

      return $this;
  }


  /**
   * Get the value of Field Value
   *
   * @return mixed
   */
  public function getFieldValue()
  {
      return $this->field_value;
  }

  /**
   * Set the value of Field Value
   *
   * @param mixed field_value
   *
   * @return self
   */
  public function setFieldValue($field_value)
  {
      $this->field_value = $field_value;

      return $this;
  }

  public function validar($json_array,$form){
   
    $bandera=0;
    //recorro para ver el formulario
    for ($i=0; $i < count($json_array); $i++) {
      if ($json_array[$i] != null) {
        if ($form == $json_array[$i]["id_form"]) {
          $bandera= 1;
        }else{
          $bandera;
        }
    }
    return $bandera;
  }
}

}//class

?>
