<style>
  .alert {
      padding: 20px;
      background-color: #f44336;
      color: white;
  }
  .yes {
      padding: 20px;
      background-color: #008000;
      color: white;
  }
  .closebtn {
      margin-left: 15px;
      color: white;
      font-weight: bold;
      float: right;
      font-size: 22px;
      line-height: 20px;
      cursor: pointer;
      transition: 0.3s;
  }

  .closebtn:hover {
      color: black;
  }
</style>


<body class="stretched">

<?php if (!empty($this->session->flashdata('error'))): ?>
  <div class="alert">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>Upps!</strong><?=$this->session->flashdata('error')?>
  </div>

  <?php endif?>
<?php if (!empty($this->session->flashdata('good'))): ?>
  <div class="yes">
    <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
    <strong>El Funcionario se ha registrado con exito</strong><?=$this->session->flashdata('good')?>
  </div>

<?php endif?>

  <!-- Page Title
  ============================================= -->
  <section id="page-title" class="page-title-mini">
    <div class="container clearfix">
        <h1 align="center">Bienvenido administrador <?=$this->session->userdata('primer_nombre');?></h1>
    </div>
  </section><!-- #page-title end -->
  <!-- Content
  ============================================= -->
  <section id="content">
    <div class="content-wrap">
      <div class="container clearfix">
        <div class="continer">
          <body>
            <h1>Registrar funcionario </h1>
            <span>En este apartado puedes registrar a funcionarios para que puedan interactuar con la plataforma.</span>
            <br><br><br><br>
              <div class="col-md-12">
                <form name="Funcionario" id="Funcionario" action="Administrador/registrar_fun" method="POST">
                  <div class="form-group">
                    <h3>Información Personal</h3>
                    <div class="col-md-3">
                        <label for="txtPrimerNom">Primer Nombre:</label> <input type="text" name="txtPrimerNom" class="form-control"  />
                    </div>
                    <div class="col-md-3">
                        <label for="txtSegundoNom">Segundo Nombre: </label><input type="text" name="txtSegundoNom" class="form-control" />
                    </div>
                    <div class="col-md-3">
                        <label for="txtPrimerAp">Primer Apellido: </label><input type="text" name="txtPrimerAp" class="form-control"/>
                    </div>
                    <div class="col-md-3">
                        <label for="txtSegundoAp">Segundo Apellido: </label><input type="text" name="txtSegundoAp" class="form-control"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="txtFecha">Fecha de nacimiento: </label><input type="date" name="txtFecha" max="2070-12-31" class="form-control" />
                    </div>
                    <div class="form-group col-md-6">
                          <label for="optGenero">Género:</label><select id="optGenero" name="optGenero" class="form-control">
                          <option value="M">Masculino</option>
                          <option value="F">Femenino</option>
                          </select>
                    </div>
                  </div>
                  <br><br><br><br>
                  <div class="form-group">
                    <h3>Información de Contacto</h3>
                    <div class="form-group col-md-3">
                        <label for="txtMovil">Móvil (Personal):</label><input type="text" name="txtMovil"class="form-control" maxlength="13" />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtTelOficina">Teléfono de Oficina:</label><input type="text" name="txtTelOficina"class="form-control" maxlength="10" />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtExtension">Extensión:</label><input type="text" name="txtExtension"class="form-control" maxlength="3" />
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtCorreo">Correo Electrónico:</label> <input type="text" name="txtCorreo" class="form-control"/>
                    </div>
                  </div>
                  <br><br><br><br>
                  <div class="form-group">
                    <h3>Dirección del funcionario</h3>
                    <div class="form-group col-md-6">
                        <label for="optEstado">Estado: </label><select id="optEstado" name="optEstado" class="form-control">
                          <option value="">Selecciona</option>
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="optMunicipio">Municipio:</label> <select id="optMunicipio" name="optMunicipio" class="form-control"></select>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtCalle">Calle:</label> <input type="text" name="txtCalle" class="form-control"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtColonia">Colonia:</label> <input type="text" name="txtColonia" class="form-control"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtNumeroI">Numero interior:</label> <input type="text" name="txtNumeroI" class="form-control"/>
                    </div>
                    <div class="form-group col-md-3">
                        <label for="txtNumeroE">Número exterior: </label><input type="text" name="txtNumeroE" class="form-control"/>
                    </div>
                  </div>
                  <br><br><br><br>
                  <div class="form-group">
                    <h3>Información de Acceso</h3>
                    <div class="form-group col-md-4">
                        <label for="optDireccion">Dependencia:</label><select id="optDireccion" name="optDireccion" class="form-control"></select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="optDependencia">Dirección: </label><select id="optDependencia" name="optDependencia" class="form-control"></select>
                    </div>
                    <div class="form-group col-md-4">
                        <label for="optRol">Rol: </label><select id="optRol" name="optRol" class="form-control"></select>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="txtContra">Contraseña: </label><input type="password" name="txtContra" class="form-control"/>
                    </div>
                    <div class="form-group col-md-6">
                        <label for="txtConfirmarContra">Confirma contraseña: </label><input type="password" name="txtConfirmarContra" class="form-control"/>
                    </div>
                  </div>
                  <div align="right">
                    <input type="submit" name="btnRegistrar" value="Registrar" class="btn btn-primary btn-lg" />
                  </div>
                </form>
              </div>
          </body>
        </div>
      </div>
    </div>
  </section><!-- #content end -->
<!-- script que llena el select de estados  -->
<script type="text/javascript">
function llenarDireccion(){
  $.ajax({
    url: '<?=base_url()?>Catalogo/getDireccion',
    dataType: 'json',
    success:function(response){
      if (response.response == 200) {
        $.each(response["direccion"], function(index, val) {
           /* iterate through array or object */
           if (val.id != 1) {
             $("#optDireccion").append('<option value='+val.id+' title='+val.descripcion+'>'+val.nombre+'</option>');
           }//if
        });
        //enviamos parametro a select dependendcia
        var id_direccion = $("#optDireccion").val();
        llenarDependencia(id_direccion);

      }//if
      else{
        $("#optDireccion").append('<option value="0" title="No hay dirección">No hay direcciones Disponibles</option>');
      }//else
    }//success
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });


}//llenarDireccion

function llenarDependencia(id_direccion){
  $.ajax({
    url: '<?=base_url()?>Catalogo/getDependencia',
    type: 'POST',
    dataType: 'json',
    data: {id_direccion: id_direccion},
    success:function(response){
      if (response.response == 200) {
        $("#optDependencia").empty();
        $.each(response["dependencia"], function(index, val) {
           /* iterate through array or object */
           if (val.id != 1) {
             $("#optDependencia").append('<option value='+val.id+' title='+val.descripcion+'>'+val.nombre+'</option>');
           }//if
        });
      }//if
      else{
        $("#optDependencia").append('<option value="0" title="No hay dependencias">No hay dependencias asociadas a esta dirección</option>');
      }//else
    },//success
    error:function(data){
      alert("error de conexión con el servidor");

    }//error
  })
  .done(function() {
    console.log("success");
  })
  .fail(function() {
    console.log("error");
  })
  .always(function() {
    console.log("complete");
  });
}
  $(document).ready(function() {
    llenarDireccion();

    //listeners
    $( "#optDireccion" ).change(function() {
      //ajax que llena el select de municipios en base a el estado seleccionado
      var id_direccion = $("#optDireccion").val();
      llenarDependencia(id_direccion);
      });//change function
    $.ajax({
      url:  '<?=base_url()?>Catalogo/getEstados',
      type:  'POST',
      dataType: 'json',
      success:function(estados){
        $.each(estados["estados"], function(index, val) {
          /* iterate through array or object */
          
            $("#optEstado").append('<option value='+val.id+'>'+val.nombre+'</option>');
          
        });
        $( "#optEstado" ).change(function() {
          //ajax que llena el select de municipios en base a el estado seleccionado
          var id_estado = $("#optEstado").val();
            if(id_estado) {
              $.ajax({
                url: '<?=base_url()?>Catalogo/getMunicipios/'+id_estado,
                type: "GET",
                dataType: "json",
                success:function(municipios) {
                  $('#optMunicipio').empty();
                    $.each(municipios["municipios"], function(index, val) {
                      $("#optMunicipio").append('<option value='+ val.id +'>'+ val.nombre +'</option>');
                    });//each
                  }//success
                });//ajax
            }//if
            else{
              $('#optMunicipio').empty();
            }//else
          });//change function
        }//success
      })//ajax
      .done(function() {
        console.log("success");
      })
      .fail(function() {
        console.log("error");
      })
      .always(function() {
        console.log("complete");
      });
      $.ajax({
      url:  '<?=base_url()?>Catalogo/getRol',
      type:  'POST',
      dataType: 'json',
      success:function(rol){
        $.each(rol["rol"], function(index, val) {
           /* iterate through array or object */
          $("#optRol").append('<option value='+val.id+'>'+val.nombre+'</option>');
        });
      }
    })
    .done(function() {
      console.log("success");
    })
    .fail(function() {
      console.log("error");
    })
    .always(function() {
      console.log("complete");
    });
    });//readyDocument
</script>
