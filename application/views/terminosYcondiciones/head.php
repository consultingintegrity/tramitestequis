<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?= $title ?></title>
<link rel="shortcut icon" href="<?= base_url()?>plantilla/images/img-tequis/logo.png" />
	<style type="text/css">
		body{
			background-color:white;
			font-family: Gotham, "Helvetica Neue", Helvetica, Arial, "sans-serif";
			width: 100%;
		}
		.contenido{
			margin-top: 0;
		  margin-bottom: 0;
		  margin-left: auto;
		  margin-right: auto;
			background-color: #F2F2F2;
			width: 800px;
			min-height: 800px;
			position: relative;


		}
		.texto{
			padding:40px;
		}
		p{
			text-align:justify;
			line-height: 2em;
			font-size: 0.8em;

		}
		img{
			  margin: 0 auto;
			  display: block;
			}
		.pie{
			position: absolute;
			left: 0;
			bottom: 0;
			width: 100%;

		}
		hr{
			border: 0; height: 0; border-top: 1px solid rgba(0, 0, 0, 0.1); border-bottom: 1px solid rgba(255, 255, 255, 0.3);
		}
	</style>
</head>
