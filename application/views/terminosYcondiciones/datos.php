<body>
	<div class="contenido">
		<header> <img src="<?= base_url() ?>plantilla/images/header.png" width="800" height="auto"></header>
		<div class="texto">
			<?php if ($val == 1) : ?>
				<h3><strong>TÉRMINOS Y CONDICIONES DE USO</strong></h3>
				<p>
					Estimado usuario, esta página establece los &quot;Términos y Condiciones&quot; que regulan el uso del
					Portal denominado Plataforma de trámites del Municipio de Tequisquiapan con dirección
					electrónica <a href="<?= base_url() ?>"><?= base_url() ?></a> (en adelante, el &quot;Portal&quot;). Le
					pedimos que lea cuidadosamente los siguientes términos y condiciones de uso, ya que por el
					simple uso o acceso a cualquiera de las páginas que integran el Portal se entenderá que
					acepta y acuerda obligarse en los términos y condiciones aquí descritos. Si no acepta estos
					Términos y Condiciones, no podrá tener acceso al mismo. La información ingresada a este
					Portal es para uso exclusivo del Municipio de Tequisquiapan, a fin de facilitar los trámites de
					carácter municipal para con los ciudadanos.<br>
				</p>
				<h3><strong>BENEFICIOS Y ALCANCE DEL PORTAL</strong></h3>
				<p>
					En todo momento los ciudadanos podrán realizar los trámites de manera presencial ante las
					instancias respectivas. El uso del portal no impide que se realicen presencialmente. Se hace
					del conocimiento del usuario que los documentos presentados por medios de identificación
					electrónica a través del Portal, producirán los mismos efectos que las leyes otorgan a los
					documentos firmados autógrafamente, salvo los casos que la ley así determine.
					<br>
					El Gobierno Municipal de Tequisquiapan procesa y usa los datos e información personal del
					usuario sólo para los servicios/procesos determinados dentro del Portal, con la finalidad de
					ahorrarle tiempo, evitando así el traslado hasta la Presidencia Municipal para todo el proceso
					de los trámites. Dichos datos e información podrán ser incluidos dentro de los informes
					estadísticos que se elaboren para el seguimiento de avances institucionales del Municipio de
					Tequisquiapan, tomando en cuenta lo señalado en las Políticas de Privacidad del Portal, que se
					encuentran en esté texto.<br>
				</p>
				<h3><strong>USO DEL CONTENIDO DEL PORTAL</strong></h3>
				<p>
					Los contenidos del Portal tales como textos, gráficos, imágenes, logos, íconos de botón,
					software y cualquier otro contenido, deberán utilizarse por el usuario únicamente para los fines
					para los cuales ha sido creado. El uso no autorizado del contenido del Portal será sancionado
					en términos de la legislación aplicable. De igual forma se sancionará penalmente al que de
					manera dolosa o con fines de lucro interrumpa o interfiera las comunicaciones alámbricas,
					inalámbricas o de fibra óptica, sean telefónicas o satelitales, por medio de las cuales se
					transfieran señales de datos.
					<br>
				</p>
				<h3><strong>USO PERMITIDO DEL PORTAL</strong></h3>
				<p>
					El Portal sólo podrá ser utilizado para la realización y/o conclusión de distintos trámites
					relacionados con el Municipio de Tequisquiapan. El Portal también podrá ser utilizado para
					consultar, entre otros, el estado de los trámites iniciados, información registrada previamente,
					así como, órdenes de pago del trámite que se solicite por parte del ciudadano cuando así
					corresponda, los trámites que pueden realizarse en el sitio, los documentos que se necesitan y
					la legislación que resulta aplicable.
					<br>
					El usuario será responsable de la veracidad, certeza, realidad, legalidad y licitud de la
					información proporcionada en el Portal y será sancionado penalmente por la falsedad de las
					declaraciones que haga ante la autoridad y por la falsificación de documentos. Adicionalmente,
					el usuario es responsable por los daños y/o perjuicios que se pudieran ocasionar a terceros,
					incluyendo el Estado, por el mal uso del Portal, o porque al utilizarlo se infrinjan, directa o
					indirectamente entre otros, los derechos de propiedad intelectual y/o se violen pactos o
					convenios de confidencialidad. El usuario asume total responsabilidad en caso de que la
					información proporcionada sea falsa, engañosa, discriminatoria, difamatoria, obscena,
					ofensiva, contraria al orden público o a las buenas costumbres, en los términos y bajo las
					sanciones contenidas en la legislación que resulte aplicable.<br>
				</p>
				<h3><strong>PROHIBICIONES</strong></h3>
				<p>
					Se prohíbe a los usuarios violar o intentar violar la seguridad del Portal, incluyendo pero no
					limitándose a: 1) acceder a datos que no estén destinados al usuario o entrar en un servidor o
					cuenta cuyo acceso no esté autorizado al usuario, 2) evaluar o probar la vulnerabilidad de un
					sistema o red, o violar las medidas de seguridad o identificación sin la adecuada autorización y
					3) intentar impedir el servicio a cualquier usuario, anfitrión o red, incluso mediante el envío de
					virus al Portal o bloqueos del sistema.
					<br>
					Las violaciones de la seguridad del sistema o de la red pueden resultar en responsabilidades
					civiles o penales en términos de la legislación aplicable.
					<br>
					SE PROHÍBE ESPECÍFICAMENTE EL USO DEL PORTAL PARA CUALQUIERA DE LOS
					SIGUIENTES FINES:
					<br>
				<ul>
					<li>Usar el Portal para cualquier propósito distinto al fin para el cual fue creado.</li>
					<li>Impedir el adecuado funcionamiento del Portal, mediante el uso de cualquier
						mecanismo, software o rutina.</li>
					<li>Intentar descifrar, separar u obtener el código fuente de cualquier programa de
						software que comprenda o constituya una parte del Portal.</li>
					<li>La extracción, a través de una transferencia temporal o permanente a cualquier medio,
						de la totalidad o de una parte del contenido de las bases de datos disponibles en el
						Portal, cualquiera que sea el medio utilizado o la forma en que se realice.</li>
					<li>Interrumpir o interferir las comunicaciones alámbricas, inalámbricas o de fibra óptica,
						sean telefónicas o satelitales, por medio de las cuales se transfieran señales de datos.</li>
				</ul><br>
				</p>
				<h3><strong>USO DE FUNCIONES Y ARCHIVOS "COOKIES"</strong></h3>
				<p>
					El usuario que tenga acceso al Portal, acuerda recibir las cookies transmitidas por los
					servidores del Municipio de Tequisquiapan. Una &quot;Cookie&quot; es un componente que almacena
					datos para asegurar la confidencialidad de los datos que pasan por la red de internet, con el
					fin de garantizar que la información no sea observada por otros usuarios.
					<br>
				</p>
				<h3><strong>CLAVE DE USUARIO Y CONTRASEÑA</strong></h3>
				<p>
					El usuario debe mantener la confidencialidad de su clave de usuario y de su contraseña, es
					responsable del mal uso que se les dé, así como del acceso a las páginas de terceros.
					<br>
				</p>
				<h3><strong>MODIFICACIONES</strong></h3>
				<p>
					El usuario debe mantener la confidencialidad de su clave de usuario y de su contraseña, es
					El Municipio de Tequisquiapan, tendrá el derecho de modificar en cualquier momento el
					contenido y alcance de los presentes “Términos y Condiciones de Uso”, lo cual se hará del
					conocimiento del usuario de manera oportuna, por lo cual deberá leerlos atentamente cada
					vez que se le haga de conocimiento y pretenda utilizar el Portal.
					<br>
				</p>
				<h3><strong>USO DEL CONTENIDO DEL PORTAL</strong></h3>
				<p>
					Los contenidos del Portal tales como textos, gráficos, imágenes, logos, íconos de botón,
					software y cualquier otro contenido, deberán utilizarse por el usuario únicamente para los fines
					para los cuales ha sido creado. El uso no autorizado del contenido del Portal será sancionado
					en términos de la legislación aplicable. De igual forma se sancionará penalmente al que de
					manera dolosa o con fines de lucro interrumpa o interfiera las comunicaciones alámbricas,
					inalámbricas o de fibra óptica, sean telefónicas o satelitales, por medio de las cuales se
					transfieran señales de datos.
					<br>
				</p>
				<h3><strong>LEYES APLICABLES Y JURISDICCIÓN EN CASO DE CONTROVERSIA</strong></h3>
				<p>
					En caso de controversia derivada de la interpretación o cumplimiento de los presentes
					“Términos y Condiciones de Uso”, de las Políticas de Privacidad o de cualquier otro documento
					relevante del Portal, el usuario está de acuerdo en que serán aplicables las leyes municipales y
					estatales de los Estados Unidos Mexicanos y competentes los tribunales del Estado de
					Querétaro, renunciando expresamente a cualquier otro fuero o jurisdicción que pudiera
					corresponderle en razón de su domicilio presente o futuro.
					<br>
					Con fundamento en los artículos 3, fracción V, 5, 13 y 17 de la Ley de Acceso a la Información
					Gubernamental del Estado de Querétaro, así como, los artículos 4, 6, 10, 12, 19 y 20 Ley de
					protección de datos personales en posesión de sujetos obligados del Estado de Querétaro, así
					como de la normatividad en materia de protección de datos personales, el Municipio de
					Tequisquiapan, en su calidad de sujeto obligado, le informa que los datos proporcionados se
					encuentran salvaguardados, emitiendo el siguiente:<br>
				</p>
				<h3><strong>AVISO DE PRIVACIDAD</strong></h3>
				<p>
					El Municipio de Tequisquiapan, con domicilio en Palmas # 5, Col. Los sabinos, C.P. 76750,
					Tequisquiapan, Querétaro; es responsable de recabar datos personales, del uso que se le dé a
					los mismos y de su protección, con el objeto de que los mismos sean utilizados para la
					realización de un trámite y/o servicio, así como para la mejora de estos. En el entendido que,
					la negativa a proporcionarlos, impedirá que la dependencia o entidad competente pueda
					realizar las gestiones solicitadas por el ciudadano.
					<br>
				</p>
				<h3><strong>INFORMACIÓN QUE SE OBTIENE</strong></h3>
				<p>
					Para las finalidades ya señaladas con anterioridad, el Municipio de Tequisquiapan podrá
					recabar uno o varios de los siguientes Datos Personales que se enlistan a continuación:
					<br>
				<ol type="I">
					<li>Nombre.</li>
					<li>Fecha de Nacimiento</li>
					<li>Domicilio Particular.</li>
					<li>Teléfono fijo o celular.</li>
					<li>Correo Electrónico.</li>
					<li>Registro Federal de Contribuyentes</li>
					<li>Los demás que sean necesarios por la dependencia o entidad para la realización de un
						trámite y/o servicio.</li>
				</ol>
				</p>
				<p>
					<br>
					Es responsabilidad del Titular de la información antes enlistada, garantizar que los datos que
					facilite directamente al Municipio de Tequisquiapan sean veraces y completos, así como de
					notificar a éste último de cualquier modificación a los mismos para dar cumplimiento a la
					obligación de mantener la información actualizada.
					<br>
					Los Titulares de los datos personales o sus representantes legales tendrán derecho a solicitar
					el acceso, rectificación, cancelación u oposición, de sus Datos Personales y/o Datos Personales
					Especialmente Protegidos; mismos que podrán ser modificados en el portal, siempre y cuando
					no sean requisitos para un trámite, además de que se podrá también, a través de solicitud de
					manera personal o por escrito a la Unidad de transparencia y acceso a la información pública
					del Municipio de Tequisquiapan.
					<br>
					El Municipio de Tequisquiapan tomará las medidas de seguridad necesarias para el
					salvaguardo de la información proporcionada en base a lo establecido por la Ley de Acceso a
					la Información Gubernamental del Estado de Querétaro.
					<br>
					En el supuesto de que el Municipio de Tequisquiapan, requiera usar sus Datos Personales y/o
					Datos Personales Especialmente Protegidos con fines distintos a los pactados o convenidos en
					el presente Aviso de Privacidad, se notificará al Titular en forma escrita, telefónica, electrónica,
					o por cualquier medio óptico, sonoro, visual u otro que la tecnología permita ahora o en lo
					futuro explicando los nuevos usos que pretenda darle a dicha información a fin de obtener su
					autorización.
					<br>
					Con el presente Aviso de Privacidad, los Titulares de la información quedan debidamente
					entendidos de los datos que se recabaron de ellos y los fines destinados para ellos, aceptando
					los términos contenidos en el presente Aviso de Privacidad.
					<br>
					El Municipio de Tequisquiapan, se reserva el derecho a modificar el presente Aviso de
					Privacidad para adaptarlo a la legislación aplicable. En dichos supuesto, se anunciará en la
					página de internet <a href="<?= base_url() ?>"><?= base_url() ?></a> cambios de referencia y
					de manera visible en las oficinas del Municipio de Tequisquiapan ubicadas en el domicilio de
					referencia.
					<br>
					El aprovechamiento de los servicios y contenidos del Sistema de Información Geográfica es
					bajo la responsabilidad exclusiva del usuario, quien en todo momento deberá servirse de ellos
					acorde a las funcionalidades permitidas por el propio portal, por lo que el usuario se obliga a
					utilizarlos de modo tal que no atenten contra las normas de uso y convivencia en Internet, las
					leyes de los Estados Unidos Mexicanos, del Estado de Querétaro y los ordenamientos jurídicos
					del Municipio de Tequisquiapan, las buenas costumbres, la dignidad de la persona y los
					derechos de terceros. El sistema es para el uso individual de los usuarios por lo que no podrá
					comercializar de manera alguna los servicios y contenidos establecidos en el portal.
					<br>
					El usuario deberá de ser creado por el/la interesado/a, mismo que se encontrará bajo su
					propio resguardo, el Municipio de Tequisquiapan no se hace responsable del uso de sus
					cuentas personales, ya que solo los usuarios tienen acceso a ellas, así como, su contraseña
					que ambas son de carácter privado. Favor de guardar dichos datos, cuando accedan a la
					plataforma para usarlos debidamente cuando el sistema se los solicite.
				</p>
				<h3><strong>TERMINOS DE USO ADICIONALES</strong></h3>
				<p>
					La información, datos, manifestaciones, actos y hechos jurídicos que se deriven por el uso de
					este Portal, estarán sujetos a la legislación que resulte aplicable, en relación a cada trámite
					que se realice.
					<br>
				</p>
			<?php elseif ($val == 2) : ?>
				<h3><strong>POLÍTICA DE PRIVACIDAD</strong></h3>
				<p>
					La presente Política de Privacidad establece los términos en que el Municipio de Tequisquiapan usa y protege la información que es proporcionada por sus usuarios al momento de utilizar la aplicación web, portales o medios digitales y electrónicos.
					<br>
					El Municipio está comprometido con la seguridad de los datos de sus usuarios.
					<br>
					Cuando se le solicite llenar o validar los campos de información personal con la cual usted pueda ser identificado, lo llevamos a cabo asegurando que sólo se utilizará de acuerdo con los términos de este documento. Sin embargo, esta Política de Privacidad puede cambiar con el tiempo o ser actualizada por lo que le recomendamos revisar constantemente esta página para asegurarse que está de acuerdo con dichos cambios.
					<br>
				</p>
				<h3><strong>Información que es recogida</strong></h3>
				<p>
					Nuestros canales digitales y electrónicos podrán recoger información personal, como son: Nombre completo, información de contacto e información demográfica. De igual forma cuando sea necesario podrá ser requerida información específica para procesar algún pago o servicios que se agreguen.
					<br>
				</p>
				<h3><strong>Uso de la información recogida</strong></h3>
				<p>
					Nuestros canales digitales y electrónicos emplean la información con el fin de proporcionar el mejor servicio de cara al ciudadano, particularmente para mantener un registro de usuarios y mejorar nuestros servicios.
					<br>
					El Municipio está comprometido con el compromiso de mantener su información segura. Usamos sistemas avanzados y los actualizamos constantemente para asegurarnos que no existan accesos no autorizados.
					<br>
				</p>
				<h3><strong>Enlaces a Terceros</strong></h3>
				<p>
					La aplicación pudiera contener enlaces a otros sitios que pudieran ser de su interés. Una vez que usted de clic en estos enlaces y abandone nuestra página, ya no tenemos control sobre al sitio al que es redirigido y por lo tanto no somos responsables de los términos o privacidad ni de la protección de sus datos en esos otros sitios terceros. Dichos sitios están sujetos a sus propias políticas de privacidad por lo cual es recomendable que los consulte para confirmar que usted está de acuerdo con estas.
					<br>
				</p>
				<h3><strong> Fundamento para el tratamiento de datos personales.</strong></h3>
				<p>
					“El Municipio trata los datos personales antes señalados con fundamento en los artículos 89, fracción XVI de la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados, 81, fracción XVI de la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados del Estado de Querétaro, así como con los artículos 111 y 115 de la Ley de Transparencia y Acceso a la Información Pública del Estado de Querétaro
					<br>
				</p>
			<?php endif; ?>
		</div>
		<br><br><br>
		<footer class="pie">
			<div align="center"><img src="<?= base_url() ?>plantilla/images/img-tequis/logos.png" width="215" height="107"></div>
		</footer>
	</div>
</body>

</html>