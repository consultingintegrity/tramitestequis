<script type="text/javascript">
     //   se lee una vez que la pagina este cargada 
     $(document).ready(function() {
                if($('#btnEncuesta').length){
                    $('#btnEncuesta').click(function(){
                        $('#encuesta_modal').modal('show');
                    });
                }else{
                    $('#encuesta_modal').modal('show');
                }

                $('#hacer_encuesta').click(function() {
                    $('#encuesta_modal').modal('show');
                });

                

                $('#form_encuesta').submit(function(btn) {
                    btn.preventDefault();
                    var url = $(this).attr('action');
                    $.ajax({
                        type: 'POST',
                        url: url,
                        data: $(this).serialize(),
                        dataType: 'json',
                        beforeSend: function() {
                            $("#btn_guardar").attr('disabled', '');
                        },
                        error: function() {
                            Swal.fire({
                                icon: 'error',
                                title: '¡Error!',
                                text: 'Error al guardar encuesta',
                                showCancelButton: false,
                                confirmButtonText: 'Aceptar'
                            });
                        },
                        success: function(correct) {
                            if (correct.msg == 'fail') {
                                Swal.fire({
                                    icon: 'error',
                                    title: '¡Error!',
                                    text: 'Error al cargar, intentalo de nuevo',
                                    showCancelButton: false,
                                    confirmButtonText: 'Aceptar'
                                });
                            }
                            if (correct.msg == 'success') {
                                Swal.fire({
                                    icon: 'success',
                                    title: '¡Éxito!',
                                    text: 'Se guardo correctamente',
                                    allowOutsideClick: false,
                                    showConfirmButton: true,
                                    confirmButtonText: 'Aceptar'
                                }).then((result) => {
                                    if (result.value) {}
                                });
                                $('#encuesta_modal').modal('hide');
                            }
                        },
                        complete: function() {
                            $("#btn_guardar").removeAttr('disabled');
                        }
                    });
                });
                $('#encuesta_modal').on('hidden.bs.modal', function(event) { //limpiar modal de evaluación 
                    $(this).find('input[type="radio"]').prop('checked', false);
                    $(this).find('textarea').val('');
                });
            });
</script>