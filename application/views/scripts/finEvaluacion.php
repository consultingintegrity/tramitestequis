<script type="text/javascript">
  $(document).ready( function () {
    //Globals vars
    var id_solicitud = 0//solicitud en revisión
    var id_fase = 0;//funcionario que revisa
    var no_fase = 0;//funcionario que revisa
    var id_ciudadano =0;//ciudadano que realizó la solicitud
    var id_tramite = 0;//ciudadano que realizó la solicitud

    var cajaEspaniol = {
        "sProcessing":     "Procesando...",
        "sLengthMenu":     "Mostrar _MENU_ registros",
        "sZeroRecords":    "No se encontraron resultados",
        "sEmptyTable":     "Ningún dato disponible en esta tabla",
        "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
        "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
        "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
        "sInfoPostFix":    "",
        "sSearch":         "Buscar:",
        "sUrl":            "",
        "sInfoThousands":  ",",
        "sLoadingRecords": "Cargando...",
        "oPaginate": {
            "sFirst":    "Primero",
            "sLast":     "Último",
            "sNext":     "Siguiente",
            "sPrevious": "Anterior"
        },
        "oAria": {
            "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
            "sSortDescending": ": Activar para ordenar la columna de manera descendente"
        }
    };//cajaEspaniol
    $('#tableEntrega').DataTable({
        "language":cajaEspaniol
    });

    $(".btnEntrega").on('click', function(event) {
      event.preventDefault();
      /* Act on the event */
      obtenerInformacionSolicitud(this.value);
    });
    function obtenerInformacionSolicitud(id_solicitud){
      $.ajax({
        url: '<?=base_url()?>Solicitud/infoSolicitud',
        type: 'POST',
        dataType: 'json',
        data: {id_solicitud: id_solicitud, estatus: 2},
        success:function(json){
          id_solicitud = json.id;
          id_fase = json.id_fase;
          id_funcionario = json.id_funcionario;
          id_ciudadano = json.id_ciudadano;
          id_tramite = json.id_tramite;
          confirmarEntrega(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite);
        },//success
        error:function(){
          //doSomething
        }//error
      });
    }//obtenerInformacionPago

  });//readyDocument
  function siguienteFase(id_solicitud,id_fase,id_funcionario,id_ciudadano,id_tramite){
    $.ajax({
      url: '<?=base_url()?>Solicitud/actualiza',
      type: 'POST',//Tipo de datos a mandar
      dataType:'json',//Tipo de respuesta que esperamos
      data: {
        id_solicitud: id_solicitud,
        id_fase : id_fase,
        id_ciudadano : id_ciudadano,
        id_tramite : id_tramite
      },//data
      success:function(jsonResponse){
        if (jsonResponse.response == 200) {
          successAlert("Revisión Exitosa ",jsonResponse.msg);
           $("#fila" + id_solicitud).remove();
           onSiguienteFase();
        }//if
        else{
          errorAlert("Algo salio Terriblemente Mal ",jsonResponse.response);
        }//else

      }//sucess
    });//ajax
  }//siguiente_fase


</script>
