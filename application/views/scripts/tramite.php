<script type="text/javascript">
  //globals vars to use on javascript
  var id_tramite = <?= $id_tramite; ?>;//tramite actual
  var id_giro = <?=(!empty($giro)) ? $giro->id : 0 ?>;
  var id_tipo_construccion = <?=(!empty($tipo_construccion)) ? $tipo_construccion->id : 0 ?>;//tipo_construcción
  var id_dependencia = <?= $id_dependencia; ?>;//dependencia del tramite
  var formularios = [];//formularios en la vista
  var frm_ctrl = <?= json_encode($formularios); ?>;//fromulario del ctrl
  var requisitosTramite = <?= json_encode($requisitos["requisitos"]);?>;//requisitosTramite
  var pestanias = <?= json_encode($pestanias);?>;//pestañas del trámite
  var form_pestanias = <?= json_encode($form_pestanias);?>;//formularios x pestañas
  var dataSolicitud = [];//array send to ctrl
  var id_subTramite = <?= $subTramiteId;?>;
  var id_tipoSolicitud = <?= $tipoSolicitud;?>;
  var tipoprovicional = <?= $provicional;?>;
  var razonSocial = <?= $razonSocial;?>;


  jQuery(document).ready(function($) {

	   processTabs();
    //Agregamos funcionalidad al botón al momento de finalizar el tramite
    $("#btnFinalizar").click(function(event) {
      /* Que comience la fiesta! :v */
      formularios = [];
      console.log(frm_ctrl);
      formularios = getFormView();//Obtenemos los formularios de las vistas
      //Recorremos los formularios para validar sus elementos
      dataSolicitud = [];
      for (var i = 0; i < formularios.length; i++) {
        //validamos los campos requeridos de los form
        //Mesa, mesa, mesa que mas aplauda
        //validamos que exista un formulario enctype
        if (formularios[i].title == "Requisitos de la Solicitud") {
          var formData = new FormData($("#"+formularios[i].title)[0]);
          //Recorremos los requisito para mandarlos al otro lado del muno
          //Mesa que mas aplauda le manda le manda le manda a la niña
          for (var j = 0; j < requisitosTramite.length; j++) {
            formData.append('txtRequisito'+requisitosTramite[j].id_requisito, $('input[type=file]')[j].files[0]);
          }//for
        }//if
        else {
          dataSolicitud[i] = {
            "id_form":frm_ctrl["formularios"][i].id,
            "name_form":frm_ctrl["formularios"][i].name,
            "estatus":false
          };//dataSolicitud
          field = 0;
          formField = [];
          $.each($('input,select', '#'+formularios[i].name_form),function(field){
            formField[field] = {
              "field":$(this).attr('id'),
              "value":$(this).val()
            };//formField
            field++;
          });//each
          dataSolicitud[i]["content"] = formField;
        }//else
      }//for
      //valiamos los campos required de los form
      if (solicitudTerminada(formularios)) {
        dataSolicitud.push(form_pestanias);
        dataSolicitud.push(formularios);
        dataSolicitud.push(pestanias);
        var dataSolicitud = JSON.stringify(dataSolicitud);
        //formlarios de la solicitud
        formStringify = JSON.stringify(frm_ctrl);
        //files de la solicitud
        requisitosTramiteStringify = JSON.stringify(requisitosTramite);//requisitosTramite
        form_pestanias = JSON.stringify(form_pestanias);//requisitosTramite
        pestanias = JSON.stringify(pestanias);
        $.ajax({
          url: '<?=base_url()?>Solicitud/insert',
          type: 'POST',//Tipo de datos a mandar
          dataType:'json',//Tipo de respuesta que esperamos
          data: {
            'infoSolicitud':"text",
            'datasolicitud':dataSolicitud,
            'formularios':formStringify,
            'id_tramite':id_tramite,
            'id_dependencia':id_dependencia,
            "id_giro" : id_giro,
            "id_tipo_construccion" : id_tipo_construccion,
            "id_sub_tramite" : id_subTramite,
            'id_tipo_solicitud' : id_tipoSolicitud,
            'tipo_provicional' : tipoprovicional,
            'razonSocial' : razonSocial
          },//datos a mandar
          beforeSend: function() {
            swal({ title: "Subiendo Información al Servidor...", allowOutsideClick:false });
            swal.showLoading();
          },
          success:function(data){
            if (data.response == 200) {
              //agregamos los parametros
              formData.append('infoSolicitud',"files");
              formData.append('listaRequisitos',requisitosTramiteStringify);
              formData.append('id_solicitud',data.id_solicitud);
              jQuery.ajax({
                    url: "<?=base_url()?>Solicitud/insert",
                    enctype: 'multipart/form-data',
                    data: formData,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    dataType: 'json',
                    success: function(jsonResponse){
                      if (jsonResponse.response == 200) {
                        swal.close();
                        $("#btnFinalizar").attr('disabled','disabled');
                        successAlert("Solicitud Exitosa",jsonResponse.msg);
                        onSiguienteFase();
                         setTimeout(function(){ window.location.replace("<?=base_url()?>Ciudadano"); }, 18000);
                      }//if
                      else{
                        swal.close();
                        errorAlert("Solicitud Registrada",jsonResponse.msg + " " + jsonResponse.error);
                        setTimeout(function(){ window.location.replace("<?=base_url()?>Ciudadano"); }, 18000);
                      }//else
                    }//success
                });
            }//if
            else {
              errorAlert("Algo Salo Terriblemente Mal!", data.msg);
            }//else
          }//success
        });//ajax
        //ajax para subir archivo a server
      }//if
      else {
        errorAlert("Algo Esta Mal", "Algunos de los campos en los formularios son requeridos. Los campos que necesitas llenar están marcados en color rojo.");
      }//else
    });//btn onClick
  });//readyDocument

  function getFormView(){
    var form_names = new Array();//creamos un array para almacenar los elementos
    //buscamos los formularios que se encuentran en la vista
    $("form").each(function() {
      for (var k = 0; k < frm_ctrl["formularios"].length; k++) {
        //validamos los nombres del array ctrl con los de la vista
        if (frm_ctrl["formularios"][k].name == this.name) {
            form_names[k] = {
              "id_form":frm_ctrl["formularios"][k].id,
              "name_form":frm_ctrl["formularios"][k].name,
              "title":frm_ctrl["formularios"][k].title
          };//
        }//if
      }//for
    });//each
    return form_names;
  }//getFormView

  function solicitudTerminada(arr_frm){
    var solicitud = true;
    for (var i = 0; i < arr_frm.length; i++) {
      if (!validaFormulario(arr_frm[i].name_form)) {
        solicitud = false;
      }//if
    }//for
    return solicitud;
  }//solicitudTerminada

  function siguientePestania(input){
    formularios = getFormView();//Obtenemos los formularios de las vistas
    //valiamos los campos required de los form
    for (var i = 0; i < formularios.length; i++) {
      //validamos que exista un formulario enctype
      if (formularios[i].id_form != 11) {
        if (!validaFormulario(formularios[i].name_form)) {
          $("#"+formularios[i].name_form).addClass( "error" );
        }//if
        else{
          $("#"+formularios[i].name_form).removeClass( "error" );
        }//else
      }//if
    }//for
    for (var i = 0; i < formularios.length; i++) {
      if ($("#"+formularios[i].name_form).hasClass( "error" )) {
        errorAlert("Algo Esta Mal", "Es necesario llenar todos los campos remarcados en color rojo");
        $(input).removeAttr('rel');
        break;
      }//if
      else {
          $(input).attr('rel', '2');
        }//else
    }//for
  }//siguientePestania

  function processTabs(){
    $(function() {
     $( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
      $( ".tab-linker" ).click(function() {
           $( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
      return false;
      });
   });

  }//proessTabs
</script>
