<script>
    $("#actionPay").click(function() {
        var id = parseInt($(this).attr('valor'));
        var desc = $(this).attr('desc');
        if (id > 0 && desc != '') {
            $.ajax({
                url: '<?= base_url(); ?>Paybanco/pagoTramite',
                type: 'POST',
                data: {
                    'idSol': id,
                    'desc': desc
                },
                dataType: 'json',
                error: function(e) {
                    alert('fallo');
                },
                success: function(res) {
                    if (res.msg == 'fail') {
                        alert('fallo');
                    }
                    if (res.msg == 'success') {
                        window.open(res.url);
                    }
                }
            });
        }

    });
</script>