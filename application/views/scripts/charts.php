<script type="text/javascript">
var char;
var labels = [], data = [];
  $(document).ready(function() {

        charBar();
        charRadar();
        charPie();

  });

  function charBar(){
    $.ajax({
      url: '<?=base_url()?>Charts/tramitesSolicitados',
      dataType: 'json',
      success:function(json){
        json["data"].forEach(function(item){
          labels.push(item.tramite);
          data.push(item.total);
        });//forEach
        char = document.getElementById("tramitesSolicitdosChar");
        var myChart = new Chart(char,options('horizontalBar',"Mostrar Trámites",labels,data,"Hola"));
     }//success
    })
    .done(function() {
    })
    .fail(function() {
    })
    .always(function() {
    });
  }//charBar

  function charRadar(){
    var labels = [], data = [];

    $.ajax({
      url: '<?=base_url()?>Charts/ingresos',
      dataType: 'json',
      success:function(json){
        json["data"].forEach(function(item){
          labels.push(item.tramite);
          data.push(item.ingreso);
        });//forEach
        char = document.getElementById("ingresosChar");
        var myChart = new Chart(char,options('polarArea',labels,labels,data,"Hola"));
     }//success
    })
    .done(function() {
    })
    .fail(function() {
    })
    .always(function() {
    });
  }//charBar


  function charPie(){
      var labels = [], data = [];
      $.ajax({
        type: "POST",
        url: '<?=base_url()?>Charts/informacionFormularioSolicitud',
        data: {"id_tramite":7, "id_form":36},
        dataType: 'json',
        success:function(json){
          drenajeMunicipal = 0;
          fosaSeptica = 0;
          domiciliario = 0;
          entubado = 0;
          estacionario = 0;
          capacidad = 0;
          otroGas = 0;
          concretoBanqueta = 0;
          empedradoBanqueta = 0;
          adoquinBanqueta = 0;
          adocretoBanqueta = 0;
          concretoPavimento = 0;
          empedradoPavimento = 0;
          adoquinPavimento = 0;
          adocretoPavimento = 0;
          asfaltoPavimento = 0;
          otrosPavimento = 0;
          json.forEach(function(item){
            switch (item.field) {
              case "txtDrenajeMunicipal":
                if (item.value == "TRUE") {
                  drenajeMunicipal++;
                }//if
                break;
              case "txtFosaSeptica":
                if (item.value == "TRUE") {
                  fosaSeptica++;
                }//if
                break;
              case "txtDomiciliario":
                if (item.value == "TRUE") {
                  domiciliario++;
                }//if
                break;
              case "txtEntubado":
                if (item.value == "TRUE") {
                  entubado++;
                }//if
                break;
              case "txtEstacionario":
                if (item.value == "TRUE") {
                  estacionario++;
                }//if
                break;
              case "txtConcretoBanqueta":
                if (item.value == "TRUE") {
                  concretoBanqueta++;
                }//if
                break;
              case "txtEmpedradoBanqueta":
                if (item.value == "TRUE") {
                  empedradoBanqueta++;
                }//if
                break;
              case "txtAdoquinBanqueta":
                if (item.value == "TRUE") {
                  adoquinBanqueta++;
                }//if
                break;
              case "txtAdocretoBanqueta":
                if (item.value == "TRUE") {
                  adocretoBanqueta++;
                }//if
                break;
              case "txtConcretoPavimento":
                if (item.value == "TRUE") {
                  concretoPavimento++;
                }//if
                break;
              case "txtEmpedradoPavimento":
                if (item.value == "TRUE") {
                  empedradoPavimento++;
                }//if
                break;
              case "txtAdoquinPavimento":
                if (item.value == "TRUE") {
                  adoquinPavimento++;
                }//if
                break;
              case "txtAdocretoPavimento":
                if (item.value == "TRUE") {
                  adocretoPavimento++;
                }//if
                break;
              case "txtAsfaltoPavimento":
                if (item.value == "TRUE") {
                  asfaltoPavimento++;
                }//if
                break;
              default:
            }
          });//forEach

          labels.push("Drenaje Municipal");
          data.push(drenajeMunicipal);

          labels.push("Fosa Septica");
          data.push(fosaSeptica);

          labels.push("Gas domiciliario");
          data.push(domiciliario);


          labels.push("Gas entubado");
          data.push(entubado);
          labels.push("Gas Estacionario");
          data.push(estacionario);
          labels.push("Concreto Guarnición/Banqueta");
          data.push(concretoBanqueta);
          labels.push("Empedrado Guarnición/Banqueta");
          data.push(empedradoBanqueta);
          labels.push("Adoquín Guarnición/Banqueta");
          data.push(adoquinBanqueta);
          labels.push("Adocreto Guarnición/Banqueta");
          data.push(adocretoBanqueta);
          labels.push("Concreto Pavimentos y Arroyos");
          data.push(concretoPavimento);
          labels.push("Empedrado Pavimento y Arroyos");
          data.push(empedradoPavimento);
          labels.push("Adoquín Pavimento y Arroyos");
          data.push(adoquinPavimento);
          labels.push("Adocreto Pavimento y Arroyos");
          data.push(adocretoPavimento);
          labels.push("Asfalto Pavimento y Arroyos");
          data.push(asfaltoPavimento);
          char = document.getElementById("estadisticasConstruccionChar");
          var myChart = new Chart(char,options('pie',labels,labels,data,"Hola"));
       }//success
      })
      .done(function() {
      })
      .fail(function() {
      })
      .always(function() {
      });
    }//charBar

  function options(type,title,labels,data,label){
    var options =  {
      type: type,
      data: {
        datasets: [{
          label: title,
          data: data,
          backgroundColor: [
            "rgba(194, 0, 0, 1)",
            "rgba(126, 199, 0, 1)",
            "rgba(146, 0, 199, 1)",
            "rgba(0, 0, 0, 1)",
            "rgba(36, 0, 199, 1)",
            "rgba(83, 78, 85, 1)",
            "rgba(235, 212, 10, 1)",
            "rgba(235, 10, 160, 1)"
          ],
          hoverBackgroundColor: [
            "rgba(194, 0, 0, 0.71)",
            "rgba(126, 199, 0, 0.77)",
            "rgba(146, 0, 199, 0.7)",
            "rgba(0, 0, 0, 0.51)",
            "rgba(36, 0, 199, 0.67)",
            "rgba(83, 78, 85, 0.74)",
            "rgba(235, 212, 10, 0.69)",
            "rgba(235, 10, 160, 0.62)"
          ]

        }],
        labels: labels
      },
      options: {
        legend: {
          position: 'top',
          labels: {
            fontFamily: 'Poppins'
          }

        },
        responsive: true
      }
    };
    return options;

  }//options


</script>
