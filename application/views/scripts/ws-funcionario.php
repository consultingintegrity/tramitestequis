<script type="text/javascript">
  /**
   * Web Socket Cliente para comuniación con el servidor
   */

  var webSocketFuncionario = (function(){

  //Creamos una función global que nos devolverá los eventos del webSocket
  function webSocketFuncionario(url) {
      this.open = false;
      this.socket = new WebSocket("ws://" + url);
      this.setupConnectionEvents();
  }//webSocketFuncionario

  webSocketFuncionario.prototype = {
    setupConnectionEvents : function () {
      var self = this;
      self.socket.onopen = function(evt) { self.connectionOpen(evt); };
      //self.socket.onmessage = function(evt) { self.connectionMessage(evt); };
      self.socket.onclose = function(evt) { self.connectionClose(evt); };
    },
    //función que se ejecuta cuando se abre una conexión con el servidor
    connectionOpen : function(evt){
      this.open = true;
      //this.addSystemMessage("Connected");
    },
    //Función que se ejecuta cuando el servidor responde... En Nuestro caso aún no la vamos a necesitar
    //evt es lo que recibimos del servidor

    /*connectionMessage : function(evt){
        var data = JSON.parse(evt);
        alert("Respondio server Fun");
        //this.addChatMessage(data.msg);
    },*/
    //Función que se ejecuta cuando la comunicación con el servidor se cierra.
    connectionClose : function(evt){
        this.open = false;
        //this.addSystemMessage("Disconnected");
    },
    //Función para mandar datos al servidor en formato JSON
    sendMsg : function(data){
      
      this.socket.send(
        JSON.stringify(
          {
            msg : data
          }
        )
      );
    },
    //Función que se ejecuta en la función connectionMessage
    /*addChatMessage : function(data){
      alert(data.msg);
      console.log(data);
      switch(data.broadType){
        case Broadcast.POST : this.addNewPost(data); break;
        default : console.log("nothing to do");
      }
    },*/
    /*addNewPost : function(data){

      var newPost = data.data;

      newHtml = "<div>"+
              "<span> "+ newPost.postText + "</span>" +
              "</div>";

      $("#messages").prepend(newHtml);
    },*/

    /*addSystemMessage : function(msg){
      // this.chatwindow.innerHTML += "<p>" + msg + "</p>";
    }*/
    };

    return webSocketFuncionario;

})();


</script>
