<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">

    <div class="container clearfix">
        <h1><?= $title ?></h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
          <div class="promo promo-full promo-border header-stick bottommargin-lg">
              <span><?= $descripcion ?></span>
         </div>
          <div class="continer">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th>Contenido</th>
                        <th>Fecha</th>
                    </tr>
                    </thead>
                    <tbody>
                        <?php if(!empty($historialNotificaciones)){
                            foreach ($historialNotificaciones as $not) { ?>
                            <tr>
                                <td><?= $not->contenido; ?></td>
                                <td><?= date("d-m-Y H:i",strtotime($not->fecha)); ?></td>
                            </tr>
                        <?php }?>
                <?php
                }else{?>
                    <td>
                                 <div class="line"></div>
                            <td>No hay notificaciones</td>
                    </td>
                <?php }?>
                    </tbody>
                </table>
            </div>
    <div class="line"></div>
           </div>
        </div>
</section><!-- #content end -->
