        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>

            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                	<div class="promo promo-full promo-border header-stick bottommargin-lg">
                   		<h4><?= $this->session->userdata('rol')[0]->nombre; ?></h4>
                   		<span>Revisa la descripción y requisitos de cada trámite y da clic en el botón de cada uno para iniciar tu trámite en línea.</span>
	               </div>
                 	<div class="container">
                    <div class="row">
                 	    <?php foreach ($tramites as $tramite): ?>
                        <div class="col-md-12">
                          <div class="feature-box fbox-light fbox-effect">
                              <div class="fbox-icon">
                                  <i><img src="<?= base_url().$tramite->logo?>"></i>
                              </div>
                              <h3><?=$tramite->nombre?></h3>
                              <p><?=$tramite->descripcion?></p>
    													<!--concatenamos el id al nombre de la modal para diferenciarlas-->
                              <a data-toggle="modal" name="idtramite" id="idtramite" data-target="#myModal<?=$tramite->id?>" class="button button-mini tright" onclick="subTramite(<?=$tramite->id?>)">Iniciar<i class="icon-circle-arrow-right"></i></a>
                              <div class="line"></div>
                          </div>
                        </div><!-- .col-md-6 -->
                 	    <?php endforeach ?>
                    </div><!-- .row-->
                  </div>
                </div>
                </div>
        </section><!-- #content end -->

<!--///////////////////////////////////////////// Modales ///////////////////////////////////////////////-->

<!-- Modal Numero Oficial-->
<div id="myModal1" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Alineamiento y Número Oficial</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
			  <div class="col-sm-12">
				  <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<!--redirigimos al controlador-->
					<form action="<?php base_url()?>Perfilados/index/1" method="post">
					<center>
					<div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-info btn-lg active" style="color:white">
						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
					</label>
					<label class="btn btn-info btn-lg" style="color:white">
						<input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
					</label>
				</div>
				</center>
				<br>
			  </div><!-- ./col -->
			  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
				</div><!-- ./col -->
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite1" name="tipo_SubTramite1" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->

		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
	  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
	  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->

<!-- Modal dictamen-->
<div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Dictamen Uso de Suelo</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
        <form action="<?php base_url()?>Perfilados/index/2" method="post">
          <div class="col-md-12">
            <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
  					<center>
              <div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
                <label class="btn btn-info btn-lg active" style="color:white">
      						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
      					</label>
      					<label class="btn btn-info btn-lg" style="color:white">
                  <input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
      					</label>
  				    </div>
              <br><br>
            </center>

          </div>
          <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si"  >Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						
            <div class="col-sm-12">
              <h4 class="m-t-0 header-title"><b>Seleccione un uso del establecimiento</b></h4>
              <select class="js-example-responsive" style="width: 100%" multiple="multiple" id="optUsoEstablecimiento" name="optUsoEstablecimiento[]" required>
                <option value="Habitacional" selected="selected">Habitacional</option>
                <option value="Comercial">Comercial</option>
                <option value="Servicios">Servicios</option>
              </select>
            </div><!-- col-sm-12 -->
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">	
                <select id="tipo_SubTramite2" name="tipo_SubTramite2" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->
		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  </form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->

<!-- Modal factibilidad de giro-->
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Factibilidad de Giro</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
          <form action="<?php base_url()?>Perfilados/index/3" method="post">
			  <div class="col-sm-12">
          <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<center>
            <div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
              <label class="btn btn-info btn-lg active" style="color:white">
    						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
    					</label>
    					<label class="btn btn-info btn-lg" style="color:white">
                <input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
    					</label>
    				    </div>
                <br><br>
          </center>
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si"  >Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
  						<h4 class="m-t-0 header-title"><b>¿Es usted el propietario del predio?</b></h4>
  							<div class="form-group">
  							  <label class="radio-inline">
  								<input type="radio" name="propietario" value="si"  >Si
  							  </label>
  							  <label class="radio-inline">
  								<input type="radio" name="propietario" checked value="no" >No
  							  </label>
  							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un Giro</b></h4>
							<div class="form-group">
                <select id="tipoGiro" name="tipoGiro" class="js-example-responsive" style="width: 100%" required>
                  <?php foreach ($giros as $giro ): ?>
                      <option value="<?=$giro->id?>"><?=$giro->descripcion?></option>
                  <?php endforeach; ?>
                </select>
							</div>
					  </div><!-- ./col -->

						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite3" name="tipo_SubTramite3" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div><!--/.col-->
						<!--/Sub Tramite-->

		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
  </form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->

<!-- Modal Licencia de funcionamiento-->
<div id="myModal4" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Licencia de funcionamiento</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
			  <div class="col-sm-12">
				  <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<!--redirigimos al controlador-->
					<form action="<?php base_url()?>Perfilados/index/4" method="post">
					<center>
					<div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-info btn-lg active" style="color:white">
						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
					</label>
					<label class="btn btn-info btn-lg" style="color:white">
						<input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
					</label>
				</div>
				</center>
			  </div><!-- ./col -->
			  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un Giro</b></h4>
							<div class="form-group">
                <select id="tipoGiroLicFuncionamiento" name="tipoGiroLicFuncionamiento" class="js-example-responsive" style="width: 100%" required>
                  <?php foreach ($giros as $giro ): ?>
                      <option name="vtaAlcohol" value="<?=$giro->id?>"><?=$giro->descripcion?></option>
                  <?php endforeach; ?>
                </select>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Cuenta con constancia de inscripción en el registro nacional de turismo?</b></h4>
							<div class="form-group">
							<label class="radio-inline">
								<input type="radio" name="turismo" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="turismo" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Cuenta con constancia de impacto ambiental?</b></h4>
							<div class="form-group">
							<label class="radio-inline">
								<input type="radio" name="ambiental" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="ambiental" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Cuenta con Licencia de Alcoholes Estatal?</b></h4>
							<div class="form-group">
							<label class="radio-inline">
								<input type="radio" name="alcoholesEstatal" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="alcoholesEstatal" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Cuenta con Licencia de Funcionamiento?</b></h4>
							<div class="form-group">
							<label class="radio-inline">
								<input type="radio" name="licFuncionamiento" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="licFuncionamiento" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite4" name="tipo_SubTramite4" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->
		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
	  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
	  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->

<!-- Modal Licencia de alcoholes-->
<div id="myModal5" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Permiso de Alcoholes</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
			  <div class="col-sm-12">
				  <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<!--redirigimos al controlador-->
					<form action="<?php base_url()?>Perfilados/index/5" method="post">
					<center>
					<div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-info btn-lg active" style="color:white">
						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
					</label>
					<label class="btn btn-info btn-lg" style="color:white">
						<input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
					</label>
				</div>
				</center>
				<br>
			  </div><!-- ./col -->
			  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Cuenta con licencia de funcionamiento?</b>&nbsp;<i class="fa fa-question-circle" aria-hidden="true" title="Si planeas realizar una solicitud de permiso provisional no es necesario tener licencia de funcionamiento"></i></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="licenciaF" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="licenciaF" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->

					  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Es usted el propietario del predio?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="propietario" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="propietario" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite5" name="tipo_SubTramite5" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->
					  
		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
	  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
	  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->

<!-- Modal Visto Bueno Proteccion Civil-->
<div id="myModal6" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Visto Bueno de Protección Civil</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
			  <div class="col-sm-12">
				  <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<!--redirigimos al controlador-->
					<form action="<?php base_url()?>Perfilados/index/6" method="post">
					<center>
					<div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-info btn-lg active" style="color:white">
						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
					</label>
					<label class="btn btn-info btn-lg" style="color:white">
						<input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
					</label>
				</div>
				</center>
				<br>
			  </div><!-- ./col -->
			  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Es usted el propietario del predio?</b></h4>
  							<div class="form-group">
  							  <label class="radio-inline">
  								<input type="radio" name="propietario" value="si"  >Si
  							  </label>
  							  <label class="radio-inline">
  								<input type="radio" name="propietario" checked value="no" >No
  							  </label>
  							</div>
								</div>
								<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Cuenta con licencia de funcionamiento anterior?</b></h4>
  							<div class="form-group">
  							  <label class="radio-inline">
  								<input type="radio" name="licenciaAnterior" value="si"  >Si
  							  </label>
  							  <label class="radio-inline">
  								<input type="radio" name="licenciaAnterior" checked value="no" >No
  							  </label>
  							</div>
								</div>	
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite6" name="tipo_SubTramite6" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->
		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
	  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
	  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->

<!-- Modal Visto Bueno Proteccion Civil-->
<div id="myModal8" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Informe de Uso de Suelo</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
			  <div class="col-sm-12">
				  <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<!--redirigimos al controlador-->
					<form action="<?php base_url()?>Perfilados/index/8" method="post">
					<center>
					<div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-info btn-lg active" style="color:white">
						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
					</label>
					<label class="btn btn-info btn-lg" style="color:white">
						<input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
					</label>
				</div>
				</center>
				<br>
			  </div><!-- ./col -->
			  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>

					  </div><!-- ./col -->
						
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite8" name="tipo_SubTramite8" class="js-example-responsive" style="width: 100%" required>
                      <option value=""></option>
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->
		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
	  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
	  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->





<!-- Modal Licencia de construcción-->
<div id="myModal7" class="modal fade" role="dialog">
  <div class="modal-dialog">
    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Perfilado Licencia de Construción</h4>
      </div>
	  <div class="modal-body">
	  <div class="custom-modal-text" id="container-perfil">
		  <div class="row">
			<form action="<?php base_url()?>Perfilados/index/7" method="post">
			  <div class="col-sm-12">
				  <h4 class="m-t-0 header-title"><b>Usted es:</b></h4>
					<!--redirigimos al controlador-->
					<center>
					<div class="row btn-info btn-group btn-group-toggle" data-toggle="buttons">
					<label class="btn btn-info btn-lg active" style="color:white">
						<input type="radio" name="options" value="fisica" id="option1" autocomplete="off" checked><i class="fa fa-user"> Persona física</i>
					</label>
					<label class="btn btn-info btn-lg" style="color:white">
						<input type="radio" name="options" value="moral" id="option2" autocomplete="off"><i class="fa fa-group"> Persona Moral</i>
					</label>
				</div>
				</center>
				<br>
			  </div><!-- ./col -->
			  <div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>¿Alguna persona de su confianza podrá ingresar, gestionar, recoger y realizar aclaraciones del trámite?</b></h4>
							<div class="form-group">
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" value="si">Si
							  </label>
							  <label class="radio-inline">
								<input type="radio" name="tipoGestor" checked value="no" >No
							  </label>
							</div>
					  </div><!-- ./col -->
						<!-- Sub Tramite-->
						<div class="col-sm-12">
						<h4 class="m-t-0 header-title"><b>Selecciona un tipo de trámite</b></h4>
							<div class="form-group">
                <select id="tipo_SubTramite7" name="tipo_SubTramite7" class="js-example-responsive" style="width: 100%" required onchange="construccion()">
                </select>
							</div>
					  </div>
						<!--/Sub Tramite-->
						<div class="col-sm-12" id="frmpregunta" name="frmpregunta">

            </div><!-- ./col -->
		  </div><!-- /.row -->
	  </div><!-- /.custom-modal -->
	</div><!-- /.modal-body -->
<div class="modal-footer"><!--footter-->
	  <input  style="background-color:#521B6E; color:white" type="submit" class="btn myBtn" name="btnPerfilado" id="btnPerfilado">
	  <button style="background-color:#B21F48; color:white" type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
		</form>
    </div><!--/.footter-->
  </div>
</div>
</div><!--/. modal-->
