        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

          <div class="container clearfix">
            <h1><?= $title ?></h1>
          </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        <section id="content">
          <div class="content-wrap">
            <div class="container clearfix">
              <?php if (isset($pago)) : ?>
                <?php if ($pago['solID'] > 0) : ?>
                  <script>
                    $(document).ready(function() {
                      tipoPago("<?= $fase->id ?>", "<?= date('Y-m-d') ?>", 0, "T");
                      siguienteFase2("<?= $fase->id ?>", "<?= $fase->id_fase ?>", "<?= $fase->id_funcionario ?>", "<?= $fase->id_ciudadano ?>", "<?= $fase->id_tramite ?>", "<?= $pago['msg']; ?>");
                    });
                  </script>
                <?php endif; ?>
              <?php endif; ?>
              <div class="promo promo-full promo-border header-stick bottommargin-lg">
                <span><?= $descripcion ?></span>
              </div>
              <div class="continer">
                <table class="table table-striped">
                  <thead>
                    <tr>
                      <th>Nombre del Trámite</th>
                      <th>Fecha</th>
                      <th>Estatus</th>
                      <th>Costo</th>
                      <th>Acciónes de pago</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php if (!empty($tramite)) {
                      foreach ($tramite as $tramites) { ?>
                        <?php
                        switch ($tramites->estatus) {
                          case 0:
                            $tramites->estatus = "Cancelado";
                            break;
                          case 1:
                            $tramites->estatus = "Terminado";
                            break;
                          case 2:
                            $tramites->estatus = "Recoger Trámite";
                            break;
                          case 4:
                            $tramites->estatus = "Revisión";
                            break;
                          case 5:
                            $tramites->estatus = "Correcciones";
                            break;
                          case 3:
                            $tramites->estatus = "Pagar";
                            break;
                          default:
                            break;
                        }
                        ?>
                        <tr>
                          <td><?= $tramites->nombre; ?> <?= ($tramites->giro != null) ? "($tramites->giro)" : "" ?> <?= ($tramites->tipoConstruccion != null) ? "($tramites->tipoConstruccion)" : "" ?></td>
                          <td><?= $tramites->fecha_inicio; ?></td>
                          <?= $td1 = ''; ?>
                          <?= $td4 = ''; ?>
                          <?= $tdR = ''; ?>
                          <?= $url = ''; ?>

                          <?php switch ($tramites->estatus) {
                            case "Cancelado":
                              $td = 'Cancelado';
                              $tramites->estatus = "Cancelado";
                              break;
                            case "Terminado":
                              $td = 'Terminado';
                              $tdR = (isset($tramites->url)) ? $tramites->url : '';
                              break;
                            case "Recoger Trámite":
                              $td = 'Recoge tu trámite';
                              break;
                            case "Revisión":
                              $td = 'En revisión';
                              break;
                            case "Correcciones":
                              $tramites->estatus = "Correcciones";
                              break;
                            case "Pagar":
                              if ($tramites->costo <= 0) {
                                $td = 'Aún no se genera tu recibo de pago';
                              } else {
                                $td = 'En espera de pago';
                                $td1 = '<a class="btn btn-default" href="' . base_url() . 'Ciudadano/comprobante/' . $tramites->id . '" target="_blank" title="Imprimir Orden de Pago" data-toggle="tooltip"><i class="fa fa-print" aria-hidden="true"></i></a>';
                                $td4 = '<a class="btn btn-default" id="actionPay" desc="' . $tramites->nombre . '" valor="' . $tramites->id . '" title="Pagar En Linea" data-toggle="tooltip"><i class="fa fa-credit-card" aria-hidden="true"></i></a>';
                              }
                              break;
                            default:
                              break;
                          }
                          ?>
                          <td><?= $td; ?></td>
                          <td>$<?= number_format($tramites->costo, 2) ?> MXN</td>
                          <td><?= $td1; ?><?= $td4; ?><?= $tdR; ?></td>

                        </tr>
                      <?php } ?>
                    <?php
                    } else { ?>
                      <td>
                        <div class="line"></div>
                      <td>No hay trámites para mostrar</td>
                      </td>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <div class="line"></div>
            </div>
          </div>
        </section><!-- #content end -->