<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">

    <div class="container clearfix">
        <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
        <div class="container clearfix">
          <div class="continer">
            <?php if ($this->session->userdata('rol')[0]->id == 2): ?>
              <?php if (!empty($tramite)):?>
                <h3>Solicitudes con correcciones.</h3>
                <label><?=$descripcion ?></label>
                <p>Da clic sobre el botón de la solicitud a la cual quieres subir tu archivo de evidencias.</p>
                 </p>
                <table id="tableEntrega" name="tableEntrega" cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                  <thead>
                    <tr>
                      <th>Solicitud</th>
                      <th>Trámite</th>
                      <th>Fecha Inicio</th>
                      <th>Correo Electrónico</th>
                      <th>Teléfono</th>
                      <th>Fecha de Corrección</th>
                      <th>Observaciones</th>
                      <th>Tiempo Transcurrido</th>
                      <th>Opciones</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach($tramite as $t) :?>

                    <tr height="20"  id ="fila<?= $t->id ?>">
                      <td height="20" ><?= $t->id?></td>
                      <td height="20" width="279"><?= $t->nombre?></td>
                      <td height="20" width="279"><?= date("Y-m-d H:i", strtotime($t->fecha_inicio))?></td>
                      <td ><?= $t->correo_electronico?></td>
                      <td height="20" width="279"><?=$t->telefono ?></td>
                      <td height="20" width="279"><?=$t->fechaCorreccion ?></td>
                      <td height="20" width="279"><?=$t->observaciones ?></td>
                      <?php
                        $fecha_hoy= date_create(date("Y-m-d"));
                        $t->fechaCorreccion=date_create($t->fechaCorreccion);
                        $dias_transcurridos=date_diff($t->fechaCorreccion,$fecha_hoy);
                      ?>
                      <td height="20" width="279"><?=$dias_transcurridos->format("%R%a días")?></td>
                      <td>
                        <?php if (empty($t->docCorreccionCiudadano)): ?>
                          <button class="btn btn-primary" type="button" name="btnEvidencias" id ="btnEvidencias" title="Subir evidencias" onclick="modalEvidencias(<?=$t->id ?>)" data-toggle="modal" ><i class="fa fa-arrow-up" aria-hidden="true"></i></button>
                          <?php else: ?>
                            <span>Documento subido correctamente</span>
                        <?php endif; ?>
                      </td>

                    </tr>
                    <?php endforeach; ?>
                  </tbody>
                </table>
              <?php else: ?>
                <h3>No tienes solicitudes con correcciones.</h3>
                <label>Los Trámites pendientes se listan en una tabla</label>
              <?php endif; ?>
      <?php endif; ?>
         </div>
  <div class="line"></div>
           </div>
        </div>
</section><!-- #content end -->



<!-- Modal -->
<div class="modal fade" id="modalEvidencias" tabindex="-1" role="dialog" aria-labelledby="modalEvidencias" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Adjuntar Evidencias</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="frmEvidenciasCiudadano" name="frmEvidenciasCiudadano" method="POST" enctype="multipart/form-data">
          <div class="form-group">
            <label for="exampleFormControlFile1">Sube tu documento en formato .pdf</label>
            <input type="file" class="form-control-file" id="txtInputFile" accept="application/pdf">
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">CANCELAR</button>
        <button type="button" class="btn btn-primary" id="btnSubir">SUBIR</button>
      </div>
    </div>
  </div>
</div>


<style>
td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}
</style>

<script type="text/javascript">
  var id_solicitud = 0;

  $("#btnSubir").on('click', function(event) {
    event.preventDefault();
    subirArchivo();
  });

  function modalEvidencias(idSolicitud){
    $("#modalEvidencias").modal("show");
    id_solicitud = idSolicitud;
  }//idSolicitud

  function subirArchivo(){
    if ($("#txtInputFile").val() != "") {
      var formData = new FormData($("#frmEvidenciasCiudadano")[0]);//creaos un formData pra subir archivos
      //AGREGAMOS EL ARCHIVO
      formData.append('importData', $('input[type=file]')[0].files[0]);
      //PARAMETROS TIPO post
      formData.append('id_solicitud', id_solicitud);
      $.ajax({
        url: '<?=base_url()?>Solicitud/evidenciaInspeccion',
        enctype: 'multipart/form-data',
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        dataType: 'json',
        beforeSend: function() {
          swal({ title: "Actualizando Solicitud", allowOutsideClick:false });
          swal.showLoading();
        },
        success:function(jsonResponse){
          if (jsonResponse.estatus == 200) {
            swal.close();
            $("#btnSubir").attr('disabled','disabled');
            swal({ title: "Se subio el documento.", allowOutsideClick:false });
            setTimeout(function(){ window.location.replace("<?=base_url()?>Ciudadano/correcciones"); },3000);
          }//if
          else{
            swal.close();
            $("#btnFinalizar").attr('disabled',' ');
            swal({ title: "Algo salio terriblemente mal", allowOutsideClick:false });
            setTimeout(function(){ window.location.replace("<?=base_url()?>Ciudadano/correcciones"); },3000);
          }//else

        }//sucess
      });
    }//if
    else{
      alert("Selecciona un documento en formado .pdf");
      }//else
  }//subirArchivo


</script>
