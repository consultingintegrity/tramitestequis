        <div class="modal fade" id="encuesta_modal" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalScrollableTitle" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalScrollableTitle"><i class="fa fa-plus"></i> Encuesta de
                            satisfacción</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Closed">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div><!-- modal-header -->
                    <form action="<?= base_url(); ?>Encuesta/encuesta" method="POST" id="form_encuesta">
                        <div class="modal-body">
                            <div class="form-row">
                                <label for="titulo">¿Qué tan satisfecho estoy con el servicio de pago en línea del
                                    Predial?</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" value="Totalmente Insatisfecho" name="satisfecho[]" id="opc1" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Totalmente Insatisfecho">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio"value="Insatisfecho" name="satisfecho[]" id="opc2" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Insatisfecho">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" value="Neutro" name="satisfecho[]" id="opc3" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Neutro">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" value="Satisfecho" name="satisfecho[]" id="opc4" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Satisfecho">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" value="Totalmente Satisfecho" name="satisfecho[]" id="opc5" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Totalmente Satisfecho">
                                    </div>
                                </div>

                                <label for="titulo">¿Qué tanto esfuerzo tuve que realizar para pagar el Predial en
                                    Línea?</label>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="Mucho esfuerzo" name="esfuerzo[]" id="opc1" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Mucho esfuerzo">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio"  value="Regular" name="esfuerzo[]" id="opc2" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Regular">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" value="Ni poco ni mucho" name="esfuerzo[]" id="opc3" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Ni poco ni mucho">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="Poco" name="esfuerzo[]" id="opc4">
                                        </div>
                                        <input type="text" class="form-control" disabled value="Poco">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="Nada de esfuerzo" name="esfuerzo[]" id="opc5" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Nada de esfuerzo">
                                    </div>
                                </div>

                                <label for="titulo">¿Qué tan probable es que recomiende a un familiar o amigo el
                                    pago del Predial en Línea?</label>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="Totalmente de acuerdo" name="probable[]" id="opc1" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Totalmente de acuerdo">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="De acuerdo" name="probable[]" id="opc2" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="De acuerdo">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="Neutro" name="probable[]" id="opc3" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Neutro">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="De acuerdo" name="probable[]" id="opc4" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Desacuerdo">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <input type="radio" aria-label="Radio button for following text input" value="Totalmente de acuerdo" name="probable[]" id="opc5" required>
                                        </div>
                                        <input type="text" class="form-control" disabled value="Totalmente en desacuerdo">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label for="textarea_encuesta">Comentarios</label>
                                    <div class="form form-floating">
                                        <textarea class="form-control" placeholder="Escribe tus comentarios aquí" id="textarea_encuesta" name="textarea_encuesta" rows="3"></textarea>
                                    </div>
                                </div>


                                <br>
                                <div class="col-md-12 text-center">
                                    <button type="submit" class="btn btn-success" id="btn_guardar"><i class="fa fa-check"></i> Guardar</button>
                                    <!-- <button type="button" class="btn btn-danger" id="btn_cancelar">Cancelar</button> -->
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>