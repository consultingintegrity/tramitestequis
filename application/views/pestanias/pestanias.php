<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">

    <div class="container clearfix">
          <h1><?= $title ?></h1>
          <?php if (!empty($giro)): ?>
            <br>
            <h1><?= "Giro: " . $giro->descripcion ?></h1>
          <?php elseif (!empty($tipo_licencia_construccion)): ?>
            <br>
            <h1><?= "Tipo de Licencia: " . $tipo_licencia_construccion->nombreSubTramite?></h1>
          <?php endif; ?>
    </div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
    <div class="content-wrap">
    	<div class="container clearfix">
			<div id="processTabs">
				<ul class="process-steps bottommargin clearfix">
					<?php $i = 0; foreach ($pestanias["pestanias"] as $pestania): $i++; ?>
						<li>
							<a href="#tab<?=$pestania->nombre?>" <?=($i == 2) ? "onclick='siguientePestania(this);' style='pointer-events: none;cursor: default;' " : " " ?>  class="i-circled i-bordered i-alt divcenter" title="<?= $pestania->descripcion ?>"><?= $i ?></a>
							<h5><?= $pestania->nombre ?></h5>
						</li>
					<?php endforeach ?>
				</ul>
				<div>
					<?php $total_pestanias = count($pestanias["pestanias"]); $i = 0; foreach ($pestanias["pestanias"] as $tab):$i++; ?>
						<div id="tab<?= $tab->nombre ?>">
							<?php foreach ($form_pestanias["form_pestanias"] as $frm_pestania): ?>
								<?php if ($tab->id_pestania == $frm_pestania->id_pestania ): ?>
									<!--Pintamos los form-->
									<?php foreach ($formularios["formularios"] as $form): ?>
									<!--asociamos los formularios con sus respectivas petañas-->
										<?php if ($form->id == $frm_pestania->id_formulario):?>
											<div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
												<div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
											</div>

											<div id="collapse<?= $form->id?>" class="collapse">

											<form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>"
												<?php foreach ($atributos_formularios as $atributo): ?>
                          <?php if ($atributo->id_formulario == $form->id): ?>
  													<?= $atributo->nombre."=".$atributo->value ?>
                          <?php endif; ?>
												<?php endforeach ?>
											>
												<!--Pintamos los atributos de los form-->
												<?php foreach ($campos_formularios as $campo): ?>
													<?php if ($campo->id_formulario == $frm_pestania->id_formulario):?>
                            <?php if ($frm_pestania->id_formulario == 33 ): ?>
                              <?php if ($campo->atributo == "label"): ?>
    														<div class="form-group">
    															<div class="col-md-5">
    																<label for="<?=$campo->name?>"><?= $campo->etiqueta ?>:</label>
                                    <?php elseif ($campo->atributo == "h1" && $campo->type == "h1"): ?>
          													<div class="col-md-12">
                                      <h2><?= $campo->etiqueta ?>:</h2>
                                    </div>
      						                  <?php elseif ($campo->atributo == "input" && $campo->type != "checkbox"): ?>
      															<input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>"
                                          placeholder="<?= $campo->placeholder ?>" maxlength="<?= $campo->maxlength ?>"  <?= $campo->required ?>
                                          <?=$campo->disabled ?> <?= ($campo->id_campo == 148) ? "onkeyup='maskClaveCtastral(this.id);'": ""; ?> />
                                  </div>
                                  </div>
                            <?php endif; ?>
                          <?php elseif ($frm_pestania->id_formulario == 35 ):?>
                            <?php if ($campo->id_campo == 156): ?>
                              <div class="alert alert-warning" role="alert">
                                <p>Llenar los campos con la información con la que cuenta tu obra.</p>
                                <p>Si tu obra no cuenta con algún concepto, ingresa un 0 en el campo.</p>
                              </div>
                            <?php endif; ?>
                            <?php if ($campo->atributo == "h1" && $campo->type == "h1"): ?>
                              <div class="col-md-5">
                                <h2><?= $campo->etiqueta ?></h2>
                              </div>
                            <?php elseif ($campo->atributo == "label"): ?>
                              <div class="form-group">
                                <div class="col-md-5">
                                  <label for="<?=$campo->name?>"><?= $campo->etiqueta ?>:</label>
                                </div>
                              </div>
                            <?php elseif ($campo->atributo == "input" && $campo->type == "text"): ?>
                              <div class="form-group">
                                <div class="col-md-5">
                                  <input class="form-control metros" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>"
                                      placeholder="<?= $campo->placeholder ?>" maxlength="<?= $campo->maxlength ?>"  minlength="0" <?= $campo->required ?>
                                      <?=$campo->disabled ?> onblur="calculaMetros();" min="0" title="Si no cuentas con el concepto, deja la opcion en 0 metros." onkeyup="onlyNumbers(this)" />
                                </div>
                              </div>
                            <?php endif; ?>
                        <!-- <?php //elseif ($frm_pestania->id_formulario == 34 ): ?>
                          <div class="alert alert-warning" role="alert">
                                <p>Los datos deben de ser iguales.</p>
                              </div>-->
                          <?php elseif ($frm_pestania->id_formulario == 36 ): ?>
                            <?php if ($campo->atributo == "h1" && $campo->type == "h1"): ?>
                              <div class="col-md-12">
                                <h2><?= $campo->etiqueta ?></h2>
                              </div>
                            <?php elseif ($campo->atributo == "label"): ?>
                              <div class="form-group">
                                <div class="col-md-5">
                                  <label for="<?=$campo->name?>"><?= $campo->etiqueta ?>:</label>
                                </div>
                              </div>
                            <?php elseif ($campo->atributo == "checkbox" && $campo->type == "checkbox"): ?>
                              <div class="form-group">
                                <div class="col-md-5">
                                  <input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" onchange="checkbox(this)" value="FALSE" />
                                </div>
                              </div>
                            <?php elseif ($campo->atributo == "input" && $campo->type != "checkbox"): ?>
                              <div class="form-group">
                                <div class="col-md-5">
                                  <input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>"   />
                                </div>
                              </div>
                            <?php endif; ?>
                          <?php elseif ($frm_pestania->id_formulario == 40 ): ?>
                            <?php if ($campo->atributo == "h1" && $campo->type == "h1"): ?>
                              <div class="col-md-12">
                                <h2><?= $campo->etiqueta ?></h2>
                              </div>
                            <?php elseif ($campo->atributo == "label"): ?>
                              <div class="form-group">
                                <div class="col-md-6">
                                  <label for="<?=$campo->name?>"><?= $campo->etiqueta ?>:</label>
                                </div>
                              </div>
                            <?php elseif ($campo->atributo == "input" && $campo->type != "checkbox"): ?>
                              <div class="form-group">
                                <div class="col-md-6">
                                  <input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>"   />
                                </div>
                              </div>
                            <?php endif; ?>

                          <?php elseif ($campo->atributo == "label"): ?>
														<div class="form-group">
															<div class="col-md-3">
																<label for="<?=$campo->name?>"><?= $campo->etiqueta ?>:</label>
                                <?php elseif ($campo->atributo == "h1" && $campo->type == "h1"): ?>
      													<div class="col-md-10">
                                  <h2><?= $campo->etiqueta ?>:</h2>
                                </div>
  						                  <?php elseif ($campo->atributo == "input" && $campo->type != "checkbox"): ?>
                                  <?php if ($campo->id_campo == 56 || $campo->id_campo == 62 || $campo->id_campo == 148): ?>
                                    <input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>" onkeyup="maskClaveCtastral(this.id);"  <?= $campo->required ?>  />
                                  <?php else: ?>
                                    <input class="form-control <?=($campo->id_campo == 64) ? 'metros' : ''; ?>" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>"
                                          placeholder="<?= $campo->placeholder ?>" maxlength="<?= $campo->maxlength ?>"  <?= $campo->required ?>
                                          <?= ($campo->id_campo == 133) ? " value='17' " : ""?> <?= ($campo->id_campo == 92 && $campo->id_formulario == 27) ? "value = ''" : "" ?><?=$campo->disabled ?> <?=($campo->id_campo == 64) ? 'onkeyup="onlyNumbers(this)"' : ""; ?>
                                        <?php if($personaFisica == 1):?>
                                        <?php if($campo->id_formulario == 9 || $campo->id_formulario == 26 || $campo->id_formulario == 57 || $campo->id_formulario == 34): ?>
                                        <?= ($campo->id_campo == 11) ? " value='".$this->session->userdata('primer_nombre')."' disabled='disabled'" : "";?>
                                        <?= ($campo->id_campo == 13) ? " value='".$this->session->userdata('segundo_nombre')."' disabled='disabled'" : ""?>
                                        <?= ($campo->id_campo == 15) ? " value='".$this->session->userdata('primer_apellido')."' disabled='disabled'": ""?>
                                        <?= ($campo->id_campo == 17) ? " value='".$this->session->userdata('segundo_apellido')."' disabled='disabled'": ""?>
                                      <?php endif; ?>
                                        <?php endif; ?>
                                    />
                                  <?php endif; ?>

                              </div>
                            </div>
                          <?php elseif ($campo->atributo == "date" && $campo->type == "date"): ?>
                                <input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>" maxlength="10" <?= $campo->required ?>  />
                              </div>
                            </div>
                            <?php elseif ($campo->atributo == "time" && $campo->type == "time"): ?>
                            <?php if ($campo->name == "txtHorarioApertura"): ?>
                                <input class="form-control" type="text" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>" maxlength="7" onkeypress="return controltag(event)" <?= $campo->required ?>/>
                              </div>
                            </div>
                            <?php elseif($campo->name == "txtHorarioCierre"): ?>
                                <input class="form-control" type="text" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>" maxlength="7" onkeypress="return controltag(event)" <?= $campo->required ?> />
                              </div>
                            </div>
                            <?php endif; ?>
                          <?php elseif ($campo->atributo == "number" && $campo->type == "number"): ?>
                                <input class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>"  minlength="0" <?= $campo->required ?> value="0" onkeyup="onlyNumbers(this)" />
                              </div>
                            </div>
                          <?php elseif ($campo->atributo == "select" && $campo->type == "select"): ?>
                            <select class="form-control" type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>"  maxlength="<?= $campo->maxlength ?>"  <?= $campo->required ?>>
                              <?php if ($campo->name == "optTipoDictamen"): ?>
                                <option value="1" title="Se refiere a comercio y servicio básico en predios con superficie máxima de 500.00 m2">A</option>
                                <option value="2" title="Se refiere a vivienda de hasta 50 unidades, comercio, servicio en predios con superficie de 501.00  a 5,000.00 m2">B</option>
                              <?php endif; ?>
                            </select>
                          	</div>
  												</div>
  													<?php endif ?>
  													<?php endif ?>
												<?php endforeach //campos_form ?>
                          <?php if ($tab->id_pestania == 2): ?>
                            <?php foreach ($requisitos["requisitos"] as $requisito): ?>
                              <div class="form-group">
  															<div class="col-md-6">
  																<p>Adjuntar <?=$requisito->nombre ?> en formato <?= $requisito->formato ?> <i class="fa fa-question-circle fa-lg" aria-hidden="true" title="<?=$requisito->descripcion ?>"></i></p>
                                  <input class="button button-light" type="file"  id="txtRequisito<?=$requisito->id_requisito?>" name="txtRequisito<?=$requisito->id_requisito ?>"  accept=".<?=$requisito->formato ?>" <?= ($requisito->obligatorio == 1) ? "required" : " " ?> />
                              	</div>
  														</div>
                            <?php endforeach; ?>
                          <?php endif; ?>
                          <?php if ($tab->id_pestania == 18): ?>
                            <div class="form-group">
															<div class="col-md-12">
                                <div class="alert alert-warning" role="alert">
                                  <p>LA LICENCIA SOLAMENTE SERÁ AUTORIZADA SI LOS PLANOS QUE LA ACOMPAÑAN CUMPLEN CON LA NORMATIVIDAD
                                  DE LAS DEPENDENCIAS QUE LA CERTIFICAN.</p>
                                  <p>- EN CASO CONTRARIO LA EXPEDICIÓN DE LA LICENCIA SERÁ EN FECHA POSTERIOR A LA ESTABLECIDA, DEBIENDO EL INTERESADO REALIZAR
                                  LOS AJUSTES CORRESPONDIENTES.</p>
                                  <p>-SI EN UN PLAZO DE 60 DÍAS NO SE PAGAN LOS DERECHOS CORRESPONDIENTES A LA LICENCIA, SE DESTRUIRÁ LA DOCUMENTACIÓN.</p>
                                </div>
                            	</div>
														</div>
                        <?php endif; ?>
											</form>
                        <div class="line"></div>
											</div>
										<?php endif ?>
									<?php endforeach  //Formularios ?>

								<?php endif ?>
							<?php endforeach //form_pestanias?>
                <div class="line"></div>
                <?php if ($i == 1): ?>
                		<a href="#" onclick="siguientePestania(this);" class="button button-3d nomargin fright tab-linker" rel="<?= $i+1 ?>">Siguiente</a>
                <?php endif ?>
                <?php if ($i > 1 && $i <= $total_pestanias): ?>
                	<a href="#" class="button button-3d nomargin tab-linker" rel="<?= $i-1 ?>">Atrás</a>
                	<?php if ($total_pestanias == $i): ?>
                  <button type="button" class="button button-3d nomargin fright tab-linker" id="btnFinalizar" name="btnFinalizar">FINALIZAR</a>
                  <?php else: ?>
                  <button onclick="siguientePestania(this);" type="button" class="button button-3d nomargin fright tab-linker" rel="<?= $i+1 ?>">Siguiente</a>
                	<?php endif ?>
                <?php endif ?>

						</div>
					<?php endforeach //pestanias ?>

				</div>
			</div>
		</div>
     </div>
</section><!-- #content end -->
<script type="text/javascript">

$(function() {
$('#txtHorarioApertura').timepicker({ 'step': 10 });
$('#txtHorarioCierre').timepicker({'step': 10});
 });
function controltag(e) {
        tecla = (document.all) ? e.keyCode : e.which;
        if (tecla==8) return true; // para la tecla de retroseso
        else if (tecla==0||tecla==9)  return true; //<-- PARA EL TABULADOR-> su keyCode es 9 pero en tecla se esta transformando a 0 asi que porsiacaso los dos
       patron =/[0-9\s]/;// -> solo numeros
        te = String.fromCharCode(tecla);
        return patron.test(te);
    }
</script>
