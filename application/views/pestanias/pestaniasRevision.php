<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">

    <div class="container clearfix">
          <h1><?= $title ?></h1>
        <?php if (!empty($giro)): ?>
          <br>
          <?= "<h1>Giro: " . $giro->descripcion."</h1> "?> <?= ($giro->impacto != "null") ? "<strong>"."(".$giro->impacto.")"."</strong>" : " " ?>
        <?php elseif (!empty($tipo_licencia_construccion)): ?>
          <br>
          <h1><?= "Tipo de Licencia: " . $tipo_licencia_construccion->tipo?></h1>
        <?php endif; ?>
    </div>

</section><!-- #page-title end -->
<!-- Content
============================================= -->

<section id="content">
  <div class="content-wrap">
    <div class="container clearfix">
		    <div id="processTabs">
          <ul class="process-steps bottommargin clearfix">
            <!--pintamos las pestañas del tramite -->
  					<?php $no_pestania = 0; foreach ($pestanias as $pestania): $no_pestania++; ?>
              <!-- Recorremos las pestañas de la solicitud -->
              <?php /*foreach ($petanias_solicitud as $pestania_sol):*/ ?>
              <!-- las valiamos con las pestañas de la solicitud -->
                <?php /*if ($pestania_sol === $pestania->id_pestania ):*/ ?>
                  <li>
                    <a href="#tab<?=$pestania->id_pestania?>" class="i-circled i-bordered i-alt divcenter" title="<?= $pestania->descripcion ?>"><?= $no_pestania ?></a>
                    <h5><?= $pestania->nombre ?></h5>
                  </li>
                <?php /*endif;*/ ?>
              <?php /*endforeach //ln24;*/ ?>
            <?php endforeach //ln20 ?>
				  </ul>
				<div>
          <?php $total_pestanias = count($pestanias); $i = 0; foreach ($pestanias as $tab):$i++; ?>
            <?php if ($tab->id_pestania == 1 || $tab->id_pestania == 2): ?>
              <div id="tab<?= $tab->id_pestania ?>">
                <?php foreach ($form_pestanias as $frm_pestania): ?>
                  <?php if ($tab->id_pestania == $frm_pestania->id_pestania): ?>
                    <!--Pintamos los form-->
                    <?php foreach ($formularios as $form): ?>
                      <!--asociamos los formularios con sus respectivas petañas-->
                        <?php foreach ($form_solicitud as $form_sol_id): ?>
                          <?php if ($form_sol_id == $form->id): ?>
                            <?php if ($form->id == $frm_pestania->id_formulario ):?>
                              <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                              </div>
                              <div id="collapse<?= $form->id?>" class="collapse">
                                <div class="nobottommargin">
                                  <table class='table table-hover'>
                                    <?php for ($x=0; $x < count($info_solicitud); $x++) {
                                      //Obtebnemos el id de los formularios
                                      if (!empty($info_solicitud[$x]["id_form"])) {
                                        for ($j=0; $j < count($info_solicitud[$x]["content"]) ; $j++) {
                                          if ($form->id == $info_solicitud[$x]["id_form"]) { ?>
                                            <?php foreach ($campos_formularios as $campo): ?>
                                              <?php if ($campo->name == $info_solicitud[$x]["content"][$j]["field"] && $campo->id_formulario == $info_solicitud[$x]["id_form"]):?>
                                                <?php if ($info_solicitud[$x]["content"][$j]["field"] == "txtCantidadHabitacional"): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>Habitacional</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtCantidadComercial"): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>Comercial</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtCantidadServicios"): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>Servicios</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtSupTerreno"): ?>
                                                  <tr>
                                                    <th colspan = "1"><h3>CONCEPTO</h3></th>
                                                      <th colspan = "1"><h3>SUPERFICIE M<sup>2</sup></h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtDrenajeMunicipal"): ?>
                                                  <tr>
                                                    <th colspan = "2"><center><h3>TIPO DE DESCARGA</center></h3></th>
                                                  </tr>
                                                  <tr>
                                                    <th colspan = "2"><h3>Aguas Residuales y Pluviales</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtDomiciliario"): ?>
                                                  <tr>
                                                    <th colspan = "2"><center><h3>TIPO DE SUMINISTRO DE GAS</h3></center></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtConcretoBanqueta"): ?>
                                                  <tr>
                                                    <th colspan = "2"><center><h3>TIPO DE GUARNICIÓN Y BANQUETA</h3></center></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtConcretoPavimento"): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>PAVIMENTOS Y ARROYOS</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtSupervisorObra"): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>REVISO</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtDirDesarrollo"): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>AUTORIZACIÓN</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtApellidoPaterno" && $campo->id_formulario == 39 ): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>PROPIETARIO</h3></th>
                                                  </tr>
                                                <?php elseif($info_solicitud[$x]["content"][$j]["field"] == "txtCalle" && $campo->id_formulario == 39 ): ?>
                                                  <tr>
                                                    <th colspan = "2"><h3>UBICACIÓN</h3></th>
                                                  </tr>
                                                <?php endif;?>
                                                <tr>
                                                  <td><?= $campo->etiqueta ?>:</td>
                                                  <?php if ($info_solicitud[$x]["content"][$j]["value"] == "TRUE"): ?>
                                                  <td><i class="fa fa-check" aria-hidden="true"></i></td>
                                                <?php elseif ($info_solicitud[$x]["content"][$j]["value"] == "FALSE"): ?>
                                                  <td><i class="fa fa-times" aria-hidden="true"></i></td>
                                                <?php elseif ($info_solicitud[$x]["content"][$j]["field"] == "txtValorEstimado"): ?>
                                                <td>$<?= $info_solicitud[$x]["content"][$j]["value"] ?></td>
                                                <?php elseif ($info_solicitud[$x]["content"][$j]["field"] == "txtMConstruidos"): ?>
                                                <td><?= number_format($info_solicitud[$x]["content"][$j]["value"],2) ?></td>
                                                <?php elseif ($info_solicitud[$x]["content"][$j]["field"] == "txtAreaTrabajo"): ?>
                                                <td><?= $info_solicitud[$x]["content"][$j]["value"] ?> M<sup>2</sup> </td>
                                                <?php elseif ($info_solicitud[$x]["content"][$j]["field"] == "txtHorarioApertura" || $info_solicitud[$x]["content"][$j]["field"] == "txtHorarioCierre"): ?>
                                                <td><?= date("h:i A", strtotime($info_solicitud[$x]["content"][$j]["value"])) ?></td>
                                                <?php else: ?>
                                                <td><?=($info_solicitud[$x]["content"][$j]["value"] == "0") ? '<i class="fa fa-times" aria-hidden="true" title="El solicitante no cuenta con este concepto"></i>' : $info_solicitud[$x]["content"][$j]["value"] ?></td>
                                                <?php endif; ?>
                                                </tr>
                                              <?php endif //if campo->name == ... ?>
                                            <?php endforeach //campos_form ?>
                                          <?php }//if form->id == info_solicitud
                                        }//for content
                                      }//if !empty
                                    }//for count info solicitud ?>
                                    <?php if ($tab->id_pestania == 2): ?>
                                      <?php foreach ($data_requisitos_solicitud as $requisito): ?>
                                        <tr>
                                          <td><?=$requisito->nombre?></td>
                                          <td><a href="<?= base_url().$requisito->url ?>" target='_blank'><i class='fa fa-search m-r-5 fa-2x' ></i></a></td>
                                        </tr>
                                      <?php endforeach; ?>
                                    <?php endif; ?>
                                  </table>
                                </div>
                              </div>
                          <?php endif ?>
                        <?php endif; ?>
                      <?php endforeach; ?>
                    <?php endforeach  //Formularios ?>
                  <?php endif ?>
                <?php endforeach // $form_pestanias?>
              <?php elseif($tab->id_pestania == 3): ?>
                <div id="tab<?= $tab->id_pestania ?>">
                  <?php foreach ($formularios as $form): ?>
                    <!--asociamos los formularios con sus respectivas petañas-->
                    <?php if ($form->id == 13 ):?>
                      <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                        <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                      </div>
                      <div id="collapse<?= $form->id?>" class="collapse">
                        <div class="row">
                          <div class="col-md-8">
                            <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                              <div class="form-group">
                                  <p><?=(!empty($inspeccion)) ? "Adjunta el nuevo documento de inspección" : "Adjunta el documento de la inspección en formato pdf" ?></p>
                                  <input class="button button-light" type="file" name="txtDocInspeccion" id="txtDocInspeccion"  title="Selecciona el documento de inspección" accept=".pdf" />
                              </div>
                            </form>
                          <?php if (!empty($inspeccion)): ?>
                              <p>A continuación se muestra el documento que subiste el día <?= date("d-m-Y",strtotime($inspeccion->fecha)) ?></p>
                              <embed src="<?= base_url().$inspeccion->url ?>"  width="100" height="100" type="application/pdf"></embed>
                          <?php endif; ?>
                        </div>
                        <div class="line"></div>
                      </div>
                    <?php endif ?>
                  <?php endforeach  //Formularios ?>
              <?php elseif($tab->id_pestania == 4): ?>
                <div id="tab<?= $tab->id_pestania ?>">
                    <?php foreach ($formularios as $form): ?>
                      <!--asociamos los formularios con sus respectivas petañas-->
                      <?php if ($form->id == 14 ):?>
                        <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                          <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                        </div>
                        <div id="collapse<?= $form->id?>" class="collapse">
                              <div class="col-md-8">
                                <p>A continuación se muestra el documento de la inspección que realizó el usuario <?= $inspeccion->primer_nombre ?> <?= $inspeccion->primer_apellido ?></p>
                                <embed src="<?= base_url().$inspeccion->url ?>"  width="100" height="100" type="application/pdf"></embed>
                            </div>
                          <div class="line"></div>
                        </div>
                      <?php endif ?>
                    <?php endforeach  //Formularios ?>
                  <?php elseif($tab->id_pestania == 5): ?>
                      <div id="tab<?= $tab->id_pestania ?>">
                        <?php foreach ($formularios as $form): ?>
                          <!--asociamos los formularios con sus respectivas petañas-->
                          <?php if ($form->id == 15):?>
                            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                              <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                            </div>
                            <div id="collapse<?= $form->id?>" class="collapse">
                    <!--pestaña documento numero oficial-->
                          <label>Generar Documento.</label>
                                <div class"col-XL-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                         <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                                        </div>
                                        <div class="col-xs-6" align="right">
                                            <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                                            <form action="#" method="post">
                                            <div class="col-md-12">
                                                    <center><label class"form-control">Comprobante Número Oficial(*)</label></center>
                                                    <input name="txtcomprobanteNO" id="txtcomprobanteNO" class="form-control" type="text" required/>
                                                </div>
                                                <br><br><br><br>
                                                <div class="col-md-12">
                                                    <center><label class"form-control">asignó Número Oficial(*)</label></center>
                                                    <input name="txtasignoNO" id="txtasignoNO" class="form-control" type="text" required/>
                                                </div>
                                                <br><br><br><br>
                                                <div class="col-md-12">
                                                    <center><label class"form-control" >observaciones</label></center>
                                                    <textarea rows="10" cols="50" id="txtdescripcion" name="txtdescripcion" class="form-control" required></textarea>
                                                </div>
                                                    <div class="line"></div>
                                                <div class="col-md-12">
                                                    <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                        </div>
                          <!--./pestaña documento-->
                          <?php endif ?>
                        <?php endforeach  //Formularios ?>
                      <?php elseif($tab->id_pestania == 6 || $tab->id_pestania == 14 || $tab->id_pestania == 15 || $tab->id_pestania == 29): ?>
                        <div id="tab<?= $tab->id_pestania ?>">
                          <?php foreach ($formularios as $form): ?>
                            <!--asociamos los formularios con sus respectivas petañas-->
                            <?php if ($form->id == 16 || $form->id == 28 || $form->id == 29 || $form->id == 55):?>
                                <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                  <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                                </div>
                                <div id="collapse<?= $form->id?>" class="collapse">
                                  <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                                    <div class="form-group">

                                      <div class="row">
                                        <?php foreach ($licencia as $lic): ?>
                                          <div class="col-md-6">
                                            <p>A continuación se muestra el trámite para proceder a firmarlo</p>
                                            <embed src="<?= base_url().$lic->url ?>"  width="100" height="100" type="application/pdf"></embed>
                                          </div>
                                        <?php endforeach; ?>
                                      </div>
                                    </div>
                                  </form>
                                  <div class="line"></div>
                                </div>
                              <?php endif ?>
                          <?php endforeach  //Formularios ?>
                          <?php elseif($tab->id_pestania == 7): ?>
                              <div id="tab<?= $tab->id_pestania ?>">
                                <?php foreach ($formularios as $form): ?>
                                  <!--asociamos los formularios con sus respectivas petañas-->
                                  <?php if ($form->id == 18):?>
                                    <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                      <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                                    </div>
                                      <div id="collapse<?= $form->id?>" class="collapse">
                                        <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                                          <div class="form-group">
                                              <?php foreach ($licencia as $lic): ?>
                                                <div class="col-md-6">
                                                  <p>A continuación se muestra el documento generado</p>
                                                  <embed src="<?= base_url().$lic->url ?>"  width="100" height="100" type="application/pdf"></embed>
                                                </div>
                                              <?php endforeach; ?>
                                          </div>
                                        </form>
                                        <div class="line"></div>
                                      </div>
                                  <?php endif ?>
                                <?php endforeach  //Formularios ?>
                              <?php elseif($tab->id_pestania == 8): ?>
                                 <div id="tab<?= $tab->id_pestania ?>">
                                   <?php foreach ($formularios as $form): ?>
                                     <!--asociamos los formularios con sus respectivas petañas-->
                                     <?php if ($form->id == 19):?>
                                       <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                         <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                                       </div>
                                       <div id="collapse<?= $form->id?>" class="collapse">
                       <!--pestaña documento-->
                       <label>Generar orden de pago.</label>
                       <div class"col-md-12">
                           <div class="row">
                               <div class="col-md-6">
                               <embed src="" id="iframe1" style="width:570px; height:675px;" type="application/pdf"></embed>
                               </div>
                               <div class="col-md-6" align="right">
                                   <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                                   <form action="#" method="post">
                                       <div class="col-md-12">
                                         <center><label class"form-control">Cantidad a Cobrar(*)</label></center>
                                         <input name="txtcobro" id="txtcobro" class="form-control"  type="text" maxlength="65" required/>
                                       </div>
                                           <div class="line"></div>
                                       <div class="col-md-12">
                                           <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generarPago()">Aceptar</button></center>
                                       </div>
                                   </form>
                               </div>
                               </div>
                               </div>
                 <!--./pestaña documento-->
                 <div class="line"></div>
                                       </div>
                                     <?php endif ?>
                                   <?php endforeach  //Formularios ?>

                          <?php elseif($tab->id_pestania == 9): ?>
                            <div id="tab<?= $tab->id_pestania ?>">
                              <?php foreach ($formularios as $form): ?>
                                <!--asociamos los formularios con sus respectivas petañas-->
                                <?php if ($form->id == 17):?>
                                  <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                    <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                                  </div>
                                  <div id="collapse<?= $form->id?>" class="collapse">
                                    <div class="row">
                                      <?php foreach ($licencia as $lic): ?>
                                        <div class="col-md-6">
                                          <p>A continuación se muestra el trámite para proceder a firmarlo</p>
                                          <embed src="<?= base_url().$lic->url ?>"  width="100" height="100" type="application/pdf"></embed>
                                        </div>
                                      <?php endforeach; ?>
                                    </div>
                                  </div>

                                <?php endif;?>

                            <?php endforeach  //Formularios ?>
                          <?php elseif($tab->id_pestania == 7): ?>
                              <div id="tab<?= $tab->id_pestania ?>">
                                <?php foreach ($formularios as $form): ?>
                                  <!--asociamos los formularios con sus respectivas petañas-->
                                  <?php if ($form->id == 18):?>
                                    <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                      <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                                    </div>
                                    <div id="collapse<?= $form->id?>" class="collapse">
                                      <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                                        <div class="form-group">
                                          <div class="row">
                                            <?php foreach ($licencia as $lic): ?>
                                              <div class="col-md-6">
                                                <p>A continuación se muestra la licencia para proceder a firmarla</p>
                                                <embed src="<?= base_url().$lic->url ?>"  width="100" height="100" type="application/pdf"></embed>
                                              </div>
                                            <?php endforeach; ?>
                                        </div>
                                      </form>
                                      <div class="line"></div>
                                    </div>

                                  <?php endif ?>
                                <?php endforeach  //Formularios ?>
                                <?php elseif($tab->id_pestania == 10): ?>
                      <div id="tab<?= $tab->id_pestania ?>">
                        <?php foreach ($formularios as $form): ?>
                          <!--asociamos los formularios con sus respectivas petañas-->
                          <?php if ($form->id == 23):?>
                            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                              <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                            </div>
                            <div id="collapse<?= $form->id?>" class="collapse">
                    <!-- documento uso de suelo-->
                          <label>Generar Documento.</label>
                                <div class"col-md-12">
                                    <div class="row">
                                        <div class="col-md-6">
                                        <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                                        </div>
                                        <div class="col-md-6" align="right">
                                            <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                                            <form action="#" method="post">
                                            <div class="col-md-12">
                                                    <center><label class"form-control">Nombre de Uso analizado(*)</label></center>
                                                    <input name="txtorganismo" id="txtorganismo" class="form-control" type="text" required/>
                                                </div>
                                                <br><br><br><br>
                                                <div class="col-md-12">
                                                    <center><label class"form-control">Uso de suelo de la zona(*)</label></center>
                                                    <input name="txtusZona" id="txtusZona" class="form-control" type="text" required/>
                                                </div>
                                                <div class="line"></div>
                                                <div class="col-md-12">
                                                    <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                                                </div>
                                            </form>
                                        </div>
                                        </div>
                                        </div>

                                        </div>
                          <!--./pestaña documento-->

                          <!--./documento dictamen uso de suelo-->


                          <!--./documento dictamen uso de suelo-->
                          <?php endif ?>
                        <?php endforeach  //Formularios ?>
                                <?php elseif($tab->id_pestania == 8): ?>
                              <div id="tab<?= $tab->id_pestania ?>">
                                <?php foreach ($formularios as $form): ?>
                                  <!--asociamos los formularios con sus respectivas petañas-->
                                  <?php if ($form->id == 19):?>
                                    <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                                      <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                                    </div>
                                    <div id="collapse<?= $form->id?>" class="collapse">
                    <!--pestaña documento-->
                    <label>Generar orden de pago.</label>
                    <div class"col-md-12">
                        <div class="row">
                            <div class="col-md-6">
                            <embed src="" id="iframe1" style="width:570px; height:675px;" type="application/pdf"></embed>
                            </div>
                            <div class="col-md-6" align="right">
                                <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                                <form action="#" method="post">
                                    <div class="col-md-12">
                                      <center><label class"form-control">Cantidad a Cobrar</label></center>
                                      <input name="txtcobro" id="txtcobro" class="form-control" type="text" maxlength="65" required/>
                                    </div>
                                        <div class="line"></div>
                                    <div class="col-md-12">
                                        <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generarPago()">Aceptar</button></center>
                                    </div>
                                </form>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
              <!--./pestaña documento-->
              <div class="line"></div>
                    </div>
                  <?php endif ?>
                <?php endforeach  //Formularios ?>
              <?php elseif($tab->id_pestania == 11): ?>
            <div id="tab<?= $tab->id_pestania ?>">
              <?php foreach ($formularios as $form): ?>
                <!--asociamos los formularios con sus respectivas petañas-->
                <?php if ($form->id == 24):?>
                <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
              </div>
              <div id="collapse<?= $form->id?>" class="collapse">
      <!-- documento proteccion civil-->
            <label>Generar Documento.</label>
                  <div class"col-md-12">
                      <div class="row">
                          <div class="col-md-6">
                          <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                          </div>
                          <div class="col-md-6" align="right">
                              <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                              <form action="#" method="post">
                                  <div class="col-md-12">
                                      <center><label class"form-control">Nombre del negocio(*)</label></center>
                                      <input name="txtnegocio" id="txtnegocio" class="form-control" type="text" required/>
                                  </div>
                                  <br><br><br><br>
                                  <div class="col-md-12">
                                      <center><label class"form-control">Giro(*)</label></center>
                                      <input name="txtgiro" id="txtgiro" class="form-control" type="text" required/>
                                  </div>
                                      <div class="line"></div>
                                  <div class="col-md-12">
                                      <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                                  </div>
                              </form>
                          </div>
                          </div>
                          </div>
                          </div>
            <!--./pestaña documento-->
            <?php endif ?>
          <?php endforeach  //Formularios ?>
         <?php elseif($tab->id_pestania == 18): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 44):?>
              <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
              </div>
              <div id="collapse<?= $form->id?>" class="collapse">
                <!--documento informe uso de suelo-->
                <label>Generar Documento.</label>
                  <div class"col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                      </div>
                        <div class="col-md-6" align="right">
                          <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                          <form action="#" method="post">
                          <div class="col-md-12">
                              <center><label class"form-control">Predio ubicado en zona:(*)</label></center>
                              <input name="txtzona" id="txtzona" class="form-control" type="text" required/>
                               <center><label class"form-control">Clave catastral:(*)</label></center>
                              <input name="txtclave" id="txtclave" class="form-control" type="text" required/>
                            </div>
                            <br><br><br><br>
                            <div class="line"></div>
                            <div class="col-md-12">
                              <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
                </div>
            <!--./pestaña documento-->
            <?php endif ?>
          <?php endforeach  //Formularios ?>
          <?php elseif($tab->id_pestania == 12): ?>
          <div id="tab<?= $tab->id_pestania ?>">
            <?php foreach ($formularios as $form): ?>
              <!--asociamos los formularios con sus respectivas petañas-->
              <?php if ($form->id == 25):?>
                <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                  <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
                </div>
                <div id="collapse<?= $form->id?>" class="collapse">
                  <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                    <div class="form-group">
                      <div class="row">
                          <div class="col-md-6">
                            <p>A continuación se muestra el orden de pago emitido</p>
                            <embed src="<?= base_url().$reciboPago->url ?>"  width="100" height="100" type="application/pdf"></embed>
                          </div>
                    </div>
                  </form>
                  <div class="line"></div>
                </div>

          <div class="line"></div>
                </div>
              <?php endif ?>
            <?php endforeach  //Formularios ?>
          <?php elseif($tab->id_pestania == 13): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
            <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 30):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
  <!--pestaña documento licencia de funcionamiento-->
        <label>Generar Documento.</label>
              <div class"col-md-12">
                  <div class="row">
                      <div class="col-md-6">
                      <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                      </div>
                      <div class="col-md-6" align="right">
                          <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                          <form action="#" method="post">
                              <div class="col-md-12">
                                  <center><label class"form-control">Tipo de licencia(*)</label></center>
                                  <input name="txtlicencia" id="txtlicencia" class="form-control" type="text" required/>
                              </div>
                              <br><br><br><br>
                                  <div class="line"></div>
                              <div class="col-md-12">
                                  <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                              </div>
                          </form>
                      </div>
                      </div>
                      </div>
        <!--./pestaña documento-->
        <?php endif ?>
      <?php endforeach  //Formularios ?>
      <?php elseif($tab->id_pestania == 16): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 31):?>
              <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
              </div>
              <div id="collapse<?= $form->id?>" class="collapse">
                <!--documento factibilidad de giro-->
                <label>Generar Documento.</label>
                  <div class"col-md-12">
                    <div class="row">
                      <div class="col-md-6">
                        <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                      </div>
                        <div class="col-md-6" align="right">
                          <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                          <form action="#" method="post">
                          <div class="col-md-12">
                              <center><label class"form-control">Razón social o nombre del negocio(*)</label></center>
                              <input name="txtnegocio" id="txtnegocio" class="form-control" type="text" required placeholder="Razón social (Solo para personas morales) nombre del negocio (Solo para personas físicas)"/>
                            </div>
                            <br><br><br><br><br>
                            <div class="col-md-12">
                              <center><label class"form-control">No. Dictamen Uso de Suelo(*)</label></center>
                              <input name="txtdictamen" id="txtdictamen" class="form-control" type="text" required/>
                            </div>
                            <div class="line"></div>
                            <div class="col-md-12">
                              <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                            </div>
                          </form>
                        </div>
                    </div>
                  </div>
                </div>
            <!--./pestaña documento-->
            <?php endif ?>
          <?php endforeach  //Formularios ?>
        <?php elseif($tab->id_pestania == 17): ?>
      <div id="tab<?= $tab->id_pestania ?>">
        <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
          <?php if ($form->id == 41):?>
          <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
          <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
        </div>
        <div id="collapse<?= $form->id?>" class="collapse">
        <?php if($tipo_licencia_construccion->id_tipoConstruccion != 15):?>
      <!--pestaña documento licencia de costruccion-->
      <label>Generar Documento.</label>
            <div class"col-md-12">
                <div class="row">
                    <div class="col-md-6">
                    <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                    </div>
                    <div class="col-md-6" align="right">
                        <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                        <form action="#" method="post">
                            <div class="col-md-12">
                                <center><label class"form-control">Número de licencia:</label></center>
                                <input name="txtnoLicencia" id="txtnoLicencia" class="form-control" type="text"/>
                            </div>
                             <div class="col-md-12">
                                <center><label class"form-control">Número de alineamiento:</label></center>
                                <input name="txtnoAlinemiento" id="txtnoAlinemiento" class="form-control" type="text"/>
                            </div>
                             <div class="col-md-12">
                                <center><label class"form-control">Vigencia:</label></center>
                                <input name="txtVigencia" id="txtVigencia" class="form-control" type="text"/>
                            </div>
                             <div class="col-md-12">
                                <center><label class"form-control">Observaciones:</label></center>
                                <textarea name="txtobservaciones" id="txtobservaciones" class="form-control"></textarea>
                            </div>
                            <br><br><br><br>
                                <div class="line"></div>
                            <div class="col-md-12">
                                <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                            </div>
                        </form>
                    </div>
                    </div>
                    </div>
                    <?php elseif($tipo_licencia_construccion->id_tipoConstruccion == 15): ?>
                    <label>Generar Documento.</label>
                    <div class"col-md-12">
                      <div class="row">
                        <div class="col-md-6">
                          <embed src="" id="iframe" style="width:570px; height:675px;" type="application/pdf"></embed>
                        </div>
                        <div class="col-md-6" align="right">
                        <!--clase de pdfs funcion = generarpdf var 1 = $tramite Var 2 = fase -->
                        <form action="#" method="post">
                            <div class="col-md-12">
                                <input name="txtnoLicencia" id="txtnoLicencia" class="form-control" type="hidden"/>
                            </div>
                             <div class="col-md-12">
                                <input name="txtnoAlinemiento" id="txtnoAlinemiento" class="form-control" type="hidden"/>
                            </div>
                             <div class="col-md-12">
                                <input name="txtVigencia" id="txtVigencia" class="form-control" type="hidden"/>
                            </div>
                               <div class="col-md-12">
                                <input name="txtDelegacion" id="txtDelegacion" class="form-control" type="hidden"/>
                            </div>
                             <div class="col-md-12">
                                <textarea name="txtobservaciones" id="txtobservaciones" class="form-control" style="display:none;"></textarea>
                            </div>
                            <br><br><br><br>
                            <div class="alert-warning"><h5>¡No es necesario llenar ninguna información adicional, solo da clic en aceptar!</h5></div>
                                <div class="line"></div>
                            <div class="col-md-12">
                                <center><button type="button" id="btnaceptar" name="btnaceptar" class="btn btn-primary" onclick="generar()">Aceptar</button></center>
                            </div>
                        </form>
                      </div>
                    </div>
                    <?php endif?>
      <!--./pestaña documento-->
      <?php endif ?>
      <?php endforeach  //Formularios ?>
      <!--Empezamos con lo de alcohol para opiniones tecnicas-->
      <?php elseif($tab->id_pestania == 19): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 45):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <div class="row">
              <div class="col-md-12">
              <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
              <div class="form-group">
                <p>Adjunta tu opinión técnica en formato .pdf</p>
                <input class="button button-light" type="file" name="txtOpinionTec" id="txtOpinionTec"  title="Selecciona el documento de opinión técnica" accept=".pdf" />
              </div>
            </form>
          </div>
              </div>
            <div class="line"></div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
          <!--Aqui podemos revisar las opiniones de las diferentes áreas-->
          <?php elseif($tab->id_pestania == 20): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 46):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <div class="row">
              <div class="col-md-12">
              <center><h4>En la siguiente tabla se muestran las opiniones técnicas de las diferentes áreas.</h4><center>
              <table class="table">
                <thead>
                  <tr>
                    <th>Rol</th>
                    <th>Área</th>
                    <th>Opinión</th>
                  </tr>
                </thead>
                <tbody>
                <?php foreach($opinionesTec as $opiniones):?>
                  <tr>
                    <td><?= $opiniones->nombre; ?></td>
                    <td><?= $opiniones->dependencia; ?></td>
                    <td><a href="<?= base_url().$opiniones->url; ?>" target='_blank'><i class='fa fa-eye'> Ver Documento</i></a></td>
                  </tr>
                  <?php endforeach  //opiniones ?>
                </tbody>
              </table>
              </div>
            </div>
            <div class="line"></div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
          <!--Aqui vamos a cargar el archivo de elaboracion de dictamen-->
          <?php elseif($tab->id_pestania == 21): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 47):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <div class="row">
              <div class="col-md-12">
                <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                  <div class="form-group">
                    <p>Adjunta el dictamen elaborado en formato .pdf</p>
                    <input class="button button-light" type="file" name="txtDocDictamen" id="txtDocDictamen"  title="Selecciona el documento de dictamen" accept=".pdf" />
                  </div>
                </form>
              </div>
            </div>
            <div class="line"></div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
           <!--Aqui vamos a ver el archivo de elaboracion de dictamen-->
           <?php elseif($tab->id_pestania == 22): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 48):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
              <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                      <p>A continuación se muestra el dictamen de comisión</p>
                      <embed src="<?= base_url().$dictamenAlcohol->url ?>"  width="100" height="100" type="application/pdf"></embed>
                    </div>
              </div>
            </form>
            <div class="line"></div>
          </div>
          <div class="line"></div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
          <!--Terminamos con lo el dictamen-->
        <!--Aqui vamos a subir el informe de cabildo-->
        <?php elseif($tab->id_pestania == 23): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 49):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <div class="row">
              <div class="col-md-12">
                <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                  <div class="form-group">
                    <p>Adjunta el informe de cabildo en formato .pdf</p>
                    <input class="button button-light" type="file" name="txtDocDictamen" id="txtDocDictamen"  title="Selecciona el documento de dictamen" accept=".pdf" />
                  </div>
                </form>
              </div>
            </div>
            <div class="line"></div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
            <!--Aqui vamos a mostrar el informe de cabildo-->
        <?php elseif($tab->id_pestania == 24): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 50):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
              <div class="form-group">
                <div class="row">
                <?php foreach ($licencia as $lic): ?>
                    <div class="col-md-6">
                      <p>A continuación se muestra el informe de cabildo</p>
                      <embed  src="<?= base_url().$lic->url ?>"  width="100" height="100" type="application/pdf"></embed>
                    </div>
                    <?php endforeach; ?>
              </div>
            </form>
            <div class="line"></div>
          </div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
          <!--Aqui vamos a solicitar la opinion-->
        <?php elseif($tab->id_pestania == 25): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 51):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
              <div class="form-group">
                <div class="row">
                    <div class="col-md-12">
                      <h4>Selecciona el área de donde requieres opinión técnica <label>(*Solo si es requerida*)</label></h4>
                    </div>
                    <div class="col-md-12">
                    <h5>Indica la fecha limite para entrega de dictámenes</h5>
                    </div>
                  <div class="col-md-4">
                        <input class="form-control mb-2" type="date" id="txtFechaDictamen" name="txtFechaDictamen" placeholder="" max="2070-12-30" min="2019-01-01" required>
                  </div>
                  <div class="col-md-12">
                  <br>
                  <br>
                  <br>
                    <label>Secretaría de Finanzas Públicas Municipales</label>
                    <div class="form-group">
  							      <label class="radio-inline">
  								      <input type="radio" name="cboxSF" id="cboxSF" value="si">Si
  							      </label>
  							      <label class="radio-inline">
  								      <input type="radio" name="cboxSF" id="cboxSF" checked value="no" >No
  							      </label>
                  </div>
                    <br>
                    <br>
                    <br>
                    <label>Protección Civil</label>
                    <div class="form-group">
  							      <label class="radio-inline">
  								      <input type="radio" name="cboxPC" id="cboxPC" value="si">Si
  							      </label>
  							      <label class="radio-inline">
  								      <input type="radio" name="cboxPC" id="cboxPC" checked value="no" >No
  							      </label>
  							    </div>
                    <br>
                    <br>
                    <br>
                    <label>Dirección de Desarrollo Urbano</label>
                    <div class="form-group">
  							      <label class="radio-inline">
  								      <input type="radio" name="cboxDU" id="cboxDU" value="si">Si
  							      </label>
  							      <label class="radio-inline">
  								      <input type="radio" name="cboxDU" id="cboxDU" checked value="no" >No
  							      </label>
  							    </div>
                  </div>
              </div>
            </form>
            <div class="line"></div>
          </div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
        <?php elseif($tab->id_pestania == 26): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 52):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
            <div class="alert alert-warning" role="alert"> <p>Fecha límite para relizar opinión Técnica <b>"<?= date("d-m-Y",strtotime($fechaOpinion->fecha_dictamen))?>"</b></p></div>
              <div class="form-group">
                <div class="row">
                    <div class="col-md-8">
                      <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                        <div class="form-group">
                            <p>Adjunta el documento de la opinión técnica en formato pdf</p>
                            <input class="button button-light" type="file" name="txtDocOpinionTecnica" id="txtDocOpinionTecnica"  title="Selecciona el documento de inspección" accept=".pdf" />
                        </div>
                      </form>
                  </div>
                  <div class="line"></div>
              </div>
            </form>
            <div class="line"></div>
          </div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
                  <!--Aqui vamos a subir el provicional de alcohol-->
        <?php elseif($tab->id_pestania == 27): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 53):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <div class="row">
              <div class="col-md-12">
                <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
                  <div class="form-group">
                    <p>Adjunta el Permiso provicional de venta de alcohol.pdf</p>
                    <input class="button button-light" type="file" name="txtDocDictamen" id="txtDocDictamen"  title="Selecciona el documento de dictamen" accept=".pdf" />
                  </div>
                </form>
              </div>
            </div>
            <div class="line"></div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>
            <!--Aqui vamos a mostrar el permiso provicional de alcohol-->
        <?php elseif($tab->id_pestania == 28): ?>
        <div id="tab<?= $tab->id_pestania ?>">
          <?php foreach ($formularios as $form): ?>
          <!--asociamos los formularios con sus respectivas petañas-->
            <?php if ($form->id == 54):?>
            <div class="toggle toggle-bg" href="#collapse<?= $form->id?>" data-toggle="collapse">
            <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i><?= $form->title ?></div>
          </div>
          <div id="collapse<?= $form->id?>" class="collapse">
            <form class="nobottommargin" name="<?= $form->name ?>" id="<?= $form->name ?>" action="<?= $form->action ?>" method="<?= $form->method ?>" enctype='multipart/form-data'>
              <div class="form-group">
                <div class="row">
                  <?php foreach ($licencia as $lic): ?>
                    <div class="col-md-6">
                    <p>A continuación se muestra el Permiso provicional de venta de Alcohol</p>
                      <embed src="<?= base_url().$lic->url ?>"  width="100" height="100" type="application/pdf"></embed>
                    </div>
                  <?php endforeach; ?>
              </div>
            </form>
            <div class="line"></div>
          </div>
          </div>
            <?php endif ?>
          <?php endforeach  //Formularios ?>

              <?php endif; //id_pestania == 1 || 2 ||3 ?>
                <div class="line"></div>
                  <?php if ($i == 1): ?>
                    <a href="#" class="button button-3d nomargin fright tab-linker" rel="<?= $i+1 ?>">Siguiente</a>
                  <?php endif ?>
                  <?php if ($i > 1 && $i <= $total_pestanias): ?>
                    <a href="#" class="button button-3d nomargin tab-linker" rel="<?= $i-1 ?>">Atrás</a>
                  <?php if ($total_pestanias == $i): ?>
                    <button type="button" class="button button-3d nomargin fright tab-linker" id="btnFinalizar" name="btnFinalizar">FINALIZAR</button>
                    <?php if ($no_fase == 2 || $no_fase == 4 && $solicitud->id_tramite != 5 ): ?>
                       <button type="button" class="btn btn-default button button-3d  nomargin fright" id="btnRechazar" name="btnRechazar" style="background-color: #adadad !important; margin-right: 10px !important;">Rechazar Solicitud</button>
                    <?php elseif($solicitud->id_tramite == 6 && $this->session->userdata("rol")[0]->id == 8): ?>
                      <button type="button" class="btn btn-default button button-3d  nomargin fright" id="btnCorrecciones" name="btnCorrecciones" style="background-color: #adadad !important; margin-right: 10px !important;">REPORTAR CORRECCIONES</button>
                   <?php elseif($this->session->userdata("rol")[0]->id == 8): ?>
                     <button type="button" class="btn btn-default button button-3d  nomargin fright" id="btnCorreccionesCiudadano" name="btnCorreccionesCiudadano" style="background-color: #adadad !important; margin-right: 10px !important;">REPORTAR CORRECCIONES A CIUDADANO</button>
                      <?php elseif($solicitud->id_tramite == 5 && $no_fase == 65): ?>
                      <button type="button" class="btn btn-default button button-3d  nomargin fright" id="btnRechazar" name="btnRechazar" style="background-color: #adadad !important; margin-right: 10px !important;">Rechazar Solicitud</button>
                    <?php endif; ?>
                    <?php else: ?>
                    <button type="button" class="button button-3d nomargin fright tab-linker" rel="<?= $i+1 ?>">Siguiente</button>
                  <?php endif ?>
                <?php endif ?>
                </div> <!--tab -->
					<?php endforeach // $total_pestanias..pestanias ?>
				</div>
			</div>

			<script>
        $(document).ready(function() {

  			  $(function() {
  				$( "#processTabs" ).tabs({ show: { effect: "fade", duration: 400 } });
  				$( ".tab-linker" ).click(function() {
  					$( "#processTabs" ).tabs("option", "active", $(this).attr('rel') - 1);
  					return false;
  				});
  			  });
        });
			</script>



		</div>
     </div>
</section><!-- #content end -->
