        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                 	<div class="continer">
                    <?php if ($this->session->userdata('rol')[0]->nombre != "Caja"): ?>
                      <?php if (!empty($tramites) || !empty($solicitud_opinion_tecnica)): ?>
                        <h3>Trámites pendientes.</h3>
                        <label>Para realizar los trámites ingresa a la pestaña de Trámites</label>
                        <table cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                          <tbody>
                            <tr>
                              <th>Trámite</th>
                              <th>Ciudadano</th>
                              <th>Correo Electrónico</th>
                              <th>Teléfono</th>
                              <th>Fecha Inicio</th>
                              <th>Estatus</th>
                              <th>Opciones</th>
                            </tr>
                            <?php foreach($tramites as $t) :?>
                              <?php
                                switch ( $t->estatus ) {
                                  case 1:
                                    $t->estatus = "Terminado";
                                    break;
                                  case 2:
                                    $t->estatus = "Entregar Trámite";
                                    break;
                                  case 4:
                                    $t->estatus = "Revisión";
                                    break;
                                  case 5:
                                    $t->estatus = "Correcciones";
                                    break;
                                  case 3:
                                    $t->estatus = "Emitir Pago";
                                    break;
                                  case 6:
                                    $t->estatus = "Correcciones Ciudadano";
                                    break;
                                  default:
                                    break;
                                }
                              ?>
                            <tr height="20">
                              <td height="20" width="279"><?= $t->nombre?></td>
                              <td height="20" width="279"><?= $t->persona?></td>
                              <td height="20" width="279"><?= $t->correo_electronico?></td>
                              <td height="20" width="279"><?= $t->telefono?></td>
                              <td height="20" width="279"><?= $t->fecha?></td>
                              <td height="20" width="279"><?= $t->estatus?></td>
                              <?php if ($t->estatus == "Revisión"): ?>
                                <td><button class="btn btn-primary" type="button" name="button" title="Evaluar" onclick="location.href='<?=base_url().'Solicitud/index/4/'.$t->id ?>'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button></td>
                              <?php elseif ($t->estatus == "Emitir Pago"): ?>
                                <td><button class="btn btn-primary" type="button" name="button" title="Emitir Pago" onclick="location.href='<?=base_url()?>Solicitud/index/3'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button></td>
                              <?php elseif ($t->estatus == "Entregar Trámite"): ?>
                                <td><button class="btn btn-primary" type="button" name="button" title="Entregar" onclick="location.href='<?=base_url()?>Solicitud/entrega'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button></td>
                              <?php elseif ($t->estatus == "Correcciones"): ?>
                                <td><button class="btn btn-warning" type="button" name="button" title="Correcciones" onclick="location.href='<?=base_url()?>Funcionario/correcciones'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button></td>
                              <?php elseif ($t->estatus == "Correcciones Ciudadano"): ?>
                                <td><button class="btn btn-warning" type="button" name="button" title="Correcciones Ciudadano" onclick="location.href='<?=base_url()?>Funcionario/correccionesCiudadano'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button></td>
                              <?php endif; ?>
                            </tr>
                            <?php endforeach; ?>

                            <?php foreach($solicitud_opinion_tecnica as $t) :?>
                            <tr height="20">
                              <td height="20" width="279"><?= $t->nombre?></td>
                              <td height="20" width="279"><?= $t->persona?></td>
                              <td height="20" width="279"><?= $t->correo_electronico?></td>
                              <td height="20" width="279"><?= $t->telefono?></td>
                              <td height="20" width="279"><?= $t->fecha?></td>
                              <td height="20" width="279">Opinión Técnica</td>
                                <td><button class="btn btn-primary" type="button" name="button" title="Evaluar" onclick="location.href='<?=base_url().'Solicitud/index/4/'.$t->id.'/'.$t->id_funcionario ?>'"><i class="fa fa-external-link-square" aria-hidden="true"></i></button></td>
                            </tr>
                            <?php endforeach; ?>

                          </tbody>
                        </table>
                      <?php else: ?>
                        <h3>No tienes trámites pendientes.</h3>
                        <label>Los Trámites pendientes se listan en una tabla</label>
                      <?php endif; ?>
                      <?php else: ?>
                        <?php if (!empty($pases_caja)): ?>
                          <h3>Ordenes de pago pendientes.</h3>
                          <label>Busca el trámite que el ciudadano va a pagar. Puedes buscar por contenido de la tabla.</label>
                          <table id="tablePago" name="tablePago" cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                            <thead>
                              <tr>
                                <th>Solicitud</th>
                                <th>Costo</th>
                                <th>Trámite</th>
                                <th>Fecha Inicio</th>
                                <th>Ciudadano</th>
                                <th>Correo Electrónico</th>
                                <th>Teléfono</th>
                                <th>Orden de Pago</th>
                                <th>Fecha de Emisión</th>
                                <th>Fecha de Pago</th>
                                <th>Opciones</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach($pases_caja as $pase) :?>
                              <tr  id = "fila<?=$pase->id ?>">
                                <td><?= $pase->id?></td>
                                <td>$<?=number_format($pase->costo,2)?></td>
                                <td><img src="<?=base_url().$pase->logo ?>" alt="" class="col-sm-5 rounded float-left"> <?= $pase->nombre?></td>
                                <td><?= date("d-m-Y H:i",strtotime($pase->fecha_inicio))?></td>
                                <td><?= $pase->primer_nombre." " . $pase->segundo_nombre . " " . $pase->primer_apellido . " " . $pase->segundo_apellido ?></td>
                                <td><?= $pase->correo_electronico?></td>
                                <td><?= $pase->telefono?></td>
                                <td> <a href="<?= $pase->url?>" target="_blank">ver </td>
                                <td><?= date("d-m-Y",strtotime($pase->fecha_emision))?></td>
                                <td><?= date("d-m-Y",strtotime($pase->fecha_vencimiento))?></td>
                                <td>
                                  <button class="btn btn-success btnPagar" type="button" name="btnPagar" id="btnPagar" value="<?=$pase->id ?>" title="Pagar"><i class="fa fa-money fa-2x" aria-hidden="true"></i></button>
                                  <?php if (date("d-m-Y H:i",strtotime($pase->fecha_vencimiento)) < date("d-m-Y H:i")): ?>
                                  <button class="btn btn-danger btnCancelar" type="button" name="btnCancelar" id="btnCancelar" value="<?=$pase->id ?>" title="Cancelar Solicitud"><i class="fa fa-close fa-2x" aria-hidden="true"></i></button>
                                  <?php endif; ?>
                                </td>
                              </tr>
                              <?php endforeach; ?>
                            </tbody>
                          </table>
                        <?php else: ?>
                          <h3>No tienes ordenes de pago pendientes.</h3>
                          <label>Las ordenes de pago pendientes se listan en una tabla</label>
                    <?php endif; ?>
              <?php endif; ?>
					       </div>
					<div class="line"></div>
                   </div>
                </div>
        </section><!-- #content end -->

        <style>
        td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
        </style>
