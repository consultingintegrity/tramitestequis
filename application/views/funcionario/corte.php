        <!-- Page Title
        ============================================= -->
        <section id="page-title" class="page-title-mini">

            <div class="container clearfix">
                <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
            </div>

        </section><!-- #page-title end -->
        <!-- Content
        ============================================= -->
        
        <section id="content">
            <div class="content-wrap">
                <div class="container clearfix">
                 	<div class="continer">
                    <?php if ($this->session->userdata('rol')[0]->nombre == "Caja"): ?>
                      
                        <?php if (!empty($datoscorte)): ?>
                          <h3>Ordenes de pago pendientes.</h3>
                          <label>Busca el trámite que el ciudadano va a pagar. Puedes buscar por contenido de la tabla.</label>
                          <button class="btn btn-success btnCorte" type="button" name="btnCorte" id="btnCorte"   title="realizarCorte" ><i class="fa fa-scissors fa-2x" aria-hidden="true"></i></button>
                          <table id="tableCorte" name="tableCorte" cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                            <thead>
                              <tr>
                                <th>Solicitud</th>
                                <th>Folio / Referencia Bancaria</th>
                                <th>Fecha Inicio</th>
                                <th>Tipo de Pago</th>
                                <th>Cantidad</th>
                              </tr>
                            </thead>
                            <tbody>
                              <?php foreach($datoscorte as $corte) :?>
                              <tr  id = "fila<?=$corte->id?>">
                                <td><?= $corte->id_solicitud?></td>
                                <td><?= $corte->folio?></td>
                                <td><?= $corte->fecha?></td>
                                <td><?= $corte->tipo_pago?></td>
                                <td><?= $corte->costo?></td>                                
                              </tr>
                              <?php endforeach; ?>
                            </tbody>
                          </table>
                        <?php else: ?>
                          <h3>No se han realizado pagos.</h3>
                          <label>Las ordenes de pago pendientes se listan en una tabla</label>

                    <?php endif; ?>
              <?php endif; ?>
					       </div>
					<div class="line"></div>
                   </div>
                </div>
        </section><!-- #content end -->

        <style>
        td, th {
    border: 1px solid #dddddd;
    text-align: left;
    padding: 8px;
}
        </style>
