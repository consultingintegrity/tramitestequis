<!--muestro los datos del trámite consultado anteriormente-->
<section id="content">

<div class="content-wrap">

    <div class="container clearfix">
       <div class="postcontent nobottommargin clearfix">
<!--pinto el nombre del trámite-->
       <h4><b>Trámite</b>: <?= $tramite->nombre?></h4>

            <div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Descripción del trámite</div>
<!--pinto la descripción del trámite-->
                <div class="togglec"><?= $tramite->descripcion?></div>
            </div>

        <div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>A quién va dirigido</div>
<!--pinto a quien va dirijido el trámite-->
                <div class="togglec"><?= $tramite->dirigido?></div>
            </div>

            <div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Documento que obtienes</div>
<!--pinto el documento del trámite-->
                <div class="togglec"><?= nl2br($tramite->documentoObtenido)?></div>
            </div>

            <div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Vigencia de mi documento</div>
<!--pinto la vigencia del trámite-->
                <div class="togglec"><?= $tramite->vigencia?></div>
            </div>

            <div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Qué requisitos necesito para realizar el trámite</div>
                <div class="togglec">
<!--pinto los requisitos del tramite pertenecientes al segundo arreglo
y valido el estatus en caso de que sea persona moral-->
                <?php foreach($requisitos as $r):?>
                    <?php if($r->status == 0):?>
                        - <?= $r->nombre;?><br>
                    <?php elseif($r->status == 1):?>
                        - <?= $r->nombre;?>&nbsp;
                        <b> (*En caso de ser persona moral*)</b><br>
                        <?php elseif($r->status == 2):?>
                        - <?= $r->nombre;?>&nbsp;
                        <b> (*En caso de ser un tercero o persona de confianza*)</b><br>
                    <?php endif;?>
                <?php endforeach;?>
                <br>
                        <b>Nota: Los requisitos pueden cambiar al momento de realizar el proceso de tramite</b>                
                </div>
           </div>

        <!--div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Fundamento Jurídico</div>
                pinto el fundamento juridico del trámite-->
                    <!--<div class="togglec"><?= $tramite->fundamentoJurudico?></div>
                </div>-->

            <!--<div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Costos</div>
                <div class="togglec">-->
<!--aun no conozco los costos estan en codigo duro-->
                <!--  <table cellspacing="0" cellpadding="0" width="100%" border="0" class="table-responsive table-striped">
                    <tbody>
                      <tr height="20">
                        <td height="20" width="279">Habitacional residencial </td>
                        <td width="97">                502.94 </td>
                      </tr>
                      <tr height="20">
                        <td height="20" width="279">Habitacional popular </td>
                        <td width="97">                151.53 </td>
                      </tr>
                      <tr height="20">
                        <td height="20" width="279">Habitacional campestre </td>
                        <td width="97">             1,005.08 </td>
                      </tr>
                      <tr height="20">
                        <td height="20" width="279">Industrial </td>
                        <td width="97">             2,513.91 </td>
                      </tr>
                      <tr height="20">
                        <td height="20" width="279">Comercial </td>
                        <td width="97">             1,508.83 </td>
                      </tr>
                      <tr height="20">
                        <td height="20" width="279">Mixto </td>
                        <td width="97">             1,508.83 </td>
                      </tr>
                      <tr height="20">
                        <td height="20" width="279">Número oficial de entrada </td>
                        <td width="97">             1,005.08</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
            </div>-->

            <!--<div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Información Adicional y Observaciones</div>
pinto observaciones adicionales del trámite-->
            <!--    <div class="togglec"><//?= //$tramite->observaciones?></div>-->
        <!--</div>-->

            <div class="toggle toggle-bg">
                <div class="togglet"><i class="toggle-closed icon-ok-circle"></i><i class="toggle-open icon-remove-circle"></i>Trámite presencial</div>
  <!--pinto como se hace el traite precencial-->
                <div class="togglec"><?= $tramite->presencial?></div>
            </div>
            </div>
            <!-- Sidebar
        ============================================= -->
        <div class="sidebar nobottommargin col_last clearfix">
                    <h4>Inicia tu trámite aquí</h4>
<!--Redirecciono a login-->
                    <a href="<?= base_url() ?>Auth" class="button button-reveal button-xlarge button-rounded tright"><i class="icon-angle-right"></i><span>Entrar</span></a>
        </div><!-- .sidebar end -->
       </div>
    </div>
</div>
</section><!-- #content end -->
