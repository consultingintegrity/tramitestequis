<style>
	.alert {
	    padding: 20px;
	    background-color: #f44336;
	    color: white;
	}
	.yes {
	    padding: 20px;
	    background-color: #008000;
	    color: white;
	}
	.closebtn {
	    margin-left: 15px;
	    color: white;
	    font-weight: bold;
	    float: right;
	    font-size: 22px;
	    line-height: 20px;
	    cursor: pointer;
	    transition: 0.3s;
	}

	.closebtn:hover {
	    color: black;
	}
</style>

<body class="stretched">
	<?php if (!empty($this->session->flashdata('validadoE'))): ?>
	<div class="alert">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Upps! tu cuenta se encuentra suspendida contacta a las oficinas para una aclaración</strong><?= $this->session->flashdata('validoE') ?>
	</div>

	<?php endif ?>
	<?php if (!empty($this->session->flashdata('errorCC'))): ?>
	<div class="alert">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Upps!</strong><?= $this->session->flashdata('errorCC') ?>
	</div>
	<?php endif ?>
<?php if (!empty($this->session->flashdata('goodCC'))): ?>
	<div class="yes">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Se te ha enviado un correo a tu dirección de correo electrónico con las instrucciones para recuperar tu contraseña</strong><?= $this->session->flashdata('goodCC') ?>
	</div>
	<?php endif ?>
<?php if (!empty($this->session->flashdata('error'))): ?>
	<div class="alert">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Upps!</strong><?= $this->session->flashdata('error') ?>
	</div>

	<?php endif ?>
<?php if (!empty($this->session->flashdata('validado'))): ?>
	<div class="yes">
	  <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
	  <strong>Felicidades tu cuenta ha sido verificada ahora puedes iniciar sesión</strong><?= $this->session->flashdata('valido') ?>
	</div>

	<?php endif ?>

 <div id="wrapper" class="clearfix">

     <section id="content">
         <div class="content-wrap nopadding">
         	<div class="section nopadding nomargin" style="width: 100%; height: 100%; position: absolute; left: 0; top: 0; background: url('<?= base_url() ?>plantilla/images/login.png') center center no-repeat; background-size:cover;">
             </div>
             <div class="section nobg full-screen nopadding nomargin">
               <div class="container vertical-middle divcenter clearfix">
                 	<div class="container clearfix">
						<div class="col_one_third nobottommargin">
							<div class="well well-lg nobottommargin">
								<form name="<?= $formulario[0]->name ?>" id="<?= $formulario[0]->name ?>" action="<?= $formulario[0]->action?>" method="<?= $formulario[0]->method ?>"
									<?php foreach ($atributos_formulario as $atributo): ?>
										<?= $atributo->nombre." " ?>
									<?php endforeach ?>
									>
									<h3 >Entra con tu cuenta</h3>
									<?php foreach ($campos_formulario as $campo): ?>
										<?php if ($campo->atributo == "label"): ?>
											<div class="col_full">
												<label for="login-form-username"><?= $campo->etiqueta ?>:</label>
										<?php elseif ($campo->atributo == "input" && $campo->type != "checkbox"): ?>
												<input type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" placeholder="<?= $campo->placeholder ?>" class="form-control" <?= $campo->required ?> />
											</div>
										<?php elseif ($campo->atributo == "input" && $campo->type == "checkbox"): ?>
												<input type="<?= $campo->type ?>" id="<?= $campo->name ?>" name="<?= $campo->name ?>" value="<?= $campo->placeholder ?>" class="form-check-input" <?= $campo->required ?> />
											</div>
										<?php elseif ($campo->atributo == "button"): ?>
											<div class="col_full nobottommargin">
												<input type="<?= $campo->type ?>" class="button button-3d nomargin" name="<?= $campo->name ?>" id="<?= $campo->name ?>" value="<?= $campo->etiqueta ?>"></input>
											</div>
											<?php elseif ($campo->atributo  == "a href"): ?>
												<div class="col_full nobottommargin">
													<a href=href="#" data-target="#pwdModal" data-toggle="modal" class="fright"><?= $campo->etiqueta ?></a>
												</div>
										<?php endif ?>
									<?php endforeach ?>
								</form>
							</div>
						</div>
						<div class="col_two_third col_last nobottommargin texto-login">
							<h3 class="texto-login">¿Aún no tienes cuenta? Regístrate ahora</h3>
							<p>Tu registro en la plataforma servirá para  realizar cualquier trámite y monitorear el seguimiento del mismo.</p>
							<form class="nobottommargin" name="<?= $formAlCiu[0]->name ?>" id="<?= $formAlCiu[0]->name ?>" action="<?= $formAlCiu[0]->action?>" method="<?= $formAlCiu[0]->method ?>"
									<?php foreach ($atributosAlCiu as $atributoAl): ?>
										<?= $atributoAl->nombre." " ?>
									<?php endforeach ?>
							/>
							<?php foreach ($campos_formAlCiu as $campoAl): ?>
								<?php if ($campoAl->atributo == "label"): ?>
									<div class="col-md-6">
										<div class="form-group">
											<label for="texto-login"class="texto-login"><?= $campoAl->etiqueta ?>:</label>
											<?php elseif ($campoAl->atributo == "input"): ?>
											<input type="<?= $campoAl->type ?>" id="<?= $campoAl->name ?>" name="<?= $campoAl->name ?>" placeholder="<?= $campoAl->placeholder ?>" class="form-control" <?= $campoAl->required ?>/>
										</div>
									</div>
									<?php elseif ($campoAl->atributo == "select"): ?>
										<select name="<?= $campoAl->name ?>" class="form-control">
											<option value="M">Masculino</option>
											<option value="F">Femenino</option>
										</select>
										</div>
									</div>

								<?php elseif ($campoAl->atributo == "button"): ?>
									<div class="col-md-6">
										<br>
										<div class="form-group">

											<input type="<?= $campoAl->type ?>" class="button button-3d button-black nomargin form-control" name="<?= $campoAl->name ?>" id="<?= $campoAl->name ?>" value="<?= $campoAl->etiqueta ?>"></input>
										</div>
									</div>
								<?php endif ?>
							<?php endforeach ?>
							</form>
							<div class="col_two_third col_last nobottommargin texto-login">
								<p>Al hacer clic en "Registrar",  acepta  la <a href="<?=base_url()?>politica/datos" target="_blank"> política de datos y la política de cookies.</a></p>
							</div>
						</div>
					</div>
               	</div>
             </div>
         </div>
         <div id="pwdModal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
          <h1 class="text-center">Recuperar contraseña</h1>
      </div>
      <div class="modal-body">
          <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="text-center">
                          <form name="<?= $for[0]->name ?>" id="<?= $for[0]->name ?>" action="<?= $for[0]->action?>" method="<?= $for[0]->method ?>">
                          <p>si olvidaste tu contraseña puedes recuperarla aquí</p>
                            <div class="panel-body">
                               <?php foreach ($atr as $atri): ?>
										<?= $atri->nombre." " ?>
									<?php endforeach ?>


									<?php foreach ($cam as $camp): ?>
										<?php if ($camp->atributo == "label"): ?>
											<div class="col_full">
												<label for="login-form-username"><?= $camp->etiqueta ?>:</label>
										<?php elseif ($camp->atributo == "input" && $camp->type != "checkbox"): ?>
												<input type="<?= $camp->type ?>" id="<?= $camp->name ?>" name="<?= $camp->name ?>" placeholder="<?= $camp->placeholder ?>" class="form-control" <?= $camp->required ?> />
											</div>
										<?php elseif ($camp->atributo == "input" && $camp->type == "checkbox"): ?>
												<input type="<?= $camp->type ?>" id="<?= $camp->name ?>" name="<?= $camp->name ?>" value="<?= $camp->placeholder ?>" class="form-check-input" <?= $camp->required ?> />
											</div>
										<?php elseif ($camp->atributo == "button"): ?>
											<div class="col_full nobottommargin">
												<input type="<?= $camp->type ?>" class="button button-3d nomargin" name="<?= $camp->name ?>" id="<?= $camp->name ?>" value="<?= $camp->etiqueta ?>"></input>
											</div>
											<?php elseif ($camp->atributo  == "a href"): ?>
												<div class="col_full nobottommargin">
													<a href=href="#" data-target="#pwdModal" data-toggle="modal" class="fright"><?= $camp->etiqueta ?></a>
												</div>
										<?php endif ?>
									<?php endforeach ?>

									</form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
      </div>
      <div class="modal-footer">
          <div class="col-md-12">
          <button class="btn" data-dismiss="modal" aria-hidden="true">Cancelar</button>
		  </div>
      </div>
  </div>
  </div>
</div>
     </section><!-- #content end -->

  <script type="text/javascript">

    <?php if (!empty($this->session->flashdata('good'))): ?>
		Swal.fire('Felicidades te has registrado con éxito, verifica tu correo electrónico para poder acceder a la plataforma')
	<?php endif ?>
  </script>
