<style>
 .alert {
     padding: 20px;
     background-color: #f44336;
     color: white;
 }
 .yes {
     padding: 20px;
     background-color: #008000;
     color: white;
 }
 .closebtn {
     margin-left: 15px;
     color: white;
     font-weight: bold;
     float: right;
     font-size: 22px;
     line-height: 20px;
     cursor: pointer;
     transition: 0.3s;
 }

 .closebtn:hover {
     color: black;
 }
</style>
<?php if (!empty($this->session->flashdata('error'))): ?>
 <div class="alert">
   <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
   <strong><?=$this->session->flashdata('error')?></strong>
 </div>
 <?php endif?>
<?php if (!empty($this->session->flashdata('valid'))): ?>
 <div class="yes">
   <span class="closebtn" onclick="this.parentElement.style.display='none';">&times;</span>
   <strong><?=$this->session->flashdata('valid')?></strong>
 </div>
 <?php endif?>

<!-- Page Title
============================================= -->
<section id="page-title" class="page-title-mini">
  <div class="container clearfix">
    <h1>¡HOLA! <?= $this->session->userdata('primer_nombre'); ?></h1>
  </div>
</section><!-- #page-title end -->
<!-- Content
============================================= -->
<section id="content">
  <div class="content-wrap">
    <div class="container clearfix">
 	    <div class="continer">
        <h3>Carga de información al catalógo del predio.</h3>
          <label>Tienes que respetar el formato del excel para que se puedan cargar los datos a la base de datos.</label>

        <form class="" action="<?=base_url()?>phpspreadsheet/Excel/read" method="post" enctype="multipart/form-data">
          <p>SELECCIONAR ARCHIVO EN EXCEL QUE CONTIENE LOS DATOS DEL PREDIAL:
            <i class="fa fa-question-circle fa-lg" aria-hidden="true" title=""></i></p>
          <input class="button button-light" type="file" name="excelPredial" id="excelPredial" required>
          <input class="btn btn-danger" type="submit" value="CARGAR INFORMACIÓN" name="submit">
        </form>
      </div>
      <div class="line"></div>
    </div>
  </div>
</section><!-- #content end -->

<style>
td, th {
border: 1px solid #dddddd;
text-align: left;
padding: 8px;
}
</style>
