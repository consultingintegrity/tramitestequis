<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags-->
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="Dashboard gestión de Trámites Tx">
    <meta name="author" content="Pepe Droid">
    <meta name="keywords" content="TX Dígital">

    <!-- Title Page-->
    <title><?=$navBar?></title>

    <link rel="shortcut icon" href="<?= base_url()?>plantilla/images/img-tequis/logo.png" />
    <!-- Fontfaces CSS-->
    <link href="<?=base_url()?>plantilla/adminStyles/css/font-face.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/font-awesome-4.7/css/font-awesome.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/font-awesome-5/css/fontawesome-all.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/mdi-font/css/material-design-iconic-font.min.css" rel="stylesheet" media="all">

    <!-- Bootstrap CSS-->
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/bootstrap-4.1/bootstrap.min.css" rel="stylesheet" media="all">

    <!-- DataTables -->
    <link rel="stylesheet" type="text/css" href="<?=base_url()?>plantilla/js/DataTables/datatables.min.css"/>
    <!-- Vendor CSS-->
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/animsition/animsition.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/bootstrap-progressbar/bootstrap-progressbar-3.3.4.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/wow/animate.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/css-hamburgers/hamburgers.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/slick/slick.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/select2/select2.min.css" rel="stylesheet" media="all">
    <link href="<?=base_url()?>plantilla/adminStyles/vendor/perfect-scrollbar/perfect-scrollbar.css" rel="stylesheet" media="all">
    <script src="<?=base_url()?>plantilla/js/sweetalert2.all.min.js"></script>
    <!-- Main CSS-->
    <link href="<?=base_url()?>plantilla/adminStyles/css/theme.css" rel="stylesheet" media="all">

</head>
