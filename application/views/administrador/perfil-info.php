<!-- MAIN CONTENT-->
<div class="main-content">
  <div class="section__content section__content--p30">
    <div class="container-fluid">
      <div class="row">
        <div class="col-lg-12">
          <div class="card">
            <div class="card-header">
                <strong><?=$title ?></strong> Administrador
            </div>
            <div class="card-body card-block">
              <?php if ($title == "Perfil"): ?>

                  <form action="" method="post" name="frmPerfilAdmin" id="frmPerfilAdmin" class="form-horizontal">
                    <div class="card-title">
                        <h3 class="text-center title-2">Información Personal</h3>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label class=" form-control-label">Nombre (s)</label>
                        </div>
                        <div class="col-12 col-md-4">
                          <input type="text" id="txtPrimerNom" name="txtPrimerNom" placeholder="Primer Nombre" class="form-control" value="<?=$this->session->userdata("primer_nombre")?>" required>
                          <small class="form-text text-muted">Primer Nombre</small>
                        </div>
                        <div class="col-12 col-md-4">
                          <input type="text" id="txtSegundoNom" name="txtSegundoNom" placeholder="Segundo Nombre" class="form-control" value="<?=$this->session->userdata("segundo_nombre")?>">
                          <small class="form-text text-muted">Segundo Nombre</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label class=" form-control-label">Apellido (s)</label>
                        </div>
                        <div class="col-12 col-md-4">
                          <input type="text" id="txtPrimerAp" name="txtPrimerAp" placeholder="Primer Apellido" class="form-control" value="<?=$this->session->userdata("primer_apellido")?>" required>
                          <small class="form-text text-muted">Primer Apellido</small>
                        </div>
                        <div class="col-12 col-md-4">
                          <input type="text" id="txtSegundoAp" name="txtSegundoAp" placeholder="Segundo Apellido" class="form-control" value="<?=$this->session->userdata("segundo_apellido")?>">
                          <small class="form-text text-muted">Segundo Apellido</small>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="optGenero" class=" form-control-label">Género</label>
                        </div>
                        <div class="col-12 col-md-9">
                          <select name="optGenero" id="optGenero" class="form-control" required>
                              <option value="0">Seleccione una opción</option>
                              <option value="M" <?=($this->session->userdata("genero") == "M") ? "selected" :""?>>Masculino</option>
                              <option value="F" <?=($this->session->userdata("genero") == "F") ? "selected" :""?>>Femenino</option>
                          </select>
                        </div>
                    </div>
                    <hr>
                    <div class="card-title">
                      <h3 class="text-center title-2">Información de Contacto</h3>
                    </div>
                    <hr>
                    <div class="row form-group">
                        <div class="col col-md-3">
                            <label for="email-input" class=" form-control-label">Correo Electrónico</label>
                        </div>
                        <div class="col-12 col-md-9">
                            <input type="email" id="txtCorreo" name="txtCorreo" placeholder="Correo Electrónico" class="form-control" value="<?=$this->session->userdata("correo_electronico")?>" required>
                            <input type="hidden" id="txtCorreoActual" name="txtCorreoActual" placeholder="" class="form-control" value="<?=$this->session->userdata("correo_electronico")?>" required>
                            <small class="help-block form-text">El correo electrónico será con el cual ingrese a la plataforma</small>
                        </div>
                    </div>
                    <hr>

              <?php elseif($title == "Contraseña"): ?>
                <form action="" method="post" name="frmPwdAdmin" id="frmPwdAdmin" class="form-horizontal">
                    <div class="card-title">
                        <h3 class="text-center title-2">Modificar   Contraseña de Acceso a la Plataforma</h3>
                    </div>
                    <hr>
                    <div class="row form-group">
                      <div class="col col-md-3">
                        <label for="password-input" class=" form-control-label">Contraseña</label>
                      </div>
                      <div class="col-12 col-md-9">
                          <input type="password" id="txtPwd" name="txtPwd" placeholder="Contraseña" class="form-control" required>
                      </div>
                    </div>
                    <div class="row form-group">
                      <div class="col col-md-3">
                        <label for="password-input" class=" form-control-label">Confirmar Contraseña</label>
                      </div>
                      <div class="col-12 col-md-9">
                          <input type="password" id="txtConfirmPwd" name="txtConfirmPwd" placeholder="Confirmar Contraseña" class="form-control" required>
                          <small class="help-block form-text">Confirme la contraseña</small>
                      </div>
                    </div>

              <?php elseif($title == "Imagen de Perfil"): ?>
              <form method="post" name="frmImagenPerfil" id="frmImagenPerfil" class="form-horizontal"  enctype="multipart/form-data">
              <div class="card-title">
                  <h3 class="text-center title-2">Modificar Imagen de Perfil</h3>
              </div>
              <hr>
              <div class="row form-group">
                <div class="col col-md-3">
                  <label for="password-input" class=" form-control-label">Imagen de Perfil</label>
                </div>
                <div class="col-12 col-md-9">
                    <input type="file" id="txtImagenPerfil" name="txtImagenPerfil" placeholder="Contraseña" class="form-control" accept=".jpg" required>
                </div>
              </div>
            <?php endif; ?>
              <div class="card-footer">
                <button type="submit" class="btn btn-primary btn-sm" name="btnModificar" value="Modificar">
                    <i class="fa fa-dot-circle-o"></i> MODIFICAR
                </button>
              </div>
            </form>

            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
