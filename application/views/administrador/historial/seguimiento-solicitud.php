<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h2 class="title-1">Seguimiento de las solicitudes del Mes</h2>
            <br>
          </div>
          <div class="col-md-6 col-lg-6">
            <div class="statistic__item">
              <h2 class="number"><?=number_format($solicitudes_terminadas) ?></h2>
              <span class="desc">Solicitudes Terminadas</span>
              <div class="icon">
                  <i class="zmdi zmdi-eye-off"></i>
              </div>
            </div>
          </div>
          <div class="col-md-6 col-lg-6">
            <div class="statistic__item">
                    <h2 class="number"><?=$solicitudes_canceladas ?></h2>
                    <span class="desc">Solicitudes Canceladas</span>
                    <div class="icon">
                        <i class="zmdi zmdi-close"></i>
                    </div>
                </div>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              <div class="table-responsive table--no-card m-b-40">
                <table class="table table-borderless table-striped table-earning">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Fecha De Inicio</th>
                      <th>Última Actualización</th>
                      <th>Días</th>
                      <th>Trámite</th>
                      <th>Ciudadano</th>
                      <th>Funcionario</th>
                      <th>Dependencia</th>
                      <th>Estatus</th>
                      <th>Contacto</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($solicitudes as $solicitud): ?>
                      <?php if ($solicitud->estatus != 0 || $solicitud->estatus != 1): ?>
                      <tr>
                        <td><?=$solicitud->id ?></td>
                        <td><?=$solicitud->fecha_inicio ?></td>
                        <td><?=$solicitud->fecha_fin ?></td>

                        <?php $ee=date_create($solicitud->fecha_inicio)  ?>
                        <?php $hoy = date_create($solicitud->fecha_fin  );  ?>
                        <?php $dias=date_diff($ee,$hoy)  ?>
                        <td><?= $dias->format('%R%a día(s)') ?></td>
                        <td><?=$solicitud->tramite; ?> <?=($solicitud->giro != null) ? "($solicitud->giro)" : "" ?> <?=($solicitud->tipoConstruccion != null) ? "($solicitud->tipoConstruccion)" :"" ?></td>
                        <td><?=$solicitud->primer_nombreC  .  " " . $solicitud->primer_apellidoC ?></td>
                        <td><?=$solicitud->primer_nombreF  .  " " . $solicitud->primer_apellidoF ?></td>
                        <td><?=$solicitud->dependencia ?></td>

                        <?php switch ($solicitud->estatus) {//validamos el estatus del trámite
                          case '4':
                            $solicitud->estatus = "Revisión";
                            break;
                          case '3':
                            $solicitud->estatus = "Pago";
                            break;
                          case '2':
                            $solicitud->estatus = "Entrega en Ventanilla";
                            break;
                          case '5':
                            $solicitud->estatus = "Correcciones";
                            break;
                          case '1':
                            $solicitud->estatus = "<span class='status--process'>Terminado</span>";
                            break;
                          case '0':
                            $solicitud->estatus = "<span class='status--denied' >Cancelado</span>";
                            break;
                          default:
                            // code...
                            break;
                        } ?>
                        <td><?=$solicitud->estatus ?></td>
                        <td><?=($solicitud->estatus != "<span class='status--process'>Terminado</span>" || $solicitud->estatus != "<span class='status--denied' >Cancelado</span>") ? '<button  data-toggle="tooltip" data-placement="top" title="Enviar correo a funcionario" name="contacto" id="contacto" value="'.$solicitud->correo_electronicoF.'"  class="btn btn-primary contacto"><i class="fa fa-envelope"></i></button>' : "Opción no Disponible" ?></td>
                      </tr>
                      <?php endif; ?>
                    <?php endforeach; ?>
                  </tbody>
                  </table>
              </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- modal medium -->
  <div class="modal fade" id="modalCorreoFuncionario" tabindex="-1" role="dialog" aria-labelledby="mediumModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="mediumModalLabel">Enviar Correo Electrónico</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="card-body card-block">
              <form action="" method="post" class="form-control" id="frmCorreoFuncionario" name="frmCorreoFuncionario">
                  <div class="form-group">
                      <div class="col col-md-12">
                          <label for="txtMensaje" class="form-control-label">Cuerpo del Mensaje</label>
                      </div>
                      <div class="col-12 col-md-12">
                        <textarea class="form-control"  name="txtMensaje" id="txtMensaje" rows="8" cols="20" required></textarea>
                      </div>
                  </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Enviar Correo</button>
    </form>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
        </div>
      </div>
    </div>
  </div>
  <!-- end modal medium -->
