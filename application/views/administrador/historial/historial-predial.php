<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h2 class="title-1">Historial de Predial</h2>
            <br>
            <p>Selecciona en que formato quieres descargar tu reporte<p/>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              <div class="table-responsive table--no-card m-b-40">
                <table class="tablaPredio table-borderless table-striped table-earning">
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>Contribuyente</th>
                      <th>Clave Catastral</th>
                      <th>Referencia</th>
                      <th>Monto</th>
                      <th>Fecha-Emisión</th>
                      <th>Fecha-Vigencia</th>
                    </tr>
                  </thead>
                  <tbody>
                   <?php foreach ($Predial as $prediales):?>
                      <tr>
                        <td><?=$prediales->id ?></td>
                        <td><?=$prediales->nombreContribuyente ?></td>
                        <td><?=$prediales->claveCatrastal ?></td>
                        <td><?=$prediales->lineaCaptura ?></td>
                        <td><?=number_format ($prediales->total,2) ?></td>
                        <td><?= date("d-m-Y", strtotime($prediales->fecha_emision)) ?></td>
                        <td><?= date("d-m-Y", strtotime($prediales->fecha_vencimiento)) ?></td>
                    <?php endforeach; ?>
                  </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  </div>