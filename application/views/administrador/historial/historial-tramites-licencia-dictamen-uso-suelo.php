<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-12 col-lg-12">
            <h2 class="title-1">Historial de Licencias de Dictamen Uso de Suelo</h2>
            <br>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-12">
              <div class="table-responsive table--no-card m-b-40">
                <table class="table table-borderless table-striped table-earning">
                  <thead>
                    <tr>
                      <th>Trámite</th>
                      <th>Sub Trámite</th>
                      <th>Total</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php foreach ($tipoDictamen as $solicitud):?>
                      <tr>
                        <td><?=$solicitud->nombre ?></td>
                        <td><?=$solicitud->nombreSubTramite ?></td>
                        <td><?=$solicitud->TOTAL ?></td>
                    <?php endforeach; ; ?>
                  </tbody>
                  </table>
              </div>
          </div>
      </div>
    </div>
  </div>
