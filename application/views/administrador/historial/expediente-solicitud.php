<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-6 col-lg-6">
            <!-- TOP CAMPAIGN-->
            <div class="top-campaign">
              <h3 class="title-3 m-b-30">Requisitos subidos por el Ciudadano</h3>
              <div class="table-responsive">
                  <table class="table table-top-campaign">
                    <thead>
                      <tr>
                          <th class="text-left">Requisito</th>
                          <th class="text-right">Archivo</th>
                      </tr>
                  </thead>
                    <tbody>
                      <?php foreach ($requisitos_solicitud as $req): ?>
                          <tr>
                            <td><?=$req->nombre ?></td>
                            <td><a class="btn btn-primary" target="_blank" href="<?=base_url().$req->url ?>"><i class="fas fa-search-plus" title="Ir A Ubicación Del Archivo"></i></a></td>
                          </tr>
                      <?php endforeach; ?>
                    </tbody>
                  </table>
              </div>
            </div>
            <!-- END TOP CAMPAIGN-->
          </div>
          <div class="col-md-6 col-lg-6">
            <!-- TOP CAMPAIGN-->
            <div class="top-campaign">
              <h3 class="title-3 m-b-30">Información</h3>
              <div class="table-responsive">
                <table class="table table-top-campaign">
                  <tbody>
                    <tr>
                      <td>Solicitud</td>
                      <td><?=$info_solicitud->id ?></td>
                    </tr>
                    <tr>
                      <td>Trámite</td>
                      <td><?=$info_solicitud->nombre ?>  <?=($info_solicitud->giro != null) ? "($info_solicitud->giro)" : "" ?> <?=($info_solicitud->tipoConstruccion != null) ? "($info_solicitud->tipoConstruccion)" :"" ?></td>
                    </tr>
                    <tr>
                      <td>Ciudadano</td>
                      <td><img src="<?=base_url().$info_solicitud->imagen ?>" alt="Imagen Ciudadano"  class="img img-responsive" width="95px">   <?=$info_solicitud->primer_nombre . " "  . $info_solicitud->primer_apellido ?></td>
                    </tr>
                    <tr>
                      <td>Fecha de Inicio</td>
                      <td><?=$info_solicitud->fecha_inicio ?></td>
                    </tr>
                    <tr>
                      <td>Última Actualización</td>
                      <td><?=$info_solicitud->fecha_fin ?></td>
                    </tr>
                    <tr>
                      <td>Duración</td>
                      <?php $fecha_inicio = date_create($info_solicitud->fecha_inicio)  ?>
                      <?php $dias_diff = date_create($info_solicitud->fecha_fin)  ?>
                      <?php $dias=date_diff($fecha_inicio,$dias_diff)  ?>
                      <td><?=$dias->format('%R%a día(s)')?></td>
                    </tr>
                    <tr>
                      <td>Costo</td>
                      <td>$<?=number_format($info_solicitud->costo,2)?></td>
                    </tr>
                    <tr>
                      <td>Estatus</td>
                      <?php switch ($info_solicitud->estatus) {//validamos el estatus del trámite
                        case '4':
                          $info_solicitud->estatus = "Revisión";
                          break;
                        case '3':
                          $info_solicitud->estatus = "Pago";
                          break;
                        case '2':
                          $info_solicitud->estatus = "Entrega en Ventanilla";
                          break;
                        case '5':
                          $info_solicitud->estatus = "Correcciones";
                          break;
                        case '1':
                          $info_solicitud->estatus = "<span class='status--process'>Terminado</span>";
                          break;
                        case '0':
                          $info_solicitud->estatus = "<span class='status--denied' >Cancelado</span>";
                          break;
                        default:
                          // code...
                          break;
                      } ?>
                      <td><?=$info_solicitud->estatus ?></td>
                    </tr>
                  </tbody>
                </table>
              </div>
            </div>
            <!-- END TOP CAMPAIGN-->
          </div>
          <div class="col-xl-12">
            <h2 class="title-1">Seguimiento de la solicitud</h2>
            <br>
            <!-- TABLE-->
            <div class="table-responsive">
              <table class="table table-borderless table-data3">
                  <thead>
                    <tr>
                      <th>Funcionario</th>
                      <th>Nombre</th>
                      <th>Dependencia</th>
                      <th>Rol</th>
                      <th>Fecha de Revisión</th>
                      <th>Días Trancurridos</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach ($funcionarios_solicitud as $fs): ?>
                      <tr>
                        <td><img src="<?=base_url().$fs->imagen ?>" alt="Imagen Usuario" class="img img-responsive" width="95px"></td>
                        <td><?=$fs->primer_nombre ." " . $fs->primer_apellido ?></td>
                        <td><?=$fs->dependencia ?></td>
                        <td><?=$fs->rol ?></td>
                        <td><?=$fs->fecha_revision_funcionario ?></td>

                        <?php $fecha_inicio = date_create($fs->fecha_inicio)  ?>
                        <?php $dias_diff = date_create($fs->fecha_revision_funcionario)  ?>
                        <?php $dias=date_diff($fecha_inicio,$dias_diff)  ?>
                        <td><?= $dias->format('%R%a día(s)') ?></td>

                      </tr>
                    <?php endforeach; ?>

                    </tbody>
                </table>
            </div>
              <!-- END TABLE-->
          </div>
        </div>
      </div>
    </div>
  </div>
