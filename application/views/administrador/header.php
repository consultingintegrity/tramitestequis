<body class="animsition">
  <div class="page-wrapper">
  <!-- HEADER MOBILE-->
  <header class="header-mobile d-block d-lg-none">
    <div class="header-mobile__bar">
        <div class="container-fluid">
            <div class="header-mobile-inner">
                <a class="logo" href="#">
                  <img src="<?=base_url()?>plantilla/images/img-tequis/logos.png" alt="Tequisquiapan" width="50%" />
                </a>
                <button class="hamburger hamburger--slider" type="button">
                    <span class="hamburger-box">
                        <span class="hamburger-inner"></span>
                    </span>
                </button>
            </div>
        </div>
    </div>
    <nav class="navbar-mobile">
        <div class="container-fluid">
            <ul class="navbar-mobile__list list-unstyled">
            <li>
                <a href="<?=base_url()?>Administrador">
                    <i class="fas fa-tachometer-alt"></i>Inicio</a>
            </li>
            <li class="active has-sub">
                <a class="js-arrow" href="#">
                    <i class="fa fa-users"></i>Funcionarios</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list">
                    <li>
                      <a href="<?=base_url()?>Administrador/index/registro">Registrar</a>
                    </li>
                    <li>
                        <a href="<?=base_url()?>Administrador/index/consultar">Consultar</a>
                    </li>
                </ul>
              </li>
              <li class="<?=($navBar != null && $navBar == 'Historial') ? 'active' : '' ?> has-sub">
                <a class="js-arrow" href="#">
                  <i class="far fa-calendar-minus"></i>Historial</a>
                <ul class="list-unstyled navbar__sub-list js-sub-list">
                  <li>
                    <a href="<?=base_url()?>Administrador/index/historial-solicitudes">Solicitudes</a>
                  </li>
                  <li>
                    <a href="<?=base_url()?>Administrador/index/historial-tramites">Trámites</a>
                  </li>
                    <a href="<?=base_url()?>Administrador/index/historial-tramites-licencia-construccion">Estadisticas de Trámites Licencia de Construcción</a>
                  </li>
                </li>
                  <a href="<?=base_url()?>Administrador/index/historial-tramites-licencia-dictamen-uso-suelo">Estadisticas de Trámites Dictamen Uso de Suelo</a>
                </li>
                </ul>
              </li>
              <li>
                <a href="<?=base_url()?>Administrador/index/seguimiento-soilcitud"><i class="fas fa-eye"></i>Seguimiento Solicitudes</a>
              </li>
            </ul>
        </div>
    </nav>
  </header>
  <!-- END HEADER MOBILE-->
