<!-- MAIN CONTENT-->
<div class="main-content">
    <div class="section__content section__content--p30">
        <div class="container-fluid">
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <h2 class="title-1">Estadísticas Generales Del mes de <strong><?=$fecha ?></strong></h2>
              <br>
            </div>
            <div class="col-md-6 col-lg-3">
                  <div class="statistic__item">
                      <h5 id="solicitudes_proceso" class="number"><?=number_format($solicitudes_proceso) ?></h5>
                      <span class="desc">Solicitudes En Proceso</span>
                      <div class="icon">
                          <i class="zmdi zmdi-eye"></i>
                      </div>
                  </div>
              </div>
            <div class="col-md-6 col-lg-3">
                  <div class="statistic__item">
                      <h5 id="solicitudes_terminadas" class="number"><?=number_format($solicitudes_terminadas) ?></h5>
                      <span class="desc">Solicitudes Terminadas</span>
                      <div class="icon">
                          <i class="zmdi zmdi-eye-off"></i>
                      </div>
                  </div>
              </div>
            <div class="col-md-6 col-lg-3">
              <div class="statistic__item">
                      <h5 id="solicitudes_canceladas" class="number"><?=$solicitudes_canceladas ?></h5>
                      <span class="desc">Solicitudes Canceladas</span>
                      <div class="icon">
                          <i class="zmdi zmdi-close"></i>
                      </div>
                  </div>
            </div>
            <div class="col-md-6 col-lg-3">
              <div class="statistic__item">
                <h4 id="dinero_generado" class="number">$<?=number_format($dinero_generado->costo,2) ?></h4>
                <span class="desc">Dinero Generado</span>
                <div class="icon">
                  <i class="zmdi zmdi-money"></i>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-lg-12">
                <h2 class="title-1 m-b-25">Tabla de solicitudes generadas</h2>
                <div class="table-responsive table--no-card m-b-40">
                    <table class="table table-borderless table-striped table-earning">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Fecha De Inicio</th>
                                <th>Última Actualización</th>
                                <th>Trámite</th>
                                <th>Estatus</th>
                                <th>Días</th>
                            </tr>
                        </thead>
                        <tbody id="tablaGralSolicitudes">
                            <?php foreach ($solicitudes as $solicitud): ?>
                              <tr>
                                <td><?=$solicitud->id ?></td>
                                <td><?=$solicitud->fecha_inicio ?></td>
                                <td><?=$solicitud->fecha_fin ?></td>
                                <td><?=$solicitud->tramite; ?> <?=($solicitud->giro != null) ? "($solicitud->giro)" : "" ?> <?=($solicitud->tipoConstruccion != null) ? "($solicitud->tipoConstruccion)" :"" ?></td>
                                <?php switch ($solicitud->estatus) {//validamos el estatus del trámite
                                  case '4':
                                    $solicitud->estatus = "Revisión";
                                    break;
                                  case '3':
                                    $solicitud->estatus = "Pago";
                                    break;
                                  case '2':
                                    $solicitud->estatus = "Entrega en Ventanilla";
                                    break;
                                  case '5':
                                    $solicitud->estatus = "Correcciones";
                                    break;
                                  case '1':
                                    $solicitud->estatus = "<span class='status--process'>Terminado</span>";
                                    break;
                                  case '0':
                                    $solicitud->estatus = "<span class='status--denied' >Cancelado</span>";
                                    break;
                                  default:
                                    // code...
                                    break;
                                } ?>
                                <td><?=$solicitud->estatus ?></td>
                                <?php $ee=date_create($solicitud->fecha_inicio)  ?>
                                <?php $hoy = date_create($solicitud->fecha_fin);  ?>
                                <?php $dias=date_diff($ee,$hoy)  ?>
                                <td><?= $dias->format('%R%a día(s)') ?></td>
                              </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="col-md-6 col-lg-6">
                  <!-- TOP CAMPAIGN-->
                  <div class="top-campaign">
                    <h3 class="title-3 m-b-30">Funcionarios Atendiendo Solicitudes</h3>
                    <div class="table-responsive">
                        <table class="table table-top-campaign">
                          <thead>
                            <tr>
                                <th>#</th>
                                <th class="text-right">Funcionario</th>
                                <th class="text-right">Dependencia</th>
                                <th class="text-right">Trámite</th>
                            </tr>
                        </thead>
                          <tbody id="tablaGralFuncionariosSolicitudes">
                            <?php foreach ($solicitudes as $solicitud): ?>
                              <?php if ($solicitud->estatus != "<span class='status--process'>Terminado</span>" && $solicitud->estatus != "<span class='status--denied' >Cancelado</span>"): ?>
                                  <tr>
                                    <td><?=$solicitud->id ?></td>
                                    <td><?=$solicitud->primer_nombreF  . " " . $solicitud->primer_apellidoF ?></td>
                                    <td><?=$solicitud->dependencia?></td>
                                    <td><?= $solicitud->tramite; ?> <?=($solicitud->giro != null) ? "($solicitud->giro)" : "" ?><?=($solicitud->tipoConstruccion != null) ? "($solicitud->tipoConstruccion)" :"" ?></td>
                                  </tr>
                              <?php endif; ?>
                            <?php endforeach; ?>
                          </tbody>
                        </table>
                    </div>
                  </div>
                  <!-- END TOP CAMPAIGN-->
              </div>
              <div class="col-xl-6">
                  <!-- TASK PROGRESS-->
                  <div class="task-progress">
                      <h3 class="title-3">Progreso de las solicitudes</h3>
                      <div class="au-skill-container">
                        <?php foreach ($progreso_solicitudes as $progreso): ?>
                          <?php if ($progreso->estatus != 1 && $progreso->estatus != 0): ?>
                            <div class="au-progress">
                              <td>#<?=$progreso->id . " " . $progreso->nombre ?> </td>
                              <span class="au-progress__title"><?=($progreso->giro != null) ? "($progreso->giro)" : "" ?><?=($progreso->tipoConstruccion != null) ? "($progreso->tipoConstruccion)" :"" ?></span>
                              <div class="progress mb-2">
                                <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: <?=$progreso->progreso ?>%" aria-valuenow="<?=$progreso->progreso ?>"
                                 aria-valuemin="0" aria-valuemax="100"><?=number_format($progreso->progreso)?>%</div>
                              </div>
                            </div>
                          <?php endif; ?>
                        <?php endforeach; ?>
                      </div>
                  </div>
                  <!-- END TASK PROGRESS-->
              </div>
          </div>
          <div class="row">
            <div class="col-md-12 col-lg-12">
              <h2 class="title-1"><?=($documentos_generados != null) ? "Documentos y Licencias Generadas" : "" ?></h2>
              <br>
            </div>
            <div class="row" id="documentos">
              <?php foreach ($documentos_generados as $doc): ?>
                <div class="col-md-6 col-lg-3">
                  <div class="statistic__item">
                    <h2 class="number"><?=number_format($doc->documentos_generados) ?></h2>
                    <span class="desc"><?=$doc->tramite ?></span>
                    <div class="icon">
                      <i class="zmdi zmdi-file"></i>
                    </div>
                  </div>
              </div>
              <?php endforeach; ?>
            </div>
          </div>
        </div>
      </div>
    </div>
